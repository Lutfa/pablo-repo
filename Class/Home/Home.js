import React, { Component } from "react"
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Image,
  Platform,
  Animated,
  StatusBar,
  StyleSheet,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  TextInput,
} from "react-native"
import Sidemenu from "./Sidemenu"
import Drawer from "react-native-drawer"
import Constant from "./../BaseClass/Constant"
import BaseStyles from "./../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "./../BaseClass/language"
import urls from "./../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import Db from "./../DB/Realm"
import ImageLoad from "./../BaseClass/ImageLoader"
import moment from "moment"
import CartDb from "./../DB/CartDb"
import Toast from "react-native-simple-toast"
import KeepAwake from "react-native-keep-awake"
import HTMLView from "react-native-htmlview"
import BannersView from "./Banners"
import { CommonActions } from "@react-navigation/native"
import SidemenuAnimation from "./SideMenuAnimation"
import axios from "axios"

var Realm = require("realm")

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      drawerOpen: false,
      isCategories: true,
      isBestSelling: true,
      bestOffersArray: [],
      bestSellingArray: [],
      newProductsArray: [],
      bannersArray: [],
      categoriesArray: [],
      healthIssueArray: [],
      articlesArray: [],
      bannerPosition: 0,
      articlePosition: 0,
      allDataArray: [],
    }

    language.setLanguage("en")
  }

  async componentDidMount() {
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )
    SplashScreen.hide()

    this.getSavedData()

    let params = {
      BannerPosition: "Top",
    }

    let articlesParams = {
      CustomerId: global.CustomerId,
    }

    let categoriesParams = {
      LastUpdatedTimeTicks: 0,
    }
    let healthIssuesParams = {
      LastUpdatedTimeTicks: 0,
    }

    let newlyAddedProductsParams = {
      PageNumber: 1,
      PageSize: 5,
    }
    console.log(JSON.stringify(articlesParams))
    // this.setState({ isLoading: true })
    this.getAddress()

    this.getData()

    this.loadCartItems()
  }

  async getData() {
    let params = {
      BannerPosition: "Top",
    }

    let articlesParams = {
      CustomerId: global.CustomerId,
    }

    let categoriesParams = {
      LastUpdatedTimeTicks: 0,
    }
    let healthIssuesParams = {
      LastUpdatedTimeTicks: 0,
    }

    let newlyAddedProductsParams = {
      PageNumber: 1,
      PageSize: 5,
    }

    const results = await Promise.all([
      await (
        await axios.post(urls.getBanners, params)
      ).data,
      await (
        await axios.post(
          urls.getHealthIssues,
          healthIssuesParams
        )
      ).data,
      await (
        await axios.get(urls.getDiscounts)
      ).data,
      await (
        await axios.post(
          urls.getArticles,
          articlesParams
        )
      ).data,
      await (
        await axios.get(urls.getTopSellerProducts)
      ).data,
      await (
        await axios.post(
          urls.getNewlyAddedProducts,
          newlyAddedProductsParams
        )
      ).data,
      await (
        await axios.post(
          urls.getCategories,
          categoriesParams
        )
      ).data,
    ])

    console.log("Axios data new", results)

    this.setState({ allDataArray: results }, () => {
      this.saveServerData()
    })
  }
  saveServerData() {
    this.saveBanners(
      this.state.allDataArray[0].Response.Banners
    )
  }
  getSavedData() {
    let DiscountData = Db.objects(
      "BestDiscount"
    ).filtered(
      "CouponCode == null AND EndDateUtc > $0",
      new Date()
    )
    let BestSellingData = Db.objects(
      "BestSelling"
    ).sorted("DisplayOrder", true)

    let newProducts = Db.objects("NewArrivals")

    let bannerArray = Db.objects("Banners")

    let categoriesData = Db.objects(
      "Categories"
    ).filtered('ParentCategoryId = "0" LIMIT(4)')

    let healthIssuesData = Db.objects(
      "HealthIssues"
    )

    let articleData = Db.objects("Articles")

    this.setState({
      bestOffersArray: DiscountData,
      bestSellingArray: BestSellingData,
      bannersArray: bannerArray,
      categoriesArray: categoriesData,
      newProductsArray: newProducts,
      healthIssueArray: healthIssuesData,
      articlesArray: articleData,
      isLoading: !(
        DiscountData.length > 0 ||
        BestSellingData.length > 0 ||
        bannerArray.length > 0 ||
        categoriesData.length > 0 ||
        newProducts.length > 0 ||
        healthIssuesData.length > 0 ||
        articleData.length > 0
      ),
    })
  }

  toggle = () => {
    const { drawerOpen } = this.state
    !drawerOpen
      ? this.drawer.openMenu()
      : this.drawer.closeMenu()
  }

  async deleteData() {
    try {
      await Db.write(() => {
        Db.deleteAll()
        const deleted = this.removeItemValue(
          "userLoggedIn"
        )
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: "Intro",
              },
            ],
          })
        )
      })
    } catch (e) {
      console.log("Error on creation")
    }
  }
  async removeItemValue(key) {
    try {
      await AsyncStorage.removeItem(key)
      return true
    } catch (exception) {
      return false
    }
  }
  onMenuSelected(name) {
    this.setState({ drawerOpen: false })
    this.drawer.closeMenu()
    if (name == "Logout") {
      // this.deleteData()
      const deleted = this.removeItemValue(
        "userLoggedIn"
      )

      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            {
              name: "Login",
            },
          ],
        })
      )
    } else if (name != "Home") {
      this.props.navigation.push(name)
    }
  }

  render() {
    const {
      userData,
      articlesArray,
      bestSellingArray,
      drawerOpen,
      isCategories,
      bestOffersArray,
      isBestSelling,
      newProductsArray,
      bannersArray,
      categoriesArray,
      healthIssueArray,
      isLoading,
    } = this.state

    return (
      <SidemenuAnimation
        ref={(menu) => (this.drawer = menu)}
        menuSize={150}
        onMenuOpen={() => {
          this.setState({ drawerOpen: true })
        }}
        onCloseOpen={() => {
          this.setState({ drawerOpen: false })
        }}
        menuView={
          <Sidemenu
            onMenuSelected={(name) => {
              this.onMenuSelected(name)
            }}
          />
        }>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            drawerOpen
              ? this.drawer.closeMenu()
              : null
          }}
          style={{
            flex: 1,
            backgroundColor: "#fff",
            borderTopLeftRadius: drawerOpen ? 8 : 0,
            borderBottomLeftRadius: drawerOpen
              ? 8
              : 0,
          }}>
          {isLoading ? Constant.showLoader() : null}
          {this.headerView()}
          <KeepAwake />
          <View
            style={styles.mainView}
            pointerEvents={
              drawerOpen ? "none" : "auto"
            }>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              ref="mainScroll">
              <View style={styles.searchMainView}>
                <TouchableOpacity
                  activeOpacity={2}
                  onPress={() => {
                    this.props.navigation.navigate(
                      "Search"
                    )
                  }}
                  style={[styles.searchInner]}>
                  <Image
                    source={images.search}
                    style={{
                      marginLeft: 10,
                      width: 20,
                      height: 20,
                    }}
                  />
                  <Text
                    style={[
                      BaseStyles.boldFont,
                      styles.searchIcon,
                    ]}>
                    {language.searchmedicine}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={styles.topBox}>
                <TouchableOpacity
                  activeOpacity={2}
                  onPress={() => {
                    this.props.navigation.navigate(
                      "UploadPrescription"
                    )
                  }}
                  style={styles.topInnerBox}>
                  <View style={styles.topInnerBox2}>
                    <Image
                      resizeMode={"center"}
                      style={{
                        height: 25,
                        width: 25,
                      }}
                      source={
                        images.uploadPrescription
                      }
                    />
                  </View>
                  <Text
                    numberOfLines={2}
                    style={[
                      BaseStyles.regularFont,
                      styles.topInnerText,
                    ]}>
                    {language.uploadPres}
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={2}
                  onPress={() => {
                    this.props.navigation.navigate(
                      "HealthRecords"
                    )
                  }}
                  style={styles.topInnerBox}>
                  <View style={styles.topInnerBox2}>
                    <Image
                      resizeMode={"center"}
                      style={{
                        height: 25,
                        width: 25,
                      }}
                      source={images.healthRecord}
                    />
                  </View>
                  <Text
                    numberOfLines={2}
                    style={[
                      BaseStyles.regularFont,
                      styles.topInnerText,
                    ]}>
                    {language.healthrecords}
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={2}
                  onPress={() => {
                    this.props.navigation.navigate(
                      "AllMedicine"
                    )
                  }}
                  style={styles.topInnerBox}>
                  <View style={styles.topInnerBox2}>
                    <Image
                      resizeMode={"center"}
                      style={{
                        height: 25,
                        width: 25,
                      }}
                      source={images.pillReminder}
                    />
                  </View>
                  <Text
                    numberOfLines={2}
                    style={[
                      BaseStyles.regularFont,
                      styles.topInnerText,
                    ]}>
                    {language.medicationReminder}
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate(
                      "Chat"
                    )
                  }}
                  style={styles.topInnerBox}>
                  <View style={styles.topInnerBox2}>
                    <Image
                      resizeMode={"center"}
                      style={{
                        height: 25,
                        width: 25,
                      }}
                      source={images.chatIcon}
                    />
                  </View>
                  <Text
                    numberOfLines={2}
                    style={[
                      BaseStyles.regularFont,
                      styles.topInnerText,
                    ]}>
                    {language.chatwith}
                  </Text>
                </TouchableOpacity>
              </View>

              {/* Banner */}

              {bannersArray.length > 0 && (
                <BannersView
                  bannersArray={bannersArray}
                />
              )}

              {/* Categories */}

              {categoriesArray.length > 0 ||
              healthIssueArray.length > 0 ? (
                <View style={{ marginTop: 20 }}>
                  <View
                    style={[
                      styles.boxTitle,
                      {
                        borderTopLeftRadius: 6,
                        borderTopRightRadius: 6,
                      },
                    ]}>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent:
                          "space-between",
                      }}>
                      <Text
                        onPress={() => {
                          this.categoriesAction()
                        }}
                        style={[
                          BaseStyles.boldFont,
                          styles.subMenu,
                          {
                            color: isCategories
                              ? "#000"
                              : Constant.homeNonSelected(),
                          },
                        ]}>
                        {language.categories}
                      </Text>
                      <Text
                        onPress={() => {
                          this.healthIssuesAction()
                        }}
                        style={[
                          BaseStyles.boldFont,
                          styles.subMenu,
                          {
                            color: !isCategories
                              ? "#000"
                              : Constant.homeNonSelected(),
                          },
                        ]}>
                        {language.healthissues}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                      }}>
                      <Text
                        onPress={() => {
                          if (isCategories) {
                            this.props.navigation.navigate(
                              "CategoriesList"
                            )
                          } else {
                            this.props.navigation.navigate(
                              "HealthIssues"
                            )
                          }
                        }}
                        style={[
                          BaseStyles.regularFont,
                          styles.viewAll,
                        ]}>
                        {language.viewall}
                      </Text>
                      <Image
                        resizeMode={"center"}
                        style={{
                          height: 12,
                          width: 12,
                          marginTop: 3,
                        }}
                        source={images.next}
                      />
                    </View>
                  </View>
                  <View
                    style={[
                      styles.topBox,
                      {
                        backgroundColor: "#fff",
                        borderBottomLeftRadius: 6,
                        borderBottomRightRadius: 6,
                        justifyContent: null,
                      },
                    ]}>
                    {(isCategories
                      ? categoriesArray
                      : healthIssueArray
                    ).map((item, index) => {
                      return (
                        <TouchableOpacity
                          activeOpacity={2}
                          onPress={() => {
                            if (isCategories) {
                              let Id = 0
                              if (
                                item.ParentCategoryId ==
                                0
                              ) {
                                Id = item.Id
                              } else {
                                Id =
                                  item.ParentCategoryId
                              }
                              this.props.navigation.navigate(
                                "Categories",
                                {
                                  parentId: Id,
                                  isCategoris: true,
                                }
                              )
                            } else {
                              this.props.navigation.navigate(
                                "Categories",
                                {
                                  parentId:
                                    item.HealthIssueId,
                                  isCategoris: false,
                                }
                              )
                            }
                          }}
                          style={[
                            styles.categoriesBox,
                          ]}>
                          <View
                            style={
                              styles.topInnerBox2
                            }>
                            {/* <Image resizeMode={'center'} style={{ height: 25, width: 25 }} source={images.otc} /> */}
                            <ImageLoad
                              placeholderStyle={{
                                height: 25,
                                width: 25,
                              }}
                              style={{
                                height: 25,
                                width: 25,
                              }}
                              loadingStyle={{
                                size: "large",
                                color:
                                  Constant.appColorAlpha(),
                              }}
                              source={{
                                uri: item.ImageUrl,
                              }}
                              placeholderSource={
                                images.placeholder
                              }
                            />
                          </View>
                          <Text
                            numberOfLines={2}
                            style={[
                              BaseStyles.regularFont,
                              styles.topInnerText,
                            ]}>
                            {item.Name}
                          </Text>
                        </TouchableOpacity>
                      )
                    })}
                  </View>
                </View>
              ) : null}

              {/* Best sellers */}

              {bestOffersArray.length > 0 ? (
                <View
                  style={{
                    marginTop: 20,
                    backgroundColor: "#fff",
                  }}>
                  <View
                    style={[
                      styles.boxTitle,
                      {
                        borderTopLeftRadius: 6,
                        borderTopRightRadius: 6,
                      },
                    ]}>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent:
                          "space-between",
                      }}>
                      <Text
                        style={[
                          BaseStyles.boldFont,
                          styles.subMenu,
                          { color: "#000" },
                        ]}>
                        {language.bestoffers}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                      }}>
                      <Text
                        style={[
                          BaseStyles.regularFont,
                          styles.viewAll,
                        ]}>
                        {language.viewall}
                      </Text>
                      <Image
                        resizeMode={"center"}
                        style={{
                          height: 12,
                          width: 12,
                          marginTop: 3,
                        }}
                        source={images.next}
                      />
                    </View>
                  </View>

                  <ScrollView
                    horizontal={true}
                    scrollEventThrottle={16}
                    showsHorizontalScrollIndicator={
                      false
                    }>
                    {bestOffersArray.map(
                      (item, i) => {
                        // the _ just means we won't use that parameter

                        return (
                          <View
                            key={i}
                            style={
                              styles.bestOffersMain
                            }>
                            <TouchableOpacity
                              activeOpacity={2}
                              onPress={() => {
                                this.props.navigation.navigate(
                                  "AllProducts",
                                  {
                                    type: 3,
                                    data: item,
                                  }
                                )
                              }}
                              style={
                                styles.bestOffersInner
                              }>
                              {/* <Image resizeMode={'center'} style = {{width:100,height:80}} source={images.testNew}/> */}

                              <ImageLoad
                                placeholderStyle={{
                                  width: 100,
                                  height: 80,
                                }}
                                style={{
                                  width: 100,
                                  height: 80,
                                }}
                                loadingStyle={{
                                  size: "large",
                                  color:
                                    Constant.appColorAlpha(),
                                }}
                                source={{
                                  uri: item.PictureUrl,
                                }}
                                placeholderSource={
                                  images.placeholder
                                }
                              />

                              <Text
                                style={[
                                  styles.bestOffersText,
                                  BaseStyles.boldFont,
                                ]}>
                                {item.Name}
                              </Text>
                            </TouchableOpacity>
                          </View>
                        )
                      }
                    )}
                  </ScrollView>
                </View>
              ) : null}

              {/* Articles for you */}
              {articlesArray.length > 0 ? (
                <View
                  style={{
                    marginTop: 20,
                    paddingLeft: 5,
                    paddingRight: 5,
                  }}>
                  <View style={[styles.boxTitle]}>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent:
                          "space-between",
                        alignItems: "center",
                        width: "100%",
                      }}>
                      <View style={{ flex: 0.4 }}>
                        <Text
                          style={[
                            BaseStyles.boldFont,
                            styles.subMenu,
                            {
                              color: "#000",
                              marginRight: 0,
                            },
                          ]}>
                          {language.articlesforyou}
                        </Text>
                      </View>
                      <View style={{ flex: 0.3 }}>
                        <View
                          style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent:
                              "center",
                          }}>
                          {articlesArray.map(
                            (_, i) => {
                              // the _ just means we won't use that parameter

                              return (
                                <Animated.View
                                  key={i}
                                  style={[
                                    styles.bannerDot,
                                    {
                                      backgroundColor:
                                        i ==
                                        this.state
                                          .articlePosition
                                          ? Constant.appColorAlpha()
                                          : Constant.appLightColor(),
                                    },
                                  ]}
                                />
                              )
                            }
                          )}
                        </View>
                      </View>
                      <TouchableOpacity
                        activeOpacity={2}
                        onPress={() => {
                          this.props.navigation.navigate(
                            "ArticleTabs"
                          )
                        }}
                        style={{
                          flex: 0.3,
                          justifyContent: "center",
                          alignItems: "center",
                          width: 80,
                          height: 30,
                        }}>
                        <Image
                          resizeMode={"center"}
                          style={{
                            position: "absolute",
                            right: 0,
                            height: 20,
                            width: 20,
                            marginTop: 3,
                          }}
                          source={images.nextTwo}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>

                  <ScrollView
                    horizontal={true}
                    scrollEventThrottle={16}
                    pagingEnabled={true}
                    decelerationRate={0}
                    snapToAlignment={"center"}
                    snapToInterval={width - 23}
                    showsHorizontalScrollIndicator={
                      false
                    }
                    onMomentumScrollEnd={
                      this._onMomentumArticle
                    }>
                    {articlesArray.map(
                      (item, i) => {
                        // the _ just means we won't use that parameter
                        return (
                          <View
                            key={i}
                            style={{ padding: 5 }}>
                            <TouchableOpacity
                              onPress={() => {
                                this.props.navigation.navigate(
                                  "ArticleDetail",
                                  { item }
                                )
                              }}
                              style={
                                styles.articleitem
                              }>
                              {/* <Image style = {{height:'100%',width:120,borderBottomLeftRadius:6,borderTopLeftRadius:6}} source = {images.testThree}/> */}

                              <ImageLoad
                                placeholderStyle={{
                                  height: "100%",
                                  width: 120,
                                  borderBottomLeftRadius: 6,
                                  borderTopLeftRadius: 6,
                                }}
                                style={{
                                  height: "100%",
                                  width: 120,
                                  borderBottomLeftRadius: 6,
                                  borderTopLeftRadius: 6,
                                }}
                                loadingStyle={{
                                  size: "large",
                                  color:
                                    Constant.appColorAlpha(),
                                }}
                                source={{
                                  uri: item.Image,
                                }}
                                placeholderSource={
                                  images.placeholder
                                }
                              />

                              <View
                                style={{
                                  flex: 1,
                                  backgroundColor:
                                    "#fff",
                                  borderBottomRightRadius: 6,
                                  borderTopRightRadius: 6,
                                  padding: 10,
                                }}>
                                <Text
                                  numberOfLines={1}
                                  style={[
                                    BaseStyles.boldFont,
                                    styles.articleTitle,
                                  ]}>
                                  {item.Title}
                                </Text>
                                {/* <Text numberOfLines = {3} style={[BaseStyles.regularFont,styles.articleSubmenu]}>{item.Description}</Text> */}
                                <View
                                  style={{
                                    height: "80%",
                                    overflow:
                                      "hidden",
                                  }}>
                                  <HTMLView
                                    value={
                                      item.Description
                                    }
                                    stylesheet={
                                      styles
                                    }
                                  />
                                </View>
                              </View>
                            </TouchableOpacity>
                          </View>
                        )
                      }
                    )}
                  </ScrollView>
                </View>
              ) : null}

              {/* Best Selling */}

              {bestSellingArray.length > 0 ||
              newProductsArray.length > 0 ? (
                <View
                  style={{
                    marginTop: 10,
                    padding: 5,
                  }}>
                  <View style={styles.boxTitle}>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent:
                          "space-between",
                      }}>
                      <Text
                        onPress={() => {
                          this.bestsellingAction()
                          this.refs._scrollView.scrollTo(
                            {
                              x: 0,
                              y: 0,
                              animated: true,
                            }
                          )
                        }}
                        style={[
                          BaseStyles.boldFont,
                          styles.subMenu,
                          {
                            color: isBestSelling
                              ? "#000"
                              : Constant.homeNonSelected(),
                          },
                        ]}>
                        {language.bestselling}
                      </Text>
                      <Text
                        onPress={() => {
                          this.newArrivalsAction()
                          this.refs._scrollView.scrollTo(
                            {
                              x: 0,
                              y: 0,
                              animated: true,
                            }
                          )
                        }}
                        style={[
                          BaseStyles.boldFont,
                          styles.subMenu,
                          {
                            color: !isBestSelling
                              ? "#000"
                              : Constant.homeNonSelected(),
                          },
                        ]}>
                        {language.newarrivals}
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={{ padding: 12 }}
                      onPress={() => {
                        this.props.navigation.navigate(
                          "AllProducts",
                          {
                            type: isBestSelling
                              ? 1
                              : 2,
                          }
                        )
                      }}
                      style={{
                        flexDirection: "row",
                      }}>
                      <Image
                        resizeMode={"center"}
                        style={{
                          height: 20,
                          width: 20,
                          marginTop: 3,
                        }}
                        source={images.nextTwo}
                      />
                    </TouchableOpacity>
                  </View>

                  <ScrollView
                    horizontal={true}
                    scrollEventThrottle={16}
                    showsHorizontalScrollIndicator={
                      false
                    }
                    ref="_scrollView">
                    {/* Best Selling */}
                    {(isBestSelling
                      ? bestSellingArray
                      : newProductsArray
                    ).map((item, i) => {
                      // the _ just means we won't use that parameter
                      return (
                        <View
                          key={i}
                          style={
                            styles.bestSellingsMain
                          }>
                          <TouchableOpacity
                            onPress={() => {
                              this.props.navigation.push(
                                "ProductDetails",
                                { product: item }
                              )
                            }}
                            style={
                              styles.bottomFlatlist
                            }>
                            {/* <Image resizeMode={'center'} style = {{width:110,height:70}} source={images.testNew}/> */}
                            <ImageLoad
                              placeholderStyle={{
                                width: 110,
                                height: 70,
                              }}
                              style={{
                                width: 110,
                                height: 70,
                              }}
                              loadingStyle={{
                                size: "large",
                                color:
                                  Constant.appColorAlpha(),
                              }}
                              source={{
                                uri:
                                  item.Images
                                    .length > 0
                                    ? item.Images[0]
                                        .Src
                                    : "null",
                              }}
                              placeholderSource={
                                images.placeholder
                              }
                            />
                            {item.ColorCode ==
                            Constant.prescriptionColorCode() ? (
                              <View
                                style={{
                                  position:
                                    "absolute",
                                  alignItems:
                                    "center",
                                  justifyContent:
                                    "center",
                                  top: 10,
                                  right: 0,
                                  height: 26,
                                  width: 32,
                                  borderBottomLeftRadius: 17.5,
                                  borderTopLeftRadius: 17.5,
                                }}>
                                <Image
                                  style={{
                                    height: 15,
                                    width: 15,
                                  }}
                                  source={
                                    images.prescription
                                  }
                                />
                              </View>
                            ) : null}
                            <View
                              style={{
                                padding: 5,
                              }}>
                              <Text
                                numberOfLines={1}
                                style={[
                                  { fontSize: 14 },
                                  BaseStyles.boldFont,
                                ]}>
                                {item.Name}
                              </Text>
                              <Text
                                numberOfLines={1}
                                style={[
                                  {
                                    fontSize: 12,
                                    color:
                                      Constant.textAlpha(),
                                    lineHeight: 15,
                                  },
                                  BaseStyles.regularFont,
                                ]}>
                                {
                                  item.ShortDescription
                                }
                              </Text>

                              <View
                                style={{
                                  height: 26,
                                  marginBottom: 0,
                                  paddingTop: 5,
                                  width: 110,
                                  justifyContent:
                                    "space-between",
                                  alignItems:
                                    "center",
                                  flexDirection:
                                    "row",
                                }}>
                                <Text
                                  style={[
                                    {
                                      fontSize: 14,
                                      width: 70,
                                      color:
                                        Constant.redColor(),
                                    },
                                    BaseStyles.boldFont,
                                  ]}>
                                  {Constant.price()}
                                  {parseFloat(
                                    item.Price
                                  ).toFixed(2)}
                                </Text>
                                <TouchableOpacity
                                  onPress={() => {
                                    let isAdded =
                                      CartDb.addData(
                                        item
                                      )
                                    if (isAdded) {
                                      Toast.show(
                                        "Product added to cart"
                                      )
                                    }
                                    //  let cartData = Db.objects('CartDB')
                                    //   console.log(JSON.stringify(cartData));
                                    this.forceUpdate()
                                  }}
                                  style={{
                                    height: 26,
                                    width: 26,
                                    backgroundColor:
                                      Constant.appColorAlpha(),
                                    borderRadius: 13,
                                    justifyContent:
                                      "center",
                                    alignItems:
                                      "center",
                                  }}>
                                  <Image
                                    resizeMode={
                                      "center"
                                    }
                                    style={{
                                      width: 16,
                                      height: 16,
                                    }}
                                    source={
                                      images.cart
                                    }
                                  />
                                </TouchableOpacity>
                              </View>
                            </View>
                          </TouchableOpacity>
                        </View>
                      )
                    })}
                  </ScrollView>
                </View>
              ) : null}

              <View style={{ height: 20 }} />
            </ScrollView>
          </View>
        </TouchableOpacity>
      </SidemenuAnimation>
    )
  }

  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView
        style={[
          styles.headerStyle,
          {
            borderTopLeftRadius: drawerOpen ? 8 : 0,
          },
        ]}>
        <StatusBar
          translucent
          barStyle="light-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor={Constant.headerColor()}
        />
        <TouchableOpacity
          style={styles.headerInnerView}>
          <TouchableOpacity
            activeOpacity={2}
            style={styles.menuStyle}
            onPress={this.toggle}>
            <Image
              style={{ width: 20, height: 20 }}
              source={images.menu}
            />
            <Text style={[BaseStyles.headerFont]}>
              Pablo Pharmacy
            </Text>
          </TouchableOpacity>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginRight: 15,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate(
                  "Cart"
                )
              }}
              style={{
                paddingRight: 12,
                marginRight: 8,
              }}>
              <Image
                resizeMode={"center"}
                style={{ width: 20, height: 20 }}
                source={images.cart}
              />
              {CartDb.Count() > 0 ? (
                <View
                  style={{
                    position: "absolute",
                    left: 12,
                    top: -10,
                    width: 20,
                    justifyContent: "center",
                    alignItems: "center",
                    height: 20,
                    borderRadius: 10,
                    backgroundColor:
                      Constant.appColorAlpha(),
                  }}>
                  <Text
                    style={[
                      {
                        color: "#fff",
                        fontSize: 12,
                      },
                      BaseStyles.boldFont,
                    ]}>
                    {CartDb.Count()}
                  </Text>
                </View>
              ) : null}
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate(
                  "Wishlist"
                )
              }}
              style={{
                paddingRight: 12,
                marginRight: 8,
              }}>
              <Image
                resizeMode={"center"}
                style={{ width: 20, height: 20 }}
                source={images.wishlist}
              />
              {CartDb.WishListCount() > 0 ? (
                <View
                  style={{
                    position: "absolute",
                    left: 12,
                    top: -10,
                    width: 20,
                    justifyContent: "center",
                    alignItems: "center",
                    height: 20,
                    borderRadius: 10,
                    backgroundColor:
                      Constant.appColorAlpha(),
                  }}>
                  <Text
                    style={[
                      {
                        color: "#fff",
                        fontSize: 12,
                      },
                      BaseStyles.boldFont,
                    ]}>
                    {CartDb.WishListCount()}
                  </Text>
                </View>
              ) : null}
            </TouchableOpacity>

            <TouchableOpacity
              style={{}}
              onPress={() => {
                this.props.navigation.navigate(
                  "Profile"
                )
              }}>
              <ImageLoad
                borderRadius={17.5}
                placeholderStyle={{
                  width: 35,
                  height: 35,
                  borderRadius: 17.5,
                }}
                style={{
                  width: 35,
                  height: 35,
                  borderRadius: 17.5,
                }}
                loadingStyle={{
                  size: "large",
                  color: Constant.appColorAlpha(),
                }}
                source={{
                  uri: userData.ProfilePicture,
                }}
              />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </SafeAreaView>
    )
  }

  _onMomentumBanner = ({ nativeEvent }: any) => {
    const index = Math.round(
      nativeEvent.contentOffset.x / width
    )

    if (index !== this.state.bannerPosition) {
      this.setState({ bannerPosition: index })
    }
  }

  _onMomentumArticle = ({ nativeEvent }: any) => {
    const index = Math.round(
      nativeEvent.contentOffset.x / width
    )

    if (index !== this.state.articlePosition) {
      this.setState({ articlePosition: index })
    }
  }

  categoriesAction() {
    this.setState({ isCategories: true })
  }

  healthIssuesAction() {
    this.setState({ isCategories: false })
  }

  bestsellingAction() {
    this.setState({ isBestSelling: true })
  }

  newArrivalsAction() {
    this.setState({ isBestSelling: false })
  }

  //Serivce calls

  async getDiscounts(time) {
    // this.setState({isLoading:true})
    let params = {
      CustomerId: global.CustomerId,
      lastUpdatedTimeTicks: time,
    }
    Constant.postMethod(
      urls.getDiscounts,
      params,
      (result) => {
        //this.setState({isLoading:false})

        if (result.success) {
          if (result.result.Response != null) {
            let discount =
              result.result.Response.Discounts

            if (discount.length > 0) {
              discount.map((item, index) => {
                try {
                  Db.write(() => {
                    Db.create(
                      "BestDiscount",
                      {
                        Id: item.Id,
                        Name: item.Name,
                        DiscountTypeId:
                          item.DiscountTypeId,
                        UsePercentage:
                          item.UsePercentage,
                        DiscountPercentage:
                          item.DiscountPercentage,
                        DiscountAmount:
                          item.DiscountAmount,
                        MaximumDiscountAmount:
                          item.MaximumDiscountAmount,
                        StartDateUtc:
                          item.StartDateUtc,
                        EndDateUtc: new Date(
                          item.EndDateUtc
                        ),
                        RequiresCouponCode:
                          item.RequiresCouponCode,
                        CouponCode: item.CouponCode,
                        IsCumulative:
                          item.IsCumulative,
                        LimitationTimes:
                          item.LimitationTimes,
                        MaximumDiscountedQuantity:
                          item.MaximumDiscountedQuantity,
                        MinOrderTotal:
                          item.MinOrderTotal,
                        Description:
                          item.Description,
                        CreatedOnUtc:
                          item.CreatedOnUtc,
                        UpdatedOnUtc:
                          item.UpdatedOnUtc,
                        PictureId: item.PictureId,
                        PictureUrl: item.PictureUrl,
                      },
                      "modified"
                    )
                  })
                } catch (e) {
                  console.log("Error on creatione")
                  console.log(JSON.stringify(e))
                }
              })
            }
            AsyncStorage.setItem(
              "DiscountTimeTicks",
              JSON.stringify(
                result.result.Response
                  .lastUpdatedTimeTicks
              )
            )
            let DiscountData = Db.objects(
              "BestDiscount"
            ).filtered(
              "CouponCode == null AND EndDateUtc > $0",
              new Date()
            )

            this.setState({
              bestOffersArray: DiscountData,
            })
          }
        }

        AsyncStorage.getItem(
          "NewArrivalsTimeTicks"
        ).then((asyncStorageRes) => {
          if (
            asyncStorageRes != null &&
            asyncStorageRes != ""
          ) {
            this.getNewProducts(asyncStorageRes)
          } else {
            this.getNewProducts("0")
          }
        })
      }
    )
  }

  saveCategories(data) {
    console.log("cat " + JSON.stringify(data))
    if (data.Categories.length > 0) {
      data.Categories.map((item, index) => {
        try {
          Db.write(() => {
            let bestSellingObj = Db.create(
              "Categories",
              {
                Id: item.Id,
                Name: item.Name,
                Description: item.Description,
                ParentCategoryId:
                  item.ParentCategoryId,
                Published: item.Published,
                Deleted: item.Deleted,
                DisplayOrder: item.DisplayOrder,
                ImageUrl: item.ImageUrl,
              },
              "modified"
            )
          })
        } catch (e) {
          console.log("Error on creatione")
          console.log(JSON.stringify(e))
        }
      })
    }
    this.setState({ isLoading: false })
    this.getSavedData()
  }

  async loadCartItems(time) {
    let params = {
      CustomerId: global.CustomerId,
    }
    Constant.postMethod(
      urls.getCartItems,
      params,
      (result) => {
        if (result.success) {
          if (result.result.Response != null) {
            let cartData = result.result.Response

            if (cartData.length > 0) {
              cartData.map((item, index) => {
                let isAdded = CartDb.addCartData(
                  item.Product,
                  item.Quantity
                )
              })
            }
            this.forceUpdate()
          }
        }
      }
    )
  }

  saveBanners(data = []) {
    if (data.length > 0) {
      CartDb.deleteDb("Banners")

      data.map((item, index) => {
        try {
          let data = Db.write(() => {
            let bestSellingObj = Db.create(
              "Banners",
              {
                BannerTypeId: item.BannerTypeId,
                BannerPositionId:
                  item.BannerPositionId,
                ImageUrl: item.ImageUrl,
                RedirectUrl: item.RedirectUrl,
                IsActive: item.IsActive,
                FromDate: item.FromDate,
                ToDate: item.ToDate,
                DisplayOrder: item.DisplayOrder,
                PictureId: item.PictureId,
                CategoryId: item.CategoryId,
                SelectedCategoryId:
                  item.SelectedCategoryId,
                SelectedDiscountId:
                  item.SelectedDiscountId,
                Id: item.Id,
                NavigationId: item.NavigationId,
                BannerTypeName: item.BannerTypeName,
                BannerPositionName:
                  item.BannerPositionName,
              },
              "modified"
            )
          })
        } catch (e) {
          console.log("Error on creatione")
          console.log(JSON.stringify(e))
        }
      })
    }

    this.saveHealthIssues(
      this.state.allDataArray[1].Response
        .HealthIssues
    )
  }

  saveNewProductsArray(data = []) {
    if (data.length > 0) {
      data.map((item, index) => {
        try {
          Db.write(() => {
            let newProduct = Db.create(
              "NewArrivals",
              {
                Id: item.Id,
                Name: item.Name,
                ShortDescription:
                  item.ShortDescription,
                FullDescription:
                  item.FullDescription,
                AdminComment: item.AdminComment,
                DisplayOrder: item.DisplayOrder,
                Price: parseFloat(item.Price),
                OldPrice: parseFloat(item.OldPrice),
                Sku: item.Sku,
                PriceValue: item.PriceValue,
                ProductCost: parseFloat(
                  item.ProductCost
                ),
                OrderMinimumQuantity:
                  item.OrderMinimumQuantity,
                OrderMaximumQuantity:
                  item.OrderMaximumQuantity,
                DisplayOrder: item.DisplayOrder,
                IsTaxExempt: item.IsTaxExempt,
                TaxCategoryId: item.TaxCategoryId,
                Published: item.Published,
                CreatedOnUtc: item.CreatedOnUtc,
                UpdatedOnUtc: item.UpdatedOnUtc,
                ColorCode: item.ColorCode,
                GenericCode: item.GenericCode,
                CombinationCode:
                  item.CombinationCode,
                TherapeuticName:
                  item.TherapeuticName,
                GenericName: item.GenericName,
                AdultDosage: item.AdultDosage,
                ChildrenDosage: item.ChildrenDosage,
                Indication: item.Indication,
                ContraIndication:
                  item.ContraIndication,
                SideEffectsOrAdverseEffects:
                  item.SideEffectsOrAdverseEffects,
                PrecautionOrWarning:
                  item.PrecautionOrWarning,
                DrugInteractions:
                  item.DrugInteractions,
                RecommendedAdultDosage:
                  item.RecommendedAdultDosage,
                RecommendedChildrenDosage:
                  item.RecommendedChildrenDosage,
                PregnancyAlert: item.PregnancyAlert,
                LactationAlert: item.LactationAlert,
                DrivingAlert: item.DrivingAlert,
                LiverAlert: item.LiverAlert,
                KidneyAlert: item.KidneyAlert,
                SymptomsAndOverdoseTreatment:
                  item.SymptomsAndOverdoseTreatment,
                StorageCondition:
                  item.StorageCondition,
                AvailabiltyPackage:
                  item.AvailabiltyPackage,
                Cautions: item.Cautions,
                RatingSum: item.RatingSumm,
                TotalReviews: item.TotalReviews,
                MinStockQuantity:
                  item.MinStockQuantity,
                StockQuantity: item.StockQuantity,
                DiscountIds: [],
                ManufacturerIds: [],
                Images: [],
                ManufacturerNames: [],
              },
              "modified"
            )

            if (
              !!item.Images &&
              item.Images.length > 0
            ) {
              item.Images.map((imageItem) => {
                newProduct.Images.push({
                  Id: imageItem.Id,
                  PictureId: imageItem.PictureId,
                  DisplayOrder:
                    imageItem.DisplayOrder,
                  Src: imageItem.Src,
                })
              })
            }

            if (
              !!item.DiscountIds &&
              item.DiscountIds.length > 0
            ) {
              item.DiscountIds.map((id) => {
                newProduct.DiscountIds.push({
                  id: id,
                })
              })
            }

            if (
              item.ManufacturerIds != null &&
              item.ManufacturerIds.length > 0
            ) {
              item.ManufacturerIds.map((id) => {
                newProduct.ManufacturerIds.push({
                  id: id,
                })
              })
            }
            if (
              !!item.ManufacturerNames &&
              item.ManufacturerIds.length > 0
            ) {
              item.ManufacturerNames.map((name) => {
                newProduct.ManufacturerNames.push({
                  name: id,
                })
              })
            }
          })
        } catch (e) {
          console.log("Error on creatione")
          console.log(JSON.stringify(e))
        }
      })
    }

    this.saveCategories(
      this.state.allDataArray[6].Response
    )
  }

  saveBestSellings(data = []) {
    if (data.length > 0) {
      data.map((item, index) => {
        try {
          Db.write(() => {
            let bestSellingObj = Db.create(
              "BestSelling",
              {
                Id: item.Id,
                Name: item.Name,
                ShortDescription:
                  item.ShortDescription,
                AdminComment: item.AdminComment,
                DisplayOrder: item.DisplayOrder,
                Price: parseFloat(item.Price),
                OldPrice: parseFloat(item.OldPrice),
                PriceValue: item.PriceValue,
                Images: [],
              },
              "modified"
            )

            if (
              !!item.Images &&
              item.Images.length > 0
            ) {
              item.Images.map((imageItem) => {
                bestSellingObj.Images.push({
                  Id: imageItem.Id,
                  PictureId: imageItem.PictureId,
                  DisplayOrder:
                    imageItem.DisplayOrder,
                  Src: imageItem.Src,
                })
              })
            }
          })
        } catch (e) {
          console.log("Error on creatione")
          console.log(JSON.stringify(e))
        }
      })
    }
    this.saveNewProductsArray(
      this.state.allDataArray[5].Response.Data
    )
  }
  saveArticles(data = []) {
    if (data.length > 0) {
      data.map((item, index) => {
        try {
          Db.write(() => {
            let articleDb = Db.create(
              "Articles",
              {
                HasLikedIt: item.HasLikedIt,
                HasBookmarkedIt:
                  item.HasBookmarkedIt,
                Id: item.Id,
                Title: item.Title,
                Description: item.Description,
                Image: item.Image,
                CreatedOnUtc: new Date(
                  item.CreatedOnUtc
                ),
                LikesCount: item.LikesCount,
                BookmarksCount: item.BookmarksCount,
                IsActive: item.IsActive,
                PictureId: item.PictureId,
                CategoryIds: [],
              },
              "modified"
            )

            if (item.CategoryIds != null) {
              if (item.CategoryIds.length > 0) {
                item.CategoryIds.map((id) => {
                  articleDb.CategoryIds.push({
                    id: id,
                  })
                })
              }
            }
          })
        } catch (e) {
          console.log("Error on creatione")
          console.log(JSON.stringify(e))
        }
      })
    }

    this.saveBestSellings(
      this.state.allDataArray[4].Response
    )
  }
  saveBestOffers(data = []) {
    if (data.length > 0) {
      data.map((item, index) => {
        try {
          Db.write(() => {
            Db.create(
              "BestDiscount",
              {
                Id: item.Id,
                Name: item.Name,
                DiscountTypeId: item.DiscountTypeId,
                UsePercentage: item.UsePercentage,
                DiscountPercentage:
                  item.DiscountPercentage,
                DiscountAmount: item.DiscountAmount,
                MaximumDiscountAmount:
                  item.MaximumDiscountAmount,
                StartDateUtc: item.StartDateUtc,
                EndDateUtc: new Date(
                  item.EndDateUtc
                ),
                RequiresCouponCode:
                  item.RequiresCouponCode,
                CouponCode: item.CouponCode,
                IsCumulative: item.IsCumulative,
                LimitationTimes:
                  item.LimitationTimes,
                MaximumDiscountedQuantity:
                  item.MaximumDiscountedQuantity,
                MinOrderTotal: item.MinOrderTotal,
                Description: item.Description,
                CreatedOnUtc: item.CreatedOnUtc,
                UpdatedOnUtc: item.UpdatedOnUtc,
                PictureId: item.PictureId,
                PictureUrl: item.PictureUrl,
              },
              "modified"
            )
          })
        } catch (e) {
          console.log("Error on creatione")
          console.log(JSON.stringify(e))
        }
      })
    }

    this.saveArticles(
      this.state.allDataArray[3].Response.Articles
    )
  }
  saveHealthIssues(data = []) {
    // alert(JSON.stringify(data))
    if (data.length > 0) {
      data.map((item, index) => {
        try {
          Db.write(() => {
            let bestSellingObj = Db.create(
              "HealthIssues",
              {
                HealthIssueId: item.Id,
                Name: item.Name,
                Description: item.Description,
                PictureId: item.PictureId,
                Published: item.Published,
                Deleted: item.Deleted,
                DisplayOrder: item.DisplayOrder,
                ImageUrl: item.ImageUrl,
              },
              "modified"
            )
          })
        } catch (e) {
          console.log("Error on creatione")
          console.log(JSON.stringify(e))
        }
      })
    }
    this.saveBestOffers(
      this.state.allDataArray[2].Response
    )
  }

  getAddress(time) {
    let params = {
      CustomerId: global.CustomerId,
      LastUpdatedTimeTicks: 0,
    }
    Constant.postMethod(
      urls.getCustomerAddress,
      params,
      (result) => {
        // this.setState({isLoading:false})

        if (result.success) {
          if (result.result.Response != null) {
            //  this.setState({AddressArray:result.result.Response.Address})

            result.result.Response.map(
              (oldItem, index) => {
                let item = oldItem.Result
                try {
                  Db.write(() => {
                    Db.create(
                      "Address",
                      {
                        FirstName: item.FirstName,
                        LastName: item.LastName,
                        Email: item.Email,
                        Company: item.Company,
                        CountryId: item.CountryId,
                        StateProvinceId:
                          item.StateProvinceId,
                        County: item.County,
                        City: item.City,
                        Address1: item.Address1,
                        Address2: item.Address2,
                        ZipPostalCode:
                          item.ZipPostalCode,
                        PhoneNumber:
                          item.PhoneNumber,
                        FaxNumber: item.FaxNumber,
                        CustomAttributes:
                          item.CustomAttributes,
                        CreatedOnUtc:
                          item.CreatedOnUtc,
                        AddressCategory:
                          item.AddressCategory,
                        Longitude: `${item.Longitude}`,
                        Latitude: `${item.Latitude}`,
                        LocationName:
                          item.LocationName,
                        AddressId: item.AddressId,
                        CustomerId: item.CustomerId,
                        DeviceUniqueId:
                          item.DeviceUniqueId,
                        Landmark: item.Landmark,
                      },
                      "modified"
                    )
                  })
                } catch (e) {
                  console.log("Error on creatione")
                  console.log(JSON.stringify(e))
                }
              }
            )

            AsyncStorage.setItem(
              "addressTimeTicks",
              JSON.stringify(
                result.result.Response
                  .LastUpdatedTimeTicks
              )
            )
            let addressData = Db.objects("Address")
            this.setState({
              AddressArray: addressData,
            })
          } else {
            //    Toast.show(result.result.Response.Message)
          }
        } else {
          //   this.setState({isNumberCorrect:false})
        }
      }
    )
  }
}

const styles = StyleSheet.create({
  headerStyle: {
    flex: 0,
    backgroundColor: "#fff",

    zIndex: 88,
    paddingBottom: 110,
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
    backgroundColor: Constant.headerColor(),
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    justifyContent: "center",
  },
  headerInnerView: {
    height: 50,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },

  homeTitle: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "montserrant-semi-bold",
    fontWeight: "200",
    left: 20,
  },
  menuStyle: {
    marginLeft: 15,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  mainView: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: -110,
    zIndex: 101,
  },
  topBox: {
    height: 100,
    paddingRight: 5,
    paddingLeft: 5,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  topInnerBox: {
    flex: 0.2,

    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 6,
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
  },

  categoriesBox: {
    width: width / 4 - 16,
    marginRight: 10,

    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 6,
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
  },
  subMenu: { fontSize: 16, marginRight: 15 },
  viewAll: {
    fontSize: 13,
    color: Constant.homeNonSelected(),
  },
  topInnerBox2: {
    width: 40,
    height: 40,
    borderRadius: 6,
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    backgroundColor: Constant.appLightColor(),
    alignItems: "center",
    justifyContent: "center",
  },
  topInnerText: {
    color: Constant.textColor(),
    paddingLeft: 5,
    paddingRight: 5,
    marginTop: 10,
    fontSize: 11,
    textAlign: "center",
    lineHeight: 13,
    height: 26,
  },

  boxTitle: {
    height: 40,
    width: "100%",
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 5,
    paddingLeft: 5,
  },
  bestOffersMain: {
    height: 120,
    width: "100%",
    borderRadius: 6,
    padding: 4,
    paddingLeft: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  bestOffersInner: {
    width: "100%",
    borderRadius: 6,
    backgroundColor: "#fff",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    elevation: 2,
    marginRight: 2,
    paddingTop: 6,
    alignItems: "center",
    justifyContent: "center",
  },
  bestOffersText: {
    width: "100%",
    backgroundColor: Constant.appColorAlpha(),
    color: Constant.selectedTextColor(),
    textAlign: "center",
    padding: 2,
    fontSize: 12,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
  bannerMain: {
    marginTop: 20,
    backgroundColor: Constant.appColorAlpha(),
    borderRadius: 6,
  },
  banneritem: {
    width: width - 30,
    height: 200,
    borderRadius: 6,
    backgroundColor: Constant.appColorAlpha(),
    overflow: "hidden",
  },
  bannerTitle: {
    color: Constant.selectedTextColor(),
    fontSize: 16,
  },
  bannerSub: {
    color: Constant.selectedTextColor(),
    fontSize: 13,
    marginTop: 10,
  },
  bannerBuy: {
    marginTop: 15,
    width: 100,
    borderRadius: 20,
    elevation: 2,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    backgroundColor: Constant.appFullColor(),
  },
  bannerDotView: {
    flexDirection: "row",
    paddingBottom: 0,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 5,
    left: 0,
    right: 0,
  },
  bannerDot: {
    height: 9,
    width: 9,
    elevation: 2,
    backgroundColor: Constant.appColorAlpha(),
    margin: 3,
    borderRadius: 4.5,
  },
  searchMainView: {
    width: "100%",
    paddingTop: 10,
    paddingRight: 5,
    paddingLeft: 5,
    height: 50,
    marginBottom: 20,
    alignItems: "center",
    justifyContent: "center",
  },

  searchInner: {
    marginRight: 5,
    marginLeft: 5,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    elevation: 0,
    height: 45,
    backgroundColor: "#fff",
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  searchIcon: {
    marginLeft: 0,
    color: Constant.textColor(),
    textAlign: "left",
    left: 20,
    fontSize: 14,
    flex: 1,
  },

  articleitem: {
    width: width - 36,
    height: 120,
    borderRadius: 6,
    backgroundColor: "#fff",
    flexDirection: "row",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    elevation: 2,
  },
  articleTitle: { fontSize: 16 },
  articleSubmenu: {
    color: Constant.textAlpha(),
    fontSize: 13,
    marginTop: 10,
  },
  bestSellingsMain: {
    height: 170,
    width: 140,
    borderRadius: 6,
    padding: 4,
    paddingLeft: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  em: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  header: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  bottomFlatlist: {
    flex: 1,
    width: 120,
    borderRadius: 6,
    backgroundColor: "#fff",
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    marginRight: 2,
    paddingTop: 6,
  },
})
export default Home
