import React from "react"
import {
  View,
  Text,
  SafeAreaView,
  Dimensions,
  Animated,
  Easing,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from "react-native"
import BaseStyles from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import images from "./../BaseClass/Images"
import language from "./../BaseClass/language"
const { width, height } = Dimensions.get("window")
import SwitchSelector from "react-native-switch-selector"
import ImageLoad from "./../BaseClass/ImageLoader"

const options = [
  { label: "English", value: "en" },
  { label: "Italian", value: "sp" },
]

export default class Sidemenu extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isShow: true,
      languageCode: "en",
    }
  }

  componentDidMount() {}

  render() {
    let menuData = [
      {
        id: "1",
        title: language.home,
        screenName: "Home",

        image: images.home,
      },
      {
        id: "2",
        title: language.healthrecords,
        screenName: "HealthRecords",

        image: images.billing,
      },
      {
        id: "4",
        title: language.addresss,
        screenName: "Address",

        image: images.inventory,
      },
      {
        id: "5",
        title: language.healthArticles,
        screenName: "ArticleTabs",
        image: images.inventory,
      },
      {
        id: "5",
        title: language.medicationReminder,
        screenName: "AllMedicine",
        image: images.pillReminder,
      },
      {
        id: "5",
        title: language.orders,
        screenName: "Orders",
        image: images.inventory,
      },
      {
        id: "5",
        title: language.logout,
        screenName: "Logout",
        image: images.logout,
      },
    ]

    const userData = global.userData

    console.log(userData)

    return (
      <View
        style={[styles.container, { padding: 0 }]}>
        <View style={styles.container}>
          <View
            style={{
              position: "absolute",
              flex: 1,
              top: 0,
              bottom: 0,
              width: 150,
              justifyContent: "center",
              right: 0,
            }}>
            <View
              style={{
                position: "absolute",
                backgroundColor:
                  "rgba(255, 255, 255, 0.5)",
                right: 0,
                flex: 1,
                top: 100,
                bottom: 100,
                width: 105,
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
              }}></View>

            <View
              style={{
                position: "absolute",
                backgroundColor:
                  "rgba(255, 255, 255, 0.5)",
                right: 0,
                flex: 1,
                top: 80,
                bottom: 80,
                width: 125,
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
              }}></View>
          </View>
          <TouchableOpacity
            style={styles.Top}
            onPress={() => {
              this.props.onMenuSelected("Profile")
            }}>
            <View style={{ flexDirection: "row" }}>
              <View
                style={{
                  height: 70,
                  width: 70,
                  overflow: "hidden",
                  marginLeft: 15,
                  marginRight: 0,
                  alignItems: "center",
                  borderRadius: 35,
                  marginLeft: 0,
                }}>
                <ImageLoad
                  placeholderStyle={{
                    height: 70,
                    width: 70,
                    borderRadius: 35,
                  }}
                  style={{
                    height: 70,
                    width: 70,
                    borderRadius: 35,
                  }}
                  loadingStyle={{
                    size: "large",
                    color: Constant.appColorAlpha(),
                  }}
                  source={{
                    uri: userData.ProfilePicture,
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "space-between",
                  paddingTop: 10,
                  paddingBottom: 10,
                }}>
                <Text
                  style={[
                    styles.T1,
                    BaseStyles.headerStyleBilling2,
                  ]}>
                  {userData.Username}
                </Text>

                <Text
                  style={[
                    styles.T1,
                    BaseStyles.headerStyleBilling2,
                    { fontSize: 14 },
                  ]}>
                  {userData.Email}
                </Text>
              </View>
            </View>
            {/* <Text style = {[styles.T2,BaseStyles.headerStyleSideMenu]}>{this.state.userData != null ? this.state.userData.store.phonenumber : ''}</Text> */}
          </TouchableOpacity>

          <FlatList
            data={menuData}
            renderItem={({ item, index }) => (
              <View style={styles.s1}>
                <View>
                  <Image
                    tintColor={"#fff"}
                    source={item.image}
                    style={{
                      tintColor: "#fff",
                      height: 20,
                      width: 20,
                    }}
                  />
                </View>
                <TouchableOpacity
                  onPress={() =>
                    this.props.onMenuSelected(
                      item.screenName
                    )
                  }>
                  <Text
                    style={[
                      styles.renderItem,
                      BaseStyles.MediumFont,
                    ]}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
            keyExtractor={(item, index) => item.id}
          />

          <View>
            <View
              style={{ height: 40, width: 140 }}>
              <SwitchSelector
                options={options}
                initial={0}
                buttonColor={Constant.appColorAlpha()}
                textColor={Constant.appColorAlpha()}
                onPress={(value) => {
                  language.setLanguage(value)
                  // this.setState({languageCode:value})

                  this.forceUpdate()
                }}
              />
            </View>

            <Text
              style={[
                styles.version,
                { marginTop: 10 },
                BaseStyles.MediumFont,
              ]}>
              {language.version} - 1.3.13
            </Text>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    backgroundColor: Constant.headerColor(),
    padding: 20,
  },
  Top: {
    marginTop: 35,
  },
  T1: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 10,
  },
  T2: {
    color: "white",
    fontSize: 20,
    left: 30,
  },
  s1: {
    height: 50,
    top: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  renderItem: {
    color: "white",
    fontSize: 15,
    padding: 10,
  },
  version: {
    color: "white",
    marginBottom: 50,
  },
})
