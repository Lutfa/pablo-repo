import React, { useState } from "react"
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Animated,
  Dimensions,
} from "react-native"
import { useTheme } from "react-native-paper"
import { withNavigation } from "@react-navigation/compat"
import ImageLoad from "./../BaseClass/ImageLoader"
import { color } from "react-native-reanimated"
import images from "../BaseClass/Images"
const { width, height } = Dimensions.get("window")

function Banners({ bannersArray, navigation }) {
  const { colors, fonts } = useTheme()
  const [bannerPosition, setBannerPosition] =
    useState(0)

  const _onMomentumBanner = ({
    nativeEvent,
  }: any) => {
    const index = Math.round(
      nativeEvent.contentOffset.x / width
    )

    if (index !== bannerPosition) {
      setBannerPosition(index)
    }
  }

  return (
    <View
      style={{
        paddingRight: 5,
        paddingLeft: 5,
      }}>
      <View style={styles.bannerMain}>
        <ScrollView
          horizontal={true}
          scrollEventThrottle={16}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          onMomentumScrollEnd={_onMomentumBanner}>
          {bannersArray.map((item, i) => {
            // the _ just means we won't use that parameter
            return (
              <TouchableOpacity
                activeOpacity={2}
                onPress={() => {
                  if (item.NavigationId > 0) {
                    if (
                      item.BannerTypeName ==
                      "Category"
                    ) {
                      navigation.navigate(
                        "Categories",
                        {
                          parentId:
                            item.NavigationId,
                          isCategoris: true,
                        }
                      )
                    } else if (
                      item.BannerTypeName ==
                      "Manufacturer"
                    ) {
                      navigation.navigate(
                        "Categories",
                        {
                          parentId:
                            item.NavigationId,
                          isCategoris: false,
                        }
                      )
                    } else if (
                      item.BannerTypeName ==
                      "Product"
                    ) {
                      navigation.push(
                        "ProductDetails",
                        {
                          product: null,
                          productId:
                            item.NavigationId,
                        }
                      )
                    }
                  }
                }}
                key={i}
                style={styles.banneritem}>
                <ImageLoad
                  placeholderStyle={{
                    height: "100%",
                    width: "100%",
                  }}
                  style={{
                    height: "100%",
                    width: "100%",
                  }}
                  loadingStyle={{
                    size: "large",
                    color: colors.primary,
                  }}
                  source={{
                    uri: item.ImageUrl,
                  }}
                  //  source={images.testImage}
                />
              </TouchableOpacity>
            )
          })}
        </ScrollView>

        <View style={styles.bannerDotView}>
          {bannersArray.map((_, i) => {
            return (
              <Animated.View
                key={i}
                style={[
                  styles.bannerDot,
                  {
                    backgroundColor:
                      i == bannerPosition
                        ? colors.primary
                        : colors.primaryAlpha,
                  },
                ]}
              />
            )
          })}
        </View>
      </View>
    </View>
  )
}

Banners.defaultProps = {
  bannersArray: [],
}
export default withNavigation(Banners)

const styles = StyleSheet.create({
  bannerMain: {
    marginTop: 20,
    borderRadius: 6,
  },
  banneritem: {
    width: width - 30,
    height: 200,
    borderRadius: 6,
    overflow: "hidden",
  },
  bannerDotView: {
    flexDirection: "row",
    paddingBottom: 0,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 5,
    left: 0,
    right: 0,
  },
  bannerDot: {
    height: 9,
    width: 9,
    elevation: 2,
    margin: 3,
    borderRadius: 4.5,
  },
})
