import React, { Component } from "react"
import {
  View,
  Platform,
  AsyncStorage,
  Keyboard,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  StatusBar,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import Images from "./../BaseClass/Images"
import AnimateLoadingButton from "./../ExtraClass/LoadingButton"
const { width, height } = Dimensions.get("window")
import { useNavigation } from "@react-navigation/core"
import {
  getUniqueId,
  getManufacturer,
} from "react-native-device-info"
import DeviceInfo from "react-native-device-info"
import urls from "./../BaseClass/ServiceUrls"
import Toast from "react-native-simple-toast"
import LinearGradient from "react-native-linear-gradient"
import images from "./../BaseClass/Images"
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import SplashScreen from "react-native-splash-screen"
import { CommonActions } from "@react-navigation/native"
import language from "./../BaseClass/language"
import {
  TextField,
  OutlinedTextField,
} from "react-native-material-textfield"
import Db from "./../DB/Realm"
class Login extends Component {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props)
    this.state = {
      email: "",
      isMailCorrect: "",
      isPasswordCorrect: "",
      password: "",
      isLoading: false,
      showPassword: false,
      isEmail: false,
      isPhone: false,
    }

    // language.setLanguage("en")
  }

  componentDidMount() {
    SplashScreen.hide()
  }

  render() {
    const {
      email,
      isMailCorrect,
      password,
      isPasswordCorrect,
      isLoading,
      isEmail,
      isPhone,
    } = this.state
    return (
      <LinearGradient
        start={{ x: 0, y: 0.5 }}
        end={{ x: 0, y: 1 }}
        colors={[
          Constant.colorOne(),
          Constant.colorTwo(),
        ]}
        style={{ flex: 1 }}>
        {/* <KeyboardAwareScrollView extraHeight={90} extraScrollHeight={50}> */}
        <StatusBar
          translucent
          barStyle="light-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor={Constant.headerColor()}
        />
        <View
          pointerEvents={
            isLoading ? "none" : "auto"
          }
          style={styles.container}>
          <View
            style={{
              height: "40%",
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Image
              resizeMode={"contain"}
              style={{
                height: "100%",
                width: width - 20,
              }}
              source={images.logoBack}
            />
          </View>

          <View
            style={{
              padding: 15,
              paddingBottom: 20,
            }}>
            <TextField
              label={language.emailpassword}
              value={email}
              lineWidth={1}
              fontSize={15}
              labelFontSize={13}
              labelTextStyle={[
                { paddingTop: 5 },
                BaseStyle.regularFont,
              ]}
              baseColor={Constant.appFullColor()}
              tintColor={Constant.appFullColor()}
              textColor={"#fff"}
              onChangeText={(text) =>
                this.setState({
                  email: text,
                  isMailCorrect: "",
                  isEmail: false,
                  isPhone: false,
                })
              }
              error={isMailCorrect}
            />
            <View>
              <TextField
                label={language.password}
                value={password}
                lineWidth={1}
                fontSize={15}
                labelFontSize={13}
                secureTextEntry={true}
                labelTextStyle={[
                  { paddingTop: 5 },
                  BaseStyle.regularFont,
                ]}
                baseColor={Constant.appFullColor()}
                tintColor={Constant.appFullColor()}
                textColor={"#fff"}
                secureTextEntry={
                  !this.state.showPassword
                }
                onChangeText={(text) =>
                  this.setState({
                    password: text,
                    isPasswordCorrect: "",
                  })
                }
                error={isPasswordCorrect}
              />
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    showPassword:
                      !this.state.showPassword,
                  })
                }}
                style={{
                  position: "absolute",
                  top: 30,
                  height: 28,
                  width: 28,
                  right: 15,
                }}>
                <Image
                  tintColor={Constant.appFullColor()}
                  style={{
                    height: 28,
                    width: 28,
                  }}
                  source={
                    this.state.showPassword
                      ? Images.showPassword
                      : Images.hidePassword
                  }
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                width: "100%",
                paddingTop: 15,
              }}>
              <Text
                onPress={() => {
                  this.props.navigation.push(
                    "ForgotPassword"
                  )
                }}
                style={[
                  styles.forgot,
                  BaseStyle.regularFont,
                ]}>
                {language.forgotpassword}?
              </Text>
            </View>
          </View>

          <TouchableOpacity
            style={styles.startView}>
            <AnimateLoadingButton
              ref={(c) => (this.loadingButton = c)}
              width={width - 30}
              height={50}
              title={language.signin}
              titleFontSize={20}
              titleWeight={"100"}
              titleColor={Constant.appFullColor()}
              backgroundColor={"#fff"}
              borderRadius={6}
              onPress={() => {
                this.loginAction()
              }}
            />
          </TouchableOpacity>

          <View
            style={{
              height: "15%",
              width: "100%",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <Text
              onPress={() => {
                this.props.navigation.push(
                  "Register"
                )
              }}
              style={[
                { color: Constant.appFullColor() },
                BaseStyle.regularFont,
              ]}>
              {language.dontHaveAccount}
              <Text
                style={[
                  { fontSize: 16, color: "#fff" },
                  BaseStyle.boldFont,
                ]}>
                {language.signup}
              </Text>{" "}
            </Text>
          </View>
        </View>

        {/* </KeyboardAwareScrollView> */}
      </LinearGradient>
    )
  }

  async serviceCall() {
    const { email, password } = this.state
    let loginInputphone = "",
      loginInputemail = "",
      loginType = ""
    // if (this.state.isEmail) {
    //   loginInputemail = email;
    //   loginType = "email";

    // } else {
    //   loginType = "phoneNumber"
    //   loginInputphone = email;
    // }
    loginInputemail = email
    loginType = "email"
    const deviceToken = await AsyncStorage.getItem(
      "TOKEN"
    )
    let parameters = {
      LoginType: loginType,
      Email: loginInputemail,
      Password: password,
      PhoneNumber: loginInputphone,
      DeviceId: deviceToken,
      DeviceType:
        Platform.OS === "android"
          ? "Android"
          : "IOS",
      DeviceUniqueId: DeviceInfo.getUniqueId(),
      Version: DeviceInfo.getVersion(),
      DeviceModel: DeviceInfo.getModel(),
      DeviceBrand: DeviceInfo.getBrand(),
      DeviceVersion: DeviceInfo.getSystemVersion(),
    }
    console.log(
      "login params " + JSON.stringify(parameters)
    )
    Constant.postMethod(
      urls.login,
      parameters,
      (result) => {
        this.loadingButton.showLoading(false)
        this.setState({ isLoading: false })
        console.log(
          "login " + JSON.stringify(result)
        )
        if (result.success) {
          let response = result.result

          if (response.Status != "Fail") {
            let userId =
              response.Response.CustomerId
            global.CustomerId = userId

            let userData = response.Response
            global.userData = userData
            global.userToken = userData.AuthToken

            // this.saveUserData(response.Response)

            try {
              Db.write(() => {
                Db.create(
                  "User",
                  {
                    CustomerId: userData.CustomerId,
                    Username: userData.Username,
                    Email: userData.Email,
                    PhoneNumber:
                      userData.PhoneNumber,
                    DeviceType: userData.DeviceType,
                    Latitude: userData.Latitude,
                    Longitude: userData.Longitude,
                    IsPhoneVerified:
                      userData.IsPhoneVerified,
                    CountryCode:
                      userData.CountryCode,
                    ProfilePicture:
                      userData.ProfilePicture,
                    BloodGroup: userData.BloodGroup,
                    Height: `${userData.Height}`,
                    Weight: `${userData.Weight}`,
                    BloodPressure:
                      userData.BloodPressure,
                    Dob: userData.Dob,
                    Gender: userData.Gender,
                    SCProof: userData.SCProof,
                    SCVerified: userData.SCVerified,
                    PHCDocument:
                      userData.PHCDocument,
                    PHCVerified:
                      userData.PHCVerified,
                    FirstName: userData.FirstName,
                    LastName: userData.LastName,
                    DeviceId: userData.DeviceId,
                    DeviceUniqueId:
                      userData.DeviceUniqueId,
                    AuthToken: userData.AuthToken,
                    ReferralCode:
                      userData.ReferralCode,
                    Version: userData.Version,
                    DeviceModel:
                      userData.DeviceModel,
                    DeviceBrand:
                      userData.DeviceBrand,
                    DeviceVersion:
                      userData.DeviceVersion,
                  },
                  "modified"
                )

                AsyncStorage.setItem(
                  "userLoggedIn",
                  "true"
                )
                this.props.navigation.dispatch(
                  CommonActions.reset({
                    index: 0,
                    routes: [
                      {
                        name: "Home",
                      },
                    ],
                  })
                )
              })
            } catch (e) {
              console.log(`${e} Error on creation`)
              Toast.show("Something went wrong")
            }
          } else {
            Toast.show("Wrong credentials")
          }
        } else {
          Toast.show("Something went wrong")
        }
      }
    )
  }

  checkValidation() {
    const { email, password } = this.state

    console.log(email)

    if (Constant.isValidEmail(email)) {
      this.setState({
        isEmail: true,
        isPhone: false,
      })
      console.log("isemail " + this.state.isEmail)
      return true
    } else if (Constant.isValidPhone(email)) {
      this.setState({
        isPhone: true,
        isEmail: false,
      })
      console.log("isphone " + this.state.isPhone)
      return true
    } else {
      this.setState({
        isMailCorrect:
          "Enter Email or Phone number",
      })
      Toast.show("Enter Email or Phone number")

      return false
    }
  }

  checkPassword() {
    const { password } = this.state
    if (password == "") {
      this.setState({
        isPasswordCorrect: "Enter password",
      })
      Toast.show("Enter password")

      return false
    }
    return true
  }

  loginAction() {
    // this.props.navigation.dispatch(
    //     CommonActions.reset({
    //       index: 0,
    //       routes: [
    //         {
    //           name: 'Home',
    //         },
    //       ],
    //     })
    //   );

    let mail = this.checkValidation()
    let password = this.checkPassword()
    if (mail && password) {
      Keyboard.dismiss()
      this.setState({ isLoading: true })
      this.loadingButton.showLoading(true)
      this.serviceCall()
    } else if (!mail) {
      this.setState({ mailError: true })
    }

    // this.loadingButton.showLoading(true);

    // // mock
    // setTimeout(() => {
    //   this.loadingButton.showLoading(false);
    //   this.props.navigation.push('Register')

    // }, 1000);
  }

  saveUserData(data) {
    try {
      Db.write(() => {
        Db.create("User", {
          CustomerId: data.CustomerId,
        })

        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: "Home",
              },
            ],
          })
        )
      })
    } catch (e) {
      console.log("Error on creation")
      Toast.show("Unable to save data")
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: height,
    width: width,
    paddingTop: 50,
  },
  scrrenStyle: {
    marginTop: 10,
    width: width,
    height: "100%",
  },

  forgot: {
    color: Constant.appFullColor(),
    position: "absolute",
    right: 10,
    top: 17,
    fontSize: 13,
  },
  register: {
    marginTop: 25,
    color: Constant.appColorAlpha(),
    marginLeft: 15,
    fontSize: 16,
  },

  startView: {
    marginTop: 20,
    borderRadius: 25,
    height: 50,
    marginLeft: 15,
    marginRight: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  startBtn: {
    color: "#fff",
    fontSize: 16,
  },
  pageNoStyle: {
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  pageStyle: {
    width: 60,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  dotStyle: {
    marginTop: 5,
    height: 6,
    width: 6,
    backgroundColor: Constant.appColorAlpha(),
    borderRadius: 3,
  },
  title: {
    marginLeft: 20,
    marginRight: 20,
    lineHeight: 30,
    fontSize: 20,
    color: Constant.appColorAlpha(),
  },
  subtitle: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    lineHeight: 20,
  },
  pageCount: {
    fontSize: 16,
  },

  startBtn: {
    color: "#fff",
    fontSize: 16,
  },
})

export default Login
