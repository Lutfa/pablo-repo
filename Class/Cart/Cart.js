import React, { Component } from "react"
import {
  View,
  Button,
  Modal,
  BackHandler,
  ImageBackground,
  AppState,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import SwipeListView from "./../ExtraClass/SwipeList/SwipeListView"
import Db from "./../DB/Realm"
const { width, height } = Dimensions.get("window")
import ImageLoad from "./../BaseClass/ImageLoader"
import CartDb from "./../DB/CartDb"
import urls from "./../BaseClass/ServiceUrls"
import language from "./../BaseClass/language"
import PickImage from "./../BaseClass/PickImage"
import Toast from "react-native-simple-toast"
import NoData from "../BaseClass/NoData"

class Cart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isSpeaking: false,
      test: false,
      wishlistArray: [],
      showloader: false,
      image: "",
      isPrescription: false,
      isPickImage: false,
    }
    this.handleBackButtonClick =
      this.handleBackButtonClick.bind(this)
  }

  componentDidMount() {
    let cartData = Db.objects("CartDB")
    this.setState({ wishlistArray: cartData })
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    )
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    )
  }

  handleBackButtonClick() {
    // if(wishlistArray.length > 0)
    // {
    // this.addToCart(true)
    // }
    // else{
    this.props.navigation.goBack(null)
    // }
    // return true;
  }

  render() {
    const { article, isSpeaking, showloader } =
      this.state

    let address = Db.objects("Address")

    const { wishlistArray, isPickImage } =
      this.state
    return (
      <View style={{ flex: 1 }}>
        {/* <SafeAreaView edges={[ 'bottom',]} style={{flex:0,backgroundColor:Constant.headerColor()}}/> */}

        <SafeAreaView
          style={{
            flex: 1,
            backgroundColor: "#fff",
          }}>
          {showloader
            ? Constant.showLoader("Updating cart")
            : null}

          <View
            style={{
              backgroundColor:
                Constant.headerColor(),
              elevation: 4,
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <View
              style={{
                paddingTop: 10,
                paddingBottom: 15,
                backgroundColor:
                  Constant.headerColor(),
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                borderBottomLeftRadius: 30,
                borderBottomRightRadius: 30,
              }}>
              <TouchableOpacity
                onPress={() => {
                  // if(wishlistArray.length > 0)
                  // {
                  // this.addToCart(true)
                  // }
                  // else{
                  this.props.navigation.goBack(null)
                  // }
                }}
                style={{
                  marginLeft: 15,
                  height: 35,
                  borderRadius: 15,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Image
                  tintColor="#fff"
                  style={{
                    tintColor: "#fff",
                    height: 20,
                    width: 20,
                    left: 0,
                  }}
                  source={images.back}
                />
              </TouchableOpacity>
              <Text
                style={[
                  { flex: 1 },
                  BaseStyle.headerFont,
                ]}>
                {language.cart.toUpperCase()}
              </Text>
            </View>
          </View>

          {(wishlistArray || []).length > 0 ? (
            <SwipeListView
              data={wishlistArray}
              keyExtractor={(item) => item.Id}
              disableRightSwipe
              renderItem={(data) => (
                <View
                  key={data.index}
                  style={{
                    width: "100%",
                    padding: 10,
                    marginBottom:
                      data.index ==
                      wishlistArray.length - 1
                        ? 50
                        : 0,
                  }}>
                  <View
                    style={{
                      elevation: 4,
                      borderRadius: 8,
                      backgroundColor: "#fff",
                      flexDirection: "row",
                      height: 114,
                    }}>
                    <ImageLoad
                      resizeMode={"center"}
                      placeholderStyle={{
                        width: 100,
                        height: 110,
                      }}
                      style={{
                        width: 100,
                        height: 110,
                      }}
                      loadingStyle={{
                        size: "large",
                        color:
                          Constant.appColorAlpha(),
                      }}
                      source={{
                        uri:
                          data.item.Images.length >
                          0
                            ? data.item.Images[0]
                                .Src
                            : "null",
                      }}
                      placeholderSource={
                        images.placeholder
                      }
                    />
                    {/* <Image resizeMode={'center'} style = {{width:100,height:110}} source = {images.testNew}/> */}
                    <View
                      style={{
                        flex: 1,
                        marginLeft: 5,
                        marginTop: 15,
                        justifyContent:
                          "space-between",
                        marginBottom: 15,
                      }}>
                      <View>
                        <Text
                          numberOfLines={1}
                          style={[
                            { fontSize: 16 },
                            BaseStyle.boldFont,
                          ]}>
                          {data.item.Name}
                        </Text>
                        <Text
                          numberOfLines={2}
                          style={[
                            {
                              lineHeight: 15,
                              fontSize: 13,
                              marginTop: 2,
                              color:
                                Constant.textAlpha(),
                            },
                            BaseStyle.regularFont,
                          ]}>
                          {
                            data.item
                              .ShortDescription
                          }
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent:
                            "space-between",
                          width: "94%",
                          paddingTop: 8,
                        }}>
                        <Text
                          style={[
                            { fontSize: 18 },
                            BaseStyle.boldFont,
                          ]}>
                          {Constant.price()}
                          {parseFloat(
                            data.item.Price
                          ).toFixed(2)}
                        </Text>

                        <View
                          style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent:
                              "space-between",
                            width: 65,
                          }}>
                          <TouchableOpacity
                            onPress={() => {
                              if (
                                data.item.count -
                                  1 >
                                0
                              ) {
                                try {
                                  Db.write(() => {
                                    data.item.count =
                                      data.item
                                        .count - 1
                                  })
                                } catch (e) {
                                  console.log(
                                    "Error on creation"
                                  )
                                }
                                // data.item.count = data.item.count-1
                                this.setState({
                                  test: true,
                                })
                              }
                            }}>
                            <Image
                              resizeMode={"contain"}
                              style={{
                                height: 21,
                                width: 21,
                              }}
                              source={images.pMinus}
                            />
                          </TouchableOpacity>

                          <Text
                            style={[
                              {
                                fontSize: 14,
                                color: "#000",
                              },
                              BaseStyle.boldFont,
                            ]}>
                            {data.item.count}
                          </Text>

                          <TouchableOpacity
                            onPress={() => {
                              try {
                                Db.write(() => {
                                  data.item.count =
                                    data.item
                                      .count + 1
                                })
                              } catch (e) {
                                console.log(
                                  "Error on creation"
                                )
                              }
                              //  data.item.count = data.item.count+1
                              this.setState({
                                test: true,
                              })
                            }}>
                            <Image
                              resizeMode={"contain"}
                              style={{
                                height: 21,
                                width: 21,
                              }}
                              source={images.pPlus}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              )}
              renderHiddenItem={(data, rowMap) => (
                <TouchableOpacity
                  onPress={() => {
                    this.closeRow(rowMap, data.item)
                    let isDeleted = CartDb.delete(
                      data.item.Id
                    )
                    this.setState({ test: true })
                  }}
                  key={data.item.Id}
                  style={{
                    width: width - 20,
                    position: "absolute",
                    borderRadius: 8,
                    right: 10,
                    marginTop: 10,
                    paddingBottom: 10,
                    height: 114,
                    backgroundColor:
                      Constant.headerColor(),
                  }}>
                  <View
                    style={{
                      width: 90,
                      position: "absolute",
                      right: 10,
                      height: "100%",
                      borderTopRightRadius: 8,
                      borderBottomRightRadius: 8,
                      justifyContent: "center",
                      alignItems: "center",
                      backgroundColor:
                        Constant.headerColor(),
                    }}>
                    <Image
                      tintColor={"#fff"}
                      style={{
                        height: 20,
                        width: 20,
                      }}
                      source={images.delete}
                    />
                    <Text
                      style={[
                        {
                          fontSize: 13,
                          marginTop: 2,
                          marginTop: 5,
                          color:
                            Constant.selectedTextColor(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      Delete
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              leftOpenValue={0}
              rightOpenValue={-100}
            />
          ) : (
            <NoData />
          )}

          {wishlistArray.length > 0 ? (
            <View
              style={{
                height: 150,
                width: "100%",
                padding: 10,
                backgroundColor: "#fff",
                elevation: 4,
              }}>
              <View
                style={{
                  height: 30,
                  width: "100%",
                  flexDirection: "row",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 16,
                      flex: 1,
                      marginTop: 2,
                      color: Constant.textAlpha(),
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.totalAmount}
                </Text>
                <Text
                  style={[
                    {
                      fontSize: 18,
                      marginRight: 5,
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {Constant.price()}
                  {parseFloat(
                    this.getTotal()
                  ).toFixed(2)}
                </Text>
              </View>

              <View
                style={{
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.addToCart(false)
                  }}
                  style={{
                    height: 50,
                    marginTop: 10,
                    width: "85%",
                    borderRadius: 25,
                    backgroundColor:
                      Constant.appColorAlpha(),
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 18,
                        color: "#fff",
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {address.length > 0
                      ? language.checkOut.toUpperCase()
                      : language.addAddress}
                  </Text>
                </TouchableOpacity>
              </View>

              <Text
                onPress={() => {
                  this.props.navigation.popToTop()
                }}
                style={[
                  {
                    fontSize: 16,
                    flex: 1,
                    textAlign: "center",
                    marginTop: 10,
                    color: Constant.textColor(),
                  },
                  BaseStyle.regularFont,
                ]}>
                {language.continueShopping}
              </Text>
            </View>
          ) : null}

          {this.prescriptionUi()}

          {isPickImage ? (
            <PickImage
              selectedImage={this.selectedImage}
              onBack={this.hidePicker}
            />
          ) : null}
        </SafeAreaView>
      </View>
    )
  }

  hidePicker = () => {
    this.setState({ showOptions: false })
  }
  selectedImage = (url) => {
    this.setState({
      image: url,
      isPickImage: false,
    })
  }

  getTotal() {
    let toalPrice = 0
    this.state.wishlistArray.map((item) => {
      let oldPrice =
        item.count * parseFloat(item.Price)

      toalPrice = toalPrice + parseFloat(oldPrice)
    })

    return toalPrice
  }

  prescriptionUi() {
    let address = Db.objects("Address")
    const { image, isPrescription } = this.state
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={isPrescription}
        onRequestClose={() => {
          this.setState({ isPopup: false })
        }}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            height: height,
          }}>
          <TouchableOpacity
            activeOpacity={2}
            onPress={() => {
              this.setState({
                isPrescription: false,
              })
            }}
            style={{
              height: height + 200,
              backgroundColor: "rgba(0,0,0,0.5)",
              position: "absolute",
              bottom: 0,
              top: 0,
              left: 0,
              right: 0,
            }}></TouchableOpacity>

          <View
            style={{
              height: 400,
              width: 300,
              backgroundColor: "white",
              flexDirection: "column",
            }}>
            <TouchableOpacity
              onPress={() =>
                this.setState({ isPickImage: true })
              }
              style={{
                backgroundColor: "#fff",
                borderColor: "black",
                flex: 1,
                borderWidth: 1,
                borderRadius: 6,
                borderStyle: "dashed",
                alignItems: "center",
                justifyContent: "center",
              }}>
              {image ? (
                <Image
                  source={{ uri: image }}
                  style={{
                    height: 300,
                    width: 260,
                    marginTop: 20,
                  }}
                />
              ) : (
                <View
                  style={{
                    height: 50,
                    width: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "black",
                    borderWidth: 1,
                    borderRadius: 6,
                    bottom: 10,
                    borderStyle: "dashed",
                  }}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                    }}
                    source={images.pPlus}
                  />
                </View>
              )}
              {image ? null : (
                <Text
                  style={[
                    styles.Text9,
                    BaseStyle.boldFont,
                  ]}>
                  {language.takePrescription}
                </Text>
              )}
            </TouchableOpacity>

            <View
              style={{
                height: 60,
                width: "100%",
                padding: 8,
                paddingLeft: 20,
                paddingRight: 20,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.nextScreen("")
                }}
                style={{
                  flex: 0.46,
                  borderRadius: 22,
                  borderColor:
                    Constant.appColorAlpha(),
                  padding: 5,
                  borderWidth: 1,
                  height: 44,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 14,
                      textAlign: "center",
                      color:
                        Constant.appColorAlpha(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.skip}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  if (!image) {
                    Toast.show(
                      "Please add Prescription or skip"
                    )
                  } else {
                    this.nextScreen(image)
                  }
                }}
                style={{
                  flex: 0.46,
                  borderRadius: 22,
                  backgroundColor:
                    Constant.appColorAlpha(),
                  padding: 5,
                  height: 44,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 14,
                      textAlign: "center",
                      color: "#fff",
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.next}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  nextScreen(url) {
    this.setState({
      isPrescription: false,
      isPickImage: false,
    })

    let address = Db.objects("Address")
    this.props.navigation.navigate("CreateOrder", {
      address: address.length > 0 ? address[0] : "",
      prescriptionUrl: url,
    })
  }

  addToCart(isBack) {
    // this.setState({isLoading:true})

    const { wishlistArray } = this.state
    let address = Db.objects("Address")
    if (address.length > 0) {
      if (wishlistArray.length <= 0) {
        return
      }

      this.setState({ showloader: true })

      let dataArray = []

      wishlistArray.map((item) => {
        let dic = {
          Id: 0,
          Quantity: item.count,
          ProductId: item.Id,
          ShoppingCartTypeId: 1,
          //  1- Cart, 2-WishList
        }
        dataArray.push(dic)
      })

      let params = {
        StoreId: 1,
        CustomerId: global.CustomerId,
        OrderItems: dataArray,
      }
      Constant.postMethod(
        urls.addToCart,
        params,
        (result) => {
          // this.setState({isLoading:false})
          console.log("cart res "+JSON.stringify(result))
          this.setState({ showloader: false })

          if (result.success) {
           
            if (
              result.result.Response == true ||
              result.result.Response == "true"
            ) {
              if (isBack) {
                this.props.navigation.pop(1)
              } else {
                let presArray = Db.objects(
                  "CartDB"
                ).filtered(
                  "ColorCode = $0",
                  Constant.prescriptionColorCode()
                )
                if (presArray.length > 0) {
                  this.setState({
                    isPrescription: true,
                  })
                } else {
                  this.nextScreen("")
                }
              }
            }
          }
        }
      )
    } else {
      this.props.navigation.navigate("AddAddress", {
        data: null,
      })
    }
  }

  closeRow(rowMap, rowKey) {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow()
    }
  }

  selectedAddress = () => {
    this.forceUpdate()
  }
}

const styles = StyleSheet.create({
  Text9: {
    fontWeight: "bold",
    // top: 180
  },
  View2: {
    left: 20,
    bottom: 0,
    top: 5,
    height: 40,
    width: 100,
    //backgroundColor:'white'
    backgroundColor: "#fff",
  },
})
export default Cart
