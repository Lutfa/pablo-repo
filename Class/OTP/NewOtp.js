import React, { Component } from "react"
import {
  View,
  Platform,
  AsyncStorage,
  Keyboard,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "../BaseClass/BaseStyles"
import Constant from "../BaseClass/Constant"
import Images from "../BaseClass/Images"
import TextField from "../Login/lib/TextField"
import AnimateLoadingButton from "../ExtraClass/LoadingButton"
const { width, height } = Dimensions.get("window")
import { useNavigation } from "@react-navigation/core"
import Db from "./../DB/Realm"
import {
  getUniqueId,
  getManufacturer,
} from "react-native-device-info"
import DeviceInfo from "react-native-device-info"
import urls from "../BaseClass/ServiceUrls"
import LinearGradient from "react-native-linear-gradient"
import images from "../BaseClass/Images"
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view"
import { TextInput } from "react-native-gesture-handler"
import { CommonActions } from "@react-navigation/native"
import auth from "@react-native-firebase/auth"
import Toast from "react-native-simple-toast"
import SplashScreen from "react-native-splash-screen"
import OTPInputView from "@twotalltotems/react-native-otp-input"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"

class NewOtp extends Component {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props)
    this.state = {
      dataArray: ["", "", "", "", "", ""],
      showSuccess: false,
      currentIndex: 0,
      number: props.route.params.number,
      countryCode: props.route.params.countryCode,
      parameters: props.route.params.parameters,
      confirmResult: "",
      setConfirm: "",
      confirm: "",
      code: "",
      promptSmsCode: false,
      timer: 30,
      code: "",
    }
  }

  componentDidMount() {
    SplashScreen.hide()
    this.interval = setInterval(() => {
      this.setState((prevState) => ({
        timer: prevState.timer - 1,
      }))
    }, 1000)

    auth().signOut()
    this.initFirebase()

    this.sendOtp(
      this.state.countryCode,
      this.state.number
    )
  }

  componentDidUpdate() {
    if (this.state.timer === 0) {
      clearInterval(this.interval)
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
    if (this.unsubscribe) this.unsubscribe()
  }

  initFirebase() {
    this.unsubscribe = auth().onAuthStateChanged(
      (user) => {
        if (user && this.state.promptSmsCode) {
          this.setState({
            showLoader: true,
            isCallingService: true,
          })
          // this.serviceCall()
        } else {
          // User has been signed out, reset the state
          this.setState({
            confirmResult: null,
          })
        }
      }
    )
  }

  async sendOtp(countryCode, phoneNumber) {
    await auth()
      .signInWithPhoneNumber(
        countryCode + phoneNumber,
        true
      )
      .then((confirmResult) => {
        this.showAlert()
        this.setState({
          confirmResult: confirmResult,
          promptSmsCode: true,
        })
      })
      .catch((error) => {
        console.log(error)
        Toast.show(
          "Something went wrong",
          Toast.LONG
        )
      })
  }

  showAlert() {
    this.setState({ showSuccess: true })
    setTimeout(() => {
      this.setState({ showSuccess: false })
    }, 1500)
  }

  serviceCall() {
    const { parameters } = this.state

    Constant.postMethod(
      urls.registerCustomer,
      parameters,
      (result) => {
        this.setState({ isLoading: false })
        this.loadingButton.showLoading(false)
        console.log(
          "servicecall",
          JSON.stringify(result)
        )
        if (result.success) {
          let response = result.result

          if (
            response != null &&
            response.Status != "Fail"
          ) {
            let userId =
              response.Response.CustomerId
            let userData = response.Response

            global.CustomerId = userId
            global.userData = userData
            global.userToken = userData.AuthToken
            console.log("userData", userData)

            // this.saveUserData(response.Response)

            try {
              Db.write(() => {
                Db.create(
                  "User",
                  {
                    CustomerId: userData.CustomerId,
                    Username: userData.Username,
                    Email: userData.Email,
                    PhoneNumber:
                      userData.PhoneNumber,
                    DeviceType: userData.DeviceType,
                    Latitude: userData.Latitude,
                    Longitude: userData.Longitude,
                    IsPhoneVerified:
                      userData.IsPhoneVerified,
                    CountryCode:
                      userData.CountryCode,
                    ProfilePicture:
                      userData.ProfilePicture,
                    BloodGroup: userData.BloodGroup,
                    Height: `${userData.Height}`,
                    Weight: `${userData.Weight}`,
                    BloodPressure:
                      userData.BloodPressure,
                    Dob: userData.Dob,
                    Gender: userData.Gender,
                    HealthIssues:
                      userData.HealthIssues,
                    SCProof: userData.SCProof,
                    SCVerified: userData.SCVerified,
                    PHCDocument:
                      userData.PHCDocument,
                    PHCVerified:
                      userData.PHCVerified,
                    FirstName: userData.FirstName,
                    LastName: userData.LastName,
                    DeviceId: userData.DeviceId,
                    DeviceUniqueId:
                      userData.DeviceUniqueId,
                    AuthToken: userData.AuthToken,
                    ReferralCode:
                      userData.ReferralCode,
                    Version: userData.Version,
                    DeviceModel:
                      userData.DeviceModel,
                    DeviceBrand:
                      userData.DeviceBrand,
                    DeviceVersion:
                      userData.DeviceVersion,
                  },
                  "modified"
                )
                AsyncStorage.setItem(
                  "userLoggedIn",
                  "true"
                )
                this.props.navigation.navigate(
                  "EditProfile",
                  { isHome: true }
                )

                //   this.props.navigation.dispatch(
                // CommonActions.reset({
                //   index: 0,
                //   routes: [
                //     {
                //       name: 'Home',
                //     },
                //   ],
                // })
                //);
              })
            } catch (e) {
              console.log("Error on creation")
              Toast.show("Unable to save data")
            }

            // this.props.navigation.dispatch(
            //           CommonActions.reset({
            //             index: 0,
            //             routes: [
            //               {
            //                 name: 'Home',
            //               },
            //             ],
            //           })
            //         );
          } else {
            Toast.show("Something went wrong")
          }
        } else {
        }
      }
    )
  }

  async confirmCode(codeInput) {
    const { confirmResult } = this.state

    try {
      console.log(
        "confirming  code" +
          confirmResult._verificationId
      )

      await confirmResult
        .confirm(codeInput)
        .then((auth) => {
          console.log(auth)
          this.setState({ isLoading: true })

          // this.props.navigation.navigate('Home');
          this.serviceCall()

          this.setState({
            showLoader: true,
            isCallingService: false,
          })
          //this.registerUser()
        })
        .catch((err) => {
          console.log(err)
          this.loadingButton.showLoading(false)

          this.setState({
            isCallingService: false,
            isLoading: false,
          })
          Toast.show(
            "Something went wrong",
            Toast.LONG
          )
        })
    } catch (error) {
      console.log("Invalid code.")
      this.setState({ isLoading: false })
      Toast.show(JSON.stringify(error), Toast.LONG)
    }
  }

  render() {
    const { timer, isLoading } = this.state
    return (
      <LinearGradient
        start={{ x: 0, y: 0.5 }}
        end={{ x: 0, y: 1 }}
        colors={[
          Constant.colorOne(),
          Constant.colorTwo(),
        ]}
        style={{ flex: 1 }}>
        <KeyboardAwareScrollView>
          <View
            pointerEvents={
              isLoading ? "none" : "auto"
            }
            style={styles.container}>
            <View
              style={{
                height: "48%",
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                resizeMode={"contain"}
                style={{
                  height: "100%",
                  width: width - 20,
                }}
                source={images.logoBack}
              />
            </View>

            <View
              style={{
                height: 60,
                paddingTop: 30,
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
              }}>
              <Text
                style={[
                  { color: "#fff", fontSize: 20 },
                  BaseStyle.boldFont,
                ]}>
                OTP
              </Text>
              <Text
                style={[
                  {
                    color: "#fff",
                    fontSize: 16,
                    marginTop: 8,
                    lineHeight: 20,
                    textAlign: "center",
                  },
                  BaseStyle.regularFont,
                ]}>
                Enter 6 digit verification code{" "}
                {"\n"} sent to your number
              </Text>
            </View>

            <View
              style={{
                padding: 15,
                paddingBottom: 50,
                height: "20%",
                alignItems: "center",
              }}>
              {/* {this.createingOtp()} */}

              <OTPInputView
                style={{
                  width: "80%",
                  height: 60,
                  marginTop: 10,
                }}
                pinCount={6}
                placeholderTextColor={Constant.appColorAlpha()}
                // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                onCodeChanged={(code) => {
                  this.setState({ code })
                }}
                autoFocusOnLoad
                codeInputFieldStyle={[
                  styles.underlineStyleBase,
                  BaseStyle.boldFont,
                ]}
                codeInputHighlightStyle={
                  styles.underlineStyleHighLighted
                }
                onCodeFilled={(code) => {
                  this.setState({ code })
                  console.log(
                    `Code is ${code}, you are good to go!`
                  )
                }}
              />

              <Text
                style={[
                  {
                    color: "#fff",
                    fontSize: 15,
                    marginTop: 20,
                    width: "100%",
                    textAlign: "center",
                  },
                  BaseStyle.boldFont,
                ]}>
                {" "}
                {timer == 0
                  ? `Resend`
                  : `Resend code in 00:${timer}`}
              </Text>
            </View>

            <TouchableOpacity
              style={styles.startView}>
              <AnimateLoadingButton
                ref={(c) =>
                  (this.loadingButton = c)
                }
                width={width - 30}
                height={50}
                title="Continue"
                titleFontSize={20}
                titleWeight={"100"}
                titleColor={Constant.appFullColor()}
                backgroundColor={"#fff"}
                borderRadius={6}
                onPress={() => {
                  this.sendAction()
                }}
              />
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>

        {Constant.showSuccess(
          "OTP sent successfully",
          this.state.showSuccess
        )}
      </LinearGradient>
    )
  }

  sendAction() {
    // this.props.navigation.dispatch(
    //     CommonActions.reset({
    //       index: 0,
    //       routes: [
    //         {
    //           name: 'Home',
    //         },
    //       ],
    //     })
    //   );

    if (
      this.state.code != "" &&
      this.state.code.length == 6
    ) {
      console.log("otp success ")
      this.loadingButton.showLoading(true)
      this.setState({ isLoading: true })
      this.confirmCode(this.state.code)
    } else {
      this.setState({ isLoading: false })

      Toast.show("Please enter OTP")

      //this.serviceCall()

      this.setState({
        showLoader: true,
        isCallingService: false,
      })
    }

    // // mock
    // setTimeout(() => {
    //   this.loadingButton.showLoading(false);
    //   this.props.navigation.push('Register')

    // }, 1000);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: height,
    width: width,
    paddingTop: 50,
  },
  scrrenStyle: {
    marginTop: 10,
    width: width,
    height: "100%",
  },

  forgot: {
    color: Constant.appFullColor(),
    position: "absolute",
    right: 10,
    top: 17,
    fontSize: 13,
  },
  register: {
    marginTop: 25,
    color: Constant.appColorAlpha(),
    marginLeft: 15,
    fontSize: 16,
  },

  startView: {
    marginTop: 20,
    borderRadius: 25,
    height: 50,
    marginLeft: 15,
    marginRight: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  startBtn: {
    color: "#fff",
    fontSize: 16,
  },
  underlineStyleBase: {
    color: "#fff",

    height: 35,
    width: 35,
    borderRadius: 6,
    backgroundColor: Constant.headerColor(),
    elevation: 2,
    margin: 4,
    justifyContent: "center",
    alignItems: "center",
  },
  underlineStyleHighLighted: {},
  pageNoStyle: {
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  pageStyle: {
    width: 60,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  dotStyle: {
    marginTop: 5,
    height: 6,
    width: 6,
    backgroundColor: Constant.appColorAlpha(),
    borderRadius: 3,
  },
  title: {
    marginLeft: 20,
    marginRight: 20,
    lineHeight: 30,
    fontSize: 20,
    color: Constant.appColorAlpha(),
  },
  subtitle: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    lineHeight: 20,
  },
  pageCount: {
    fontSize: 16,
  },

  startBtn: {
    color: "#fff",
    fontSize: 16,
  },
})

export default NewOtp
