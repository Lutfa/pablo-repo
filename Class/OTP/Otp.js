import React,{Component}from "react"
import { View,Image ,Text,ScrollView,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from './../BaseClass/BaseStyles'
import  Constant from './../BaseClass/Constant'
import images from './../BaseClass/Images'
import auth from '@react-native-firebase/auth';
import Toast from 'react-native-simple-toast';
import { useNavigation } from '@react-navigation/native';
import urls from './../BaseClass/ServiceUrls'
const { width,height } = Dimensions.get('window');

class Otp extends Component
{

    constructor(props)
    {
        super(props);
        this.state={
            dataArray : ['','','','','',''],
            showSuccess:false,
            currentIndex:0,
            number: props.route.params.number,
            countryCode: props.route.params.countryCode,
            parameters: props.route.params.parameters,
            confirmResult:'',
            setConfirm:'',
            confirm:'',
            code:'',
            promptSmsCode:false
        }

        // this.initFirebase()
    }
    componentDidMount() {
   
      auth().signOut();
      this.initFirebase();

      this.sendOtp(this.state.countryCode,this.state.number)
  
    }

    showAlert()
    {
        this.setState({showSuccess:true})
        setTimeout(() => {
            this.setState({showSuccess:false})

          }, 1500);
         
    }
  
    
  


    initFirebase() {
      
      this.unsubscribe = auth().onAuthStateChanged((user) => {
        if (user && this.state.promptSmsCode) {
        this.setState({showLoader: true,isCallingService : true});
         this.serviceCall()
        } else {
          // User has been signed out, reset the state
          this.setState({
            confirmResult: null,
          });
        }
      });
    }

    componentWillUnmount() {
      if (this.unsubscribe) this.unsubscribe();
    }

  async sendOtp(countryCode,phoneNumber)
  {


     
    await auth()
      .signInWithPhoneNumber(countryCode+phoneNumber,true)
      .then(confirmResult => {
          this.showAlert()
          this.setState({confirmResult : confirmResult,promptSmsCode:true});
      })
      .catch(error =>{
        console.log(error);
       Toast.show('Something went wrong', Toast.LONG);

        
       }
      );
    
  }


  serviceCall()
  {

   
    const {parameters} = this.state

    Constant.postMethod(urls.registerCustomer,parameters, (result) => {
      if(result.success)
      {
         let response = result.result
         
         if(response != null && response.Status != 'Fail')
         {
           let userId = response.Response.CustomerId
           AsyncStorage.setItem('CustomerId', `${userId}`);

         this.props.navigation.navigate('Home');

         }
         else
         {
             Toast.show('Wrong credentials');

         }
      }
      else
      {
         Toast.show('Wrong credentials');

      }
     })
  }

  async confirmCode(codeInput){

    const {confirmResult} = this.state;

     try {


      console.log('confirming  code' + confirmResult._verificationId); 

    await confirmResult.confirm(codeInput).then((auth) => {
        console.log(auth)
        // this.props.navigation.navigate('Home');
        this.serviceCall()

         this.setState({showLoader: true,isCallingService:false});
         //this.registerUser()

 }).catch(err => { 
   console.log(err)
              this.setState({isCallingService:false});
       Toast.show('Something went wrong', Toast.LONG);

   })
    } catch (error) {
      console.log('Invalid code.');
    }

   
  };
   

    render()
    {
        return(
            <View style ={styles.container}>
         <View style ={{padding:15,height:200,marginTop:50, justifyContent:'center',alignItems:'center'}}>
        <Text style = {{color:Constant.appColorAlpha(),fontSize:14,lineHeight: 20,}}>OTP has been sent to <Text style ={[styles.title,BaseStyle.boldFont]}>{this.state.countryCode+' '+this.state.number}{'\n'}</Text>
               Please check your message box and submit.</Text>
        {this.createingOtp()}


        </View>
        {
                    Constant.showSuccess('OTP sent successfully',this.state.showSuccess)
          }
        <View style = {{alignItems:'center',justifyContent:'center'}}>

        <Text 
        onPress={()=>{
          // this.sendOtp(this.state.countryCode,this.state.number)
        }}
         style = {{color:Constant.appColorAlpha(),fontSize:14,lineHeight: 20,marginTop:25}}>Dont receive the OTP ? <Text style ={[styles.title,BaseStyle.boldFont,{color:Constant.appColorAlpha()}]}>RESEND OTP</Text></Text>
      </View>

        {this.createNumberPad()}

            </View>

        )
    }

    createingOtp()
    {
        const {dataArray,currentIndex} = this.state

          return(

                    <View style = {{flexDirection:'row',alignItems:'center',justifyContent:'center',marginTop:20}}>
                        {
                  dataArray.map((item, index) => (
                  
                    <View key={index} style = {{height:40,width:40,borderRadius:20 ,borderWidth:1,borderColor:Constant.appColorAlpha() ,margin:4,justifyContent:'center',alignItems:'center'}}>

                   <Text style = {[styles.otpNumber,BaseStyle.boldFont]}>{item}</Text>
                    </View> 
                  ))
                    }
                  </View>
               
)
    }


    addData(number)
    {
        if(this.state.currentIndex <= 5)
        {
        let NewData = this.state.dataArray.map((item,index)=>{
            return index == this.state.currentIndex ? number : item
        })
        this.setState({dataArray:NewData,currentIndex:this.state.currentIndex+1})
    }
    }
    removeData()
    {
        if(this.state.currentIndex > 0)
        {
        let NewData = this.state.dataArray.map((item,index)=>{
            return index == this.state.currentIndex-1 ? '' : item
        })
        this.setState({dataArray:NewData,currentIndex:this.state.currentIndex-1})
    }
    }

    createNumberPad()
    {
        return(
            <View style = {{position:'absolute',bottom:0,left:0,right:0,height:'45%'}}>
            <View style = {{width:'100%',height:'25%',flexDirection:'row',justifyContent:'center'}}>
              <TouchableOpacity 
              onPress={()=>{  
                this.addData(1)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>1</Text>
              </View>
              </TouchableOpacity>
              
              
              <TouchableOpacity 
              onPress={()=>{
                this.addData(2)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>2</Text>
              </View>
              </TouchableOpacity>

              <TouchableOpacity 
              onPress={()=>{
                this.addData(3)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>3</Text>
              </View>
              </TouchableOpacity>



            </View>
           
            <View style = {{width:'100%',height:'25%',flexDirection:'row',justifyContent:'center'}}>
              <TouchableOpacity 
              onPress={()=>{
                this.addData(4)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>4</Text>
              </View>
              </TouchableOpacity>
              
              
              <TouchableOpacity 
              onPress={()=>{
                this.addData(5)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>5</Text>
              </View>
              </TouchableOpacity>

              <TouchableOpacity 
              onPress={()=>{
                this.addData(6)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>6</Text>
              </View>
              </TouchableOpacity>
              


            </View>

            <View style = {{width:'100%',height:'25%',flexDirection:'row',justifyContent:'center'}}>
              <TouchableOpacity 
              onPress={()=>{
                this.addData(7)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>7</Text>
              </View>
              </TouchableOpacity>
              
              
              <TouchableOpacity 
              onPress={()=>{
                this.addData(8)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>8</Text>
              </View>
              </TouchableOpacity>

              <TouchableOpacity 
              onPress={()=>{
                this.addData(9)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>9</Text>
              </View>
              </TouchableOpacity>
              


            </View>

            <View style = {{width:'100%',height:'25%',flexDirection:'row',justifyContent:'center'}}>
              <TouchableOpacity 
              onPress={()=>{
                
                
                // const { navigate, isFocused } = useNavigation();

               // this.props.navigation.navigate('Home')
                  let newOtp = ''
                  let otp = this.state.dataArray.find((item)=>{
                    newOtp = `${newOtp}${item}`
                    console.log(newOtp,item);
                    
                   return item == '' && item != '0'
                  })
                  if(newOtp.length == 6)
                  {

                    this.confirmCode(newOtp)
                  }
                  else
                  {
                    Toast.show('Please enter OTP')
                  }
                
              }}
              
              style = {styles.numberView}>
            <View style ={styles.submit}>
            <Image tintColor= '#fff' style = {{height:30,width:30}} source = {images.rightArrow}/>

</View>
             
              </TouchableOpacity>
              
              
              <TouchableOpacity 
              onPress={()=>{
                this.addData(0)
              }}
              style = {styles.numberView}>
              <View style ={styles.numberBack}>
              <Text style = {[styles.number,BaseStyle.boldFont]}>0</Text>
              </View>
              </TouchableOpacity>

              <TouchableOpacity 
               onPress={()=>{
                this.removeData()
              }}
              style = {styles.numberView}>
            <View style ={[styles.submit,{backgroundColor:null}]}>
                <Image style = {{height:30,width:30}} source = {images.backSpace}/>
             </View>

              </TouchableOpacity>
              


            </View>

            </View>
        )
    }

}


const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:'#fff'
    },
    title:{
        marginLeft:20,marginRight:20,lineHeight:30,
        fontSize:16,
        color:Constant.appColorAlpha()
    },

    otpNumber:{
        fontSize:16,
        color:Constant.appColorAlpha()
        },
    number:{
        fontSize:20
        },

        submit:{
            width:'100%',
            height:'100%',
            backgroundColor:Constant.appColorAlpha(),
            justifyContent:'center',alignItems:'center',
            borderRadius:5

        },
        numberBack:{
            width:'100%',
            height:'100%',
            backgroundColor:'#D3D3D3',
            justifyContent:'center',alignItems:'center',
            borderRadius:5

        },

     numberView:
     {width:"33%",height:'100%',justifyContent:'center',alignItems:'center',
     padding:6,
    }

})
export default Otp;