import React, { Component } from "react"
import {
  View,
  Animated,
  ImageBackground,
  Text,
  Modal,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import {
  FlatList,
  TextInput,
} from "react-native-gesture-handler"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import ImagePicker from "react-native-image-crop-picker"
import CalendarStrip from "react-native-calendar-strip"
import moment from "moment"
import language from "./../BaseClass/language"
import Toast from "react-native-simple-toast"
const { width, height } = Dimensions.get("window")
import urls from "./../BaseClass/ServiceUrls"
import CartDb from "./../DB/CartDb"
import Db from "./../DB/Realm"

class AddMedicine extends Component {
  constructor(props) {
    super(props)
    this.state = {
      formArray: [
        "Capsule",
        "Tablet",
        "Syringe",
        "Drop",
        "Tonic",
      ],
      selectedForm: "",
      showColor: false,
      imageOptions: false,
      image: "",
      doseOne: true,
      doseTwo: false,
      doseThree: false,
      doseFour: false,
      medicineName: "",
      isUrl: false,
      timeOne: true,
      timeTwo: false,
      timeThree: false,
      timeFour: false,
      timeSlotPageIndex: 0,
      ManufactureName: "",
      recurringValue: "",
      GenericName: "",
      isLoading: false,
      isRecurring: false,
      isSelectAll: false,
      recurringArray: [
        "Two Weeks",
        "Three Weeks",
        "One Month",
        "Two Months",
        "Three Moths",
      ],
      timeArrayDot: ["", "", ""],
      daysArray: ["", "", "", "", "", "", ""],
      timeWithImageArray: [
        {
          time: "08:00 AM",
          image: images.pillImageOne,
          isSelected: false,
        },
        {
          time: "10:00 AM",
          image: images.pillImageTwo,
          isSelected: false,
        },
        {
          time: "12:00 PM",
          image: images.pillImageThree,
          isSelected: false,
        },
        {
          time: "02:00 PM",
          image: images.pillImageFour,
          isSelected: false,
        },
        {
          time: "04:00 PM",
          image: images.pillImageFive,
          isSelected: false,
        },
        {
          time: "06:00 PM",
          image: images.pillImageSix,
          isSelected: false,
        },
        {
          time: "08:00 PM",
          image: images.pillImageOne,
          isSelected: false,
        },
        {
          time: "10:00 PM",
          image: images.pillImageSeven,
          isSelected: false,
        },
      ],
      timeWithOutImageArray: [
        { time: "01:00 AM", isSelected: false },
        { time: "02:00 AM", isSelected: false },
        { time: "03:00 AM", isSelected: false },
        { time: "04:00 AM", isSelected: false },
        { time: "05:00 AM", isSelected: false },
        { time: "06:00 AM", isSelected: false },
        { time: "07:00 AM", isSelected: false },
        { time: "08:00 AM", isSelected: false },
        { time: "09:00 AM", isSelected: false },
        { time: "10:00 AM", isSelected: false },
        { time: "11:00 AM", isSelected: false },
        { time: "12:00 PM", isSelected: false },
        { time: "01:00 PM", isSelected: false },
        { time: "02:00 PM", isSelected: false },
        { time: "03:00 PM", isSelected: false },
        { time: "04:00 PM", isSelected: false },
        { time: "05:00 PM", isSelected: false },
        { time: "06:00 PM", isSelected: false },
        { time: "07:00 PM", isSelected: false },
        { time: "08:00 PM", isSelected: false },
        { time: "09:00 PM", isSelected: false },
        { time: "10:00 PM", isSelected: false },
        { time: "11:00 PM", isSelected: false },
        { time: "12:00 AM", isSelected: false },
      ],
      isOnlyTime: false,

      imageAdd: false,
      shapeIndex: -1,
      selectedColor: -1,
      shapesArray: [
        { image: images.roundRect, name: "Rect" },
        { image: images.circle, name: "Circle" },
        { image: images.oval, name: "Oval" },
        { image: images.rectangle, name: "Squr" },
        { image: images.pentagon, name: "Pent" },
      ],
      colorsArray: [
        "#DB4F64",
        "#BA9664",
        "#98C3E8",
        "#E7C78B",
        "#D2CCCF",
        "#2C4D30",
        "#C871A4",
        "#668AF7",
        "#906CFB",
      ],
    }
  }

  componentDidMount() {
    SplashScreen.hide()

    let datesArray = []

    this.state.daysArray.map((item, i) => {
      let name = moment()
        .add(i, "day")
        .format("ddd")
        .toUpperCase()
      let date = moment()
        .add(i, "day")
        .format("DD")
        .toUpperCase()

      let fullDate = moment()
        .add(i, "day")
        .format("YYYY-MM-DD")
        .toUpperCase()
      let isSelected = false

      let dic = {
        name: name,
        date: date,
        fullDate: fullDate,
        isSelected: isSelected,
      }
      datesArray.push(dic)
    })

    this.setState({ daysArray: datesArray }, () => {
      const { isEdit, selectedItem } = this.props
        .route.params || { isEdit: false }
      if (!!isEdit && isEdit) {
        this.updateEditData(selectedItem)
      }
    })
  }

  updateEditData(selectedItem) {
    const {
      PillName,
      Image,
      Generic,
      Brand,
      ShapeType,
      ColorCode,
      Dosage,
      DoseTimes,
      IsModulerView,
      IsRecurring,
      IsSelectAll,
      PillReminderSlots,
      PillType,
    } = selectedItem

    const selectedShapes =
      this.state.shapesArray.findIndex(
        (item) => item.name == ShapeType
      )
    const selectedForm =
      this.state.formArray.findIndex(
        (item) => item == PillType
      )
    const selectedColor =
      this.state.colorsArray.findIndex(
        (item) => item == ColorCode
      )

    const seletedTimes = PillReminderSlots.map(
      (item) => item.Time
    ).filter(
      (val, id, array) => array.indexOf(val) == id
    )
    if (IsModulerView) {
      this.state.timeWithOutImageArray.map((item) =>
        seletedTimes.includes(item.time)
          ? { ...(item.isSelected = true) }
          : { ...(item.isSelected = false) }
      )
    } else {
      this.state.timeWithImageArray.map((item) =>
        seletedTimes.includes(item.time)
          ? { ...(item.isSelected = true) }
          : { ...(item.isSelected = false) }
      )
    }

    console.log(seletedTimes)
    var doseOne = false
    var doseTwo = false
    var doseThree = false
    var doseFour = false

    if (Dosage == "1/4") {
      doseOne = true
    } else if (Dosage == "2/4") {
      doseOne = true
      doseTwo = true
    } else if (Dosage == "3/4") {
      doseOne = true
      doseTwo = true
      doseThree = true
    } else if (Dosage == "4/4" || Dosage == "1") {
      doseOne = true
      doseTwo = true
      doseThree = true
      doseFour = true
    }

    var timeOne = false
    var timeTwo = false
    var timeThree = false
    var timeFour = false

    if (DoseTimes == "1/4") {
      timeOne = true
    } else if (DoseTimes == "2/4") {
      timeOne = true
      timeTwo = true
    } else if (DoseTimes == "3/4") {
      timeOne = true
      timeTwo = true
      timeThree = true
    } else if (
      DoseTimes == "4/4" ||
      DoseTimes == "1"
    ) {
      timeOne = true
      timeTwo = true
      timeThree = true
      timeFour = true
    }

    var getDateArray = PillReminderSlots.map(
      (item) => {
        let date = moment(item.Day).format(
          "YYYY-MM-DD"
        )
        return date
      }
    )

    this.state.daysArray.map((day) => {
      const fullDate = day.fullDate
      day.isSelected =
        getDateArray.includes(fullDate)
    })

    this.setState({
      medicineName: PillName,
      image: Image,
      isUrl: true,
      imageAdd: true,
      ManufactureName: Brand,
      GenericName: Generic,
      shapeIndex: selectedShapes,
      selectedColor: selectedColor,
      doseOne,
      doseTwo,
      doseThree,
      doseFour,
      timeOne,
      timeTwo,
      timeThree,
      timeFour,
      isOnlyTime: IsModulerView,
      isRecurring: IsRecurring,
      isSelectAll: IsSelectAll,
      selectedForm:
        this.state.formArray[selectedForm],
    })
  }

  render() {
    const {
      formArray,
      selectedForm,
      isLoading,
      recurringValue,
      medicineName,
      timeArrayDot,
      isRecurring,
      isSelectAll,
      daysArray,
      selectedTimeIndex,
      timeWithOutImageArray,
      isOnlyTime,
      timeSlotPageIndex,
      timeWithImageArray,
      doseOne,
      doseTwo,
      doseThree,
      doseFour,
      timeOne,
      timeTwo,
      timeThree,
      timeFour,
      showColor,
      shapesArray,
      colorsArray,
      imageAdd,
      image,
      shapeIndex,
      selectedColor,
    } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {isLoading ? Constant.showLoader() : null}

        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: 4,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor:
                Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              {language.addmedicine.toUpperCase()}
            </Text>

            <TouchableOpacity
              onPress={() => {
                this.addPillService()
              }}
              style={{
                height: 30,
                width: 55,
                marginRight: 10,
                backgroundColor:
                  Constant.selectedTextColor(),
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Text
                style={[
                  {
                    color: Constant.headerColor(),
                    fontSize: 12,
                  },
                  BaseStyle.boldFont,
                ]}>
                {language.save.toUpperCase()}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.searchMainView}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate(
                  "Search",
                  {
                    isFromPill: true,
                    selectedItem: this.selectedItem,
                  }
                )
              }}
              style={[styles.searchInner]}>
              <Image
                source={images.search}
                style={{
                  marginLeft: 10,
                  width: 15,
                  height: 15,
                }}
              />
              <Text
                style={[
                  BaseStyle.boldFont,
                  styles.searchIcon,
                ]}>
                {medicineName != ""
                  ? medicineName
                  : language.searchmedicine}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView>
          <View
            style={{
              margin: 15,
              elevation: 4,
              backgroundColor: "#fff",
              padding: 10,
              borderRadius: 8,
              margin: 10,
            }}>
            <Text
              style={[
                {
                  fontSize: 14,
                  color: Constant.textColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {language.medicineform.toUpperCase()}
            </Text>

            <View
              style={{
                height: 100,
                marginTop: 10,
                marginLeft: -10,
                marginRight: -10,
              }}>
              <ScrollView
                style={{ height: 100 }}
                horizontal={true}>
                {formArray.map(
                  (category, index) => (
                    <TouchableOpacity
                      key={category.id}
                      onPress={() => {
                        this.setState({
                          selectedForm: category,
                        })
                      }}
                      activeOpacity={1}
                      style={{
                        height: 90,
                        width: 72,
                        alignItems: "center",
                        elevation: 4,
                        justifyContent: "center",
                        marginLeft: 10,
                        marginTop: 5,
                        marginBottom: 5,
                        marginRight:
                          index ==
                          formArray.length - 1
                            ? 10
                            : 0,
                        backgroundColor:
                          category == selectedForm
                            ? Constant.appColorAlpha()
                            : "#fff",
                        borderRadius: 13,
                      }}>
                      <Image
                        tintColor={
                          category == selectedForm
                            ? "#fff"
                            : Constant.headerColor()
                        }
                        style={{
                          height: 18,
                          width: 18,
                        }}
                        source={images.calendar}
                      />
                      <Text
                        style={[
                          {
                            fontSize: 12,
                            marginTop: 8,
                            color:
                              category ==
                              selectedForm
                                ? "#fff"
                                : Constant.appFullColor(),
                          },
                          BaseStyle.regularFont,
                        ]}>
                        {category}
                      </Text>
                    </TouchableOpacity>
                  )
                )}
              </ScrollView>
            </View>
            {(this.state.selectedForm ==
              "Capsule" ||
              this.state.selectedForm ==
                "Tablet") && (
              <>
                <View>
                  <Text
                    style={[
                      {
                        fontSize: 14,
                        color: Constant.textColor(),
                        marginTop: 8,
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {language.shape.toUpperCase()}
                  </Text>

                  <ScrollView
                    style={{ height: 50 }}
                    horizontal={true}>
                    {shapesArray.map(
                      (shape, index) => (
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              shapeIndex:
                                index == shapeIndex
                                  ? -1
                                  : index,
                            })
                          }}
                          activeOpacity={1}
                          style={{
                            height: 50,
                            width: 50,
                            alignItems: "center",
                            justifyContent:
                              "center",
                            marginLeft: 10,
                            marginTop: 5,
                            marginBottom: 5,
                            marginRight:
                              index ==
                              shapesArray.length - 1
                                ? 10
                                : 0,
                            backgroundColor: "#fff",
                            borderRadius: 0,
                          }}>
                          <ImageBackground
                            resizeMode={"center"}
                            tintColor={
                              index == shapeIndex
                                ? Constant.appColorAlpha()
                                : "#d3d3d3"
                            }
                            style={{
                              height: 30,
                              width: 50,
                              alignItems: "center",
                              justifyContent:
                                "center",
                            }}
                            source={shape.image}>
                            {index == shapeIndex ? (
                              <Image
                                tintColor={"#fff"}
                                style={{
                                  height: 12,
                                  width: 12,
                                }}
                                source={
                                  images.correct
                                }
                              />
                            ) : null}
                          </ImageBackground>
                        </TouchableOpacity>
                      )
                    )}
                  </ScrollView>
                </View>

                <View>
                  <Text
                    style={[
                      {
                        fontSize: 14,
                        color: Constant.textColor(),
                        marginTop: 8,
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {language.color.toUpperCase()}
                  </Text>

                  <ScrollView
                    style={{ height: 50 }}
                    horizontal={true}>
                    {colorsArray.map(
                      (color, index) => (
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              selectedColor:
                                index ==
                                selectedColor
                                  ? -1
                                  : index,
                            })
                          }}
                          activeOpacity={1}
                          style={{
                            height: 30,
                            width: 30,
                            alignItems: "center",
                            justifyContent:
                              "center",
                            marginLeft: 10,
                            borderRadius: 15,
                            marginTop: 5,
                            marginBottom: 5,
                            marginRight:
                              index ==
                              colorsArray.length - 1
                                ? 10
                                : 0,
                            backgroundColor: color,
                          }}>
                          {index ==
                          selectedColor ? (
                            <Image
                              tintColor={"#fff"}
                              style={{
                                height: 12,
                                width: 12,
                              }}
                              source={
                                images.correct
                              }
                            />
                          ) : null}
                        </TouchableOpacity>
                      )
                    )}
                  </ScrollView>
                </View>
              </>
            )}
          </View>

          {/* SecondView */}
          <View
            style={{
              height: 120,
              width: "100%",
              padding: 10,
            }}>
            <View
              style={{
                height: 120,
                width: width - 20,
                elevation: 3,
                paddingLeft: 10,
                paddingRight: 10,
                backgroundColor: "#fff",
                borderRadius: 8,
                flexDirection: "row",
                justifyContent: "space-between",
              }}>
              <View
                style={{
                  flex: 0.3,
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 14,
                      color: Constant.textColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.dose.toUpperCase()}
                </Text>
                <View style={styles.doseView}>
                  <View
                    style={{
                      flexDirection: "row",
                      flex: 0.5,
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          doseOne: !doseOne,
                        })
                      }}
                      style={{
                        flex: 0.5,
                        backgroundColor: doseOne
                          ? Constant.appColorAlpha()
                          : "rgb(244,245,250)",
                      }}>
                      <Text></Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          doseTwo: !doseTwo,
                        })
                      }}
                      style={{
                        flex: 0.5,
                        backgroundColor: doseTwo
                          ? Constant.appColorAlpha()
                          : "rgb(244,245,250)",
                      }}>
                      <Text></Text>
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      flex: 0.5,
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          doseThree: !doseThree,
                        })
                      }}
                      style={{
                        flex: 0.5,
                        backgroundColor: doseThree
                          ? Constant.appColorAlpha()
                          : "rgb(244,245,250)",
                      }}>
                      <Text></Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          doseFour: !doseFour,
                        })
                      }}
                      style={{
                        flex: 0.5,
                        backgroundColor: doseFour
                          ? Constant.appColorAlpha()
                          : "rgb(244,245,250)",
                      }}>
                      <Text></Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              {/* TImes */}
              <View
                style={{
                  flex: 0.3,
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 14,
                      color: Constant.textColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.times.toUpperCase()}
                </Text>

                <View style={styles.timeView}>
                  <View
                    style={{
                      flexDirection: "row",
                      flex: 0.5,
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          timeOne: !timeOne,
                        })
                      }}
                      style={{
                        flex: 0.5,
                        backgroundColor: timeOne
                          ? Constant.appColorAlpha()
                          : "rgb(244,245,250)",
                      }}>
                      <Text></Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          timeTwo: !timeTwo,
                        })
                      }}
                      style={{
                        flex: 0.5,
                        backgroundColor: timeTwo
                          ? Constant.appColorAlpha()
                          : "rgb(244,245,250)",
                      }}>
                      <Text></Text>
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      flex: 0.5,
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          timeThree: !timeThree,
                        })
                      }}
                      style={{
                        flex: 0.5,
                        backgroundColor: timeThree
                          ? Constant.appColorAlpha()
                          : "rgb(244,245,250)",
                      }}>
                      <Text></Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          timeFour: !timeFour,
                        })
                      }}
                      style={{
                        flex: 0.5,
                        backgroundColor: timeFour
                          ? Constant.appColorAlpha()
                          : "rgb(244,245,250)",
                      }}>
                      <Text></Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              <View
                style={{
                  flex: 0.3,
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 14,
                      color: Constant.textColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.image.toUpperCase()}
                </Text>

                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      imageOptions: true,
                    })
                  }}
                  style={{
                    height: 70,
                    marginTop: 6,
                    alignItems: "center",
                    justifyContent: "center",
                    width: 70,
                    borderWidth: 1,
                    borderRadius: 5,
                    borderStyle: "dashed",
                    borderColor:
                      Constant.appColorAlpha(),
                  }}>
                  <Image
                    style={{
                      width: 50,
                      height: 50,
                    }}
                    source={
                      imageAdd
                        ? { uri: image }
                        : images.plus
                    }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>

          {/* three */}

          {/* SecondView */}
          <View
            style={{
              width: "100%",
              padding: 10,
              marginTop: 15,
            }}>
            <View
              style={{
                height: 180,
                width: width - 20,
                elevation: 3,
                paddingLeft: 10,
                paddingRight: 10,
                backgroundColor: "#fff",
                borderRadius: 8,
                justifyContent: "space-between",
              }}>
              <View
                style={{
                  height: 35,
                  width: "100%",
                  padding: 10,
                  paddingBottom: 0,
                  justifyContent: "space-between",
                  flexDirection: "row",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 14,
                      width: "72%",
                      color: Constant.textColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.timeslot.toUpperCase()}
                </Text>
                <Text
                  onPress={() => {
                    this.refs._scrollView.scrollTo(
                      0
                    )
                    this.resetTimes()
                    this.setState({
                      isOnlyTime: !isOnlyTime,
                      timeSlotPageIndex: 0,
                    })
                  }}
                  style={[
                    {
                      fontSize: 14,
                      color: Constant.textAlpha(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.switchView.toUpperCase()}
                </Text>
              </View>

              <ScrollView
                style={{ height: 120 }}
                ref="_scrollView"
                onMomentumScrollEnd={
                  this._onMomentumScrollEnd
                }
                horizontal={true}>
                {(isOnlyTime
                  ? timeWithOutImageArray
                  : timeWithImageArray
                ).map((time, index) => (
                  <TouchableOpacity
                    key={time.time}
                    onPress={() => {
                      if (
                        this.isTimeSlotSelectionValid(
                          time.isSelected
                        )
                      ) {
                        time.isSelected =
                          !time.isSelected
                        this.forceUpdate()
                      }
                    }}
                    activeOpacity={1}
                    style={{
                      height: 100,
                      width: 80,
                      alignItems: "center",
                      elevation: 4,
                      justifyContent: "center",
                      marginLeft: 10,
                      marginTop: 5,
                      marginBottom: 5,
                      marginRight:
                        index ==
                        timeWithImageArray.length -
                          1
                          ? 10
                          : 0,
                      backgroundColor:
                        time.isSelected
                          ? Constant.appColorAlpha()
                          : "#fff",
                      borderRadius: 13,
                    }}>
                    {isOnlyTime ? null : (
                      <Image
                        tintColor={
                          time.isSelected
                            ? "#fff"
                            : Constant.headerColor()
                        }
                        style={{
                          height: 30,
                          width: 30,
                          marginTop: 5,
                        }}
                        source={time.image}
                      />
                    )}
                    <Text
                      style={[
                        {
                          fontSize: 12,
                          marginTop: 8,
                          color: time.isSelected
                            ? "#fff"
                            : Constant.textColor(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {time.time}
                    </Text>
                  </TouchableOpacity>
                ))}
              </ScrollView>

              <View
                style={{
                  height: 35,
                  width: "100%",
                  padding: 10,
                  justifyContent: "center",
                  flexDirection: "row",
                }}>
                {timeArrayDot.map((_, i) => {
                  // the _ just means we won't use that parameter

                  return (
                    <Animated.View
                      key={i}
                      style={{
                        height: 10,
                        width: 10,
                        marginLeft: 6,
                        borderRadius: 5,
                        backgroundColor: "#fff",
                        elevation: 4,
                        borderColor:
                          Constant.appColorAlpha(),
                        borderWidth:
                          i == timeSlotPageIndex
                            ? 1
                            : 0,
                        justifyContent: "center",
                        alignItems: "center",
                      }}>
                      <View
                        style={{
                          height: 4,
                          width: 4,
                          borderRadius: 2,
                          backgroundColor:
                            i != timeSlotPageIndex
                              ? "#fff"
                              : Constant.appColorAlpha(),
                        }}
                      />
                    </Animated.View>
                  )
                })}
              </View>
            </View>
          </View>

          {/* last */}

          <View
            style={{
              width: "100%",
              padding: 10,
              marginTop: 15,
            }}>
            <View
              style={{
                height: 200,
                width: width - 20,
                elevation: 3,
                paddingTop: 0,
                paddingLeft: 10,
                paddingRight: 10,
                backgroundColor: "#fff",
                borderRadius: 8,
                justifyContent: "space-between",
              }}>
              <View
                style={{
                  height: 65,
                  width: "100%",
                  alignItems: "center",
                  overflow: "hidden",
                  justifyContent: "center",
                }}>
                <View
                  style={{
                    height: 180,
                    width: 180,
                    borderRadius: 90,
                    marginTop: -120,
                    justifyContent: "flex-end",
                    backgroundColor:
                      Constant.appColorAlpha(),
                  }}>
                  <View
                    style={{
                      position: "absolute",
                      alignItems: "center",
                      justifyContent: "center",
                      bottom: 15,
                      left: 0,
                      right: 0,
                      height: 40,
                    }}>
                    <Text
                      style={[
                        {
                          fontSize: 16,
                          color: "#fff",
                        },
                        BaseStyle.boldFont,
                      ]}>
                      {moment().format("MMM YYYY")}
                    </Text>
                    <Text
                      style={[
                        {
                          fontSize: 10,
                          marginTop: 1,
                          color: "#fff",
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {language.selectdays}
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  height: 60,
                  width: width - 30,
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}>
                {daysArray.map((item, i) => {
                  // the _ just means we won't use that parameter

                  return (
                    <TouchableOpacity
                      onPress={() => {
                        item.isSelected =
                          !item.isSelected
                        this.forceUpdate()
                      }}
                      style={{
                        flex: 0.12,
                        backgroundColor:
                          item.isSelected
                            ? Constant.appColorAlpha()
                            : "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius:
                          item.isSelected ? 8 : 0,
                      }}>
                      <Text
                        style={[
                          {
                            fontSize: 15,
                            color: item.isSelected
                              ? "#fff"
                              : Constant.textAlpha(),
                          },
                          BaseStyle.regularFont,
                        ]}>
                        {item.name}
                      </Text>
                      <Text
                        style={[
                          {
                            fontSize: 13,
                            marginTop: 5,
                            color: item.isSelected
                              ? "#fff"
                              : Constant.textColor(),
                          },
                          BaseStyle.regularFont,
                        ]}>
                        {item.date}
                      </Text>
                    </TouchableOpacity>
                  )
                })}
              </View>

              <View
                style={{
                  height: 50,
                  width: "100%",
                  flexDirection: "row",
                  justifyContent: "space-around",
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      isRecurring: !isRecurring,
                    })
                  }}
                  style={{
                    flexDirection: "row",
                    height: 30,
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <View
                    style={{
                      height: 20,
                      width: 20,
                      borderWidth: 1,
                      borderColor:
                        Constant.appColorAlpha(),
                      backgroundColor:
                        recurringValue != ""
                          ? Constant.appColorAlpha()
                          : "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                    }}>
                    <Image
                      tintColor={"#fff"}
                      resizeMode={"center"}
                      source={images.correct}
                      style={{
                        height: 16,
                        width: 16,
                      }}
                    />
                  </View>
                  <Text
                    style={[
                      {
                        fontSize: 15,
                        marginLeft: 5,
                        color: "#000",
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {language.recurring.toUpperCase()}
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => {
                    this.selectAllAction(
                      !isSelectAll
                    )
                    this.setState({
                      isSelectAll: !isSelectAll,
                    })
                  }}
                  style={{
                    flexDirection: "row",
                    height: 30,
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <View
                    style={{
                      height: 20,
                      width: 20,
                      borderWidth: 1,
                      borderColor:
                        Constant.appColorAlpha(),
                      backgroundColor: isSelectAll
                        ? Constant.appColorAlpha()
                        : "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                    }}>
                    <Image
                      tintColor={"#fff"}
                      resizeMode={"center"}
                      source={images.correct}
                      style={{
                        height: 16,
                        width: 16,
                      }}
                    />
                  </View>

                  <Text
                    style={[
                      {
                        fontSize: 15,
                        marginLeft: 5,
                        color: "#000",
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {language.selectall.toUpperCase()}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View
            style={{ height: 20, width: "100%" }}
          />
        </ScrollView>

        {this.showRecurring()}

        {this.showImageOptions()}
      </SafeAreaView>
    )
  }

  selectAllAction(isSelected) {
    this.state.daysArray.map((item) => {
      item.isSelected = isSelected
    })
  }

  resetTimes() {
    this.state.timeWithImageArray.map((item) => {
      item.isSelected = false
    })

    this.state.timeWithOutImageArray.map((item) => {
      item.isSelected = false
    })
  }

  addPillService() {
    const {
      medicineName,
      selectedForm,
      shapeIndex,
      selectedColor,
      isUrl,
      image,
    } = this.state
    const isTimeSlotrequired =
      this.state.selectedForm == "Capsule" ||
      this.state.selectedForm == "Tablet"
    const selectedTimes = this.state.isOnlyTime
      ? this.state.timeWithOutImageArray.filter(
          (item) => {
            return item.isSelected == true
          }
        )
      : this.state.timeWithImageArray.filter(
          (item) => {
            return item.isSelected == true
          }
        )
    const selectedTimeCount = this.getTime()

    if (medicineName == "") {
      Toast.show("Select Medicine")
      return
    } else if (selectedForm == "") {
      Toast.show("Select Medicine Form")
      return
    } else if (
      shapeIndex == -1 &&
      isTimeSlotrequired
    ) {
      Toast.show("Select Medicine Shape")
      return
    } else if (
      selectedColor == -1 &&
      isTimeSlotrequired
    ) {
      Toast.show("Select Medicine Color")
      return
    } else if (
      selectedTimes.length != selectedTimeCount
    ) {
      Toast.show(
        "Please select timeslots same as number of times"
      )
      return
    } else {
      if (image != "" && !isUrl) {
        this.uploadImage(image)
      } else {
        this.serviceCall()
      }
    }
  }

  uploadImage(url) {
    this.setState({ isLoading: true })

    Constant.uploadImageToServer(
      urls.mediaUpload,
      url,
      (result) => {
        if (result.success) {
          let PictureUrl = result.url.Response.URL

          this.setState(
            {
              image: PictureUrl,
            },
            () => {
              this.serviceCall()
            }
          )
        }
        this.setState({ isLoading: false })
        return
      }
    )
  }

  isTimeSlotSelectionValid(isSelected) {
    const selectedTimes = this.state.isOnlyTime
      ? this.state.timeWithOutImageArray.filter(
          (item) => {
            return item.isSelected == true
          }
        )
      : this.state.timeWithImageArray.filter(
          (item) => {
            return item.isSelected == true
          }
        )
    const selectedTimeCount = this.getTime()

    if (selectedTimes.length < selectedTimeCount) {
      return true
    }
    return isSelected ? true : false
  }

  serviceCall() {
    this.setState({ isLoading: true })
    const {
      GenericName,
      ManufactureName,
      isOnlyTime,
      formArray,
      selectedForm,
      isSelectAll,
      medicineName,
      recurringValue,
      medicineform,
      shapeIndex,
      shapesArray,
      colorsArray,
      selectedColor,
      image,
    } = this.state

    const isTimeSlotrequired =
      this.state.selectedForm == "Capsule" ||
      this.state.selectedForm == "Tablet"

    const { isEdit, selectedItem } = this.props
      .route.params || { isEdit: false }
    let params = {
      id: isEdit
        ? !!selectedItem.Id
          ? selectedItem.Id
          : 0
        : 0,
      customerId: global.CustomerId,
      Brand: ManufactureName,
      Generic: GenericName,
      pillName: medicineName,
      pillType: selectedForm,
      shapeType: isTimeSlotrequired
        ? shapesArray[shapeIndex].name
        : "",
      colorCode: isTimeSlotrequired
        ? colorsArray[selectedColor]
        : "",
      dosage: `${this.getDose()}/4`,
      doseTimes: `${this.getTime()}/4`,
      image: image,
      alarm: "",
      IsRecurring:
        recurringValue != "" ? true : false,
      RecurringValue: recurringValue,
      IsSelectAll: isSelectAll,
      IsModulerView: isOnlyTime,
      pillReminderSlots: this.pillSlots(),
    }

    console.log("params", params)

    Constant.postMethod(
      isEdit
        ? urls.updatePillReminder
        : urls.createPillReminder,
      params,
      (result) => {
        this.setState({ isLoading: false })
        console.log("new Result", result)

        if (result.success) {
          if (result.result.Response != null) {
            if (
              result.result.Response.pillReminder
                .length > 0
            ) {
              if (isEdit) {
                const deletedData =
                  Db.objectForPrimaryKey(
                    "PillReminder",
                    selectedItem.Id
                  )
                deletedData.PillReminderSlots.map(
                  (item) => {
                    try {
                      Db.write(() => {
                        Db.delete(
                          Db.objectForPrimaryKey(
                            "Pill",
                            item.Id
                          )
                        )
                      })
                    } catch (e) {
                      console.log(
                        "Error on pill data delete"
                      )
                      console.log(JSON.stringify(e))
                    }
                    return true
                  }
                )

                try {
                  Db.write(() => {
                    Db.delete(
                      Db.objectForPrimaryKey(
                        "PillReminder",
                        selectedItem.Id
                      )
                    )
                  })
                } catch (e) {
                  console.log(
                    "Error on pill delete"
                  )
                  console.log(JSON.stringify(e))
                }
              }
              CartDb.addPillReminder(
                result.result.Response
                  .pillReminder[0]
              )
              this.props.navigation.goBack(null)
            } else {
              Toast.show("Somthing went wrong")
            }
          }
        }
      }
    )
  }

  getDose() {
    const {
      doseOne,
      doseTwo,
      doseThree,
      doseFour,
    } = this.state

    let count = 0

    if (doseOne) {
      count = count + 1
    }
    if (doseTwo) {
      count = count + 1
    }
    if (doseTwo) {
      count = count + 1
    }
    if (doseTwo) {
      count = count + 1
    }

    return count
  }

  getTime() {
    const {
      timeOne,
      timeTwo,
      timeThree,
      timeFour,
    } = this.state

    let count = 0

    if (timeOne) {
      count = count + 1
    }
    if (timeTwo) {
      count = count + 1
    }
    if (timeThree) {
      count = count + 1
    }
    if (timeFour) {
      count = count + 1
    }

    return count
  }

  getWeekCount() {
    const { recurringValue } = this.state
    if (recurringValue == "Two Weeks") {
      return 2
    } else if (recurringValue == "Three Weeks") {
      return 3
    } else if (recurringValue == "One Month") {
      return 4
    } else if (recurringValue == "Two Month") {
      return 8
    } else if (recurringValue == "Three Month") {
      return 12
    }
    return 1
    //fileprivate let titles = ["Two Weeks","Three Weeks", "One Month", "Two Month", "Three Month"]
  }

  pillSlots() {
    const {
      isOnlyTime,
      recurringValue,
      timeWithOutImageArray,
      timeWithImageArray,
      daysArray,
    } = this.state

    let selectedTimes = []
    let pillReminderSlots = []
    if (isOnlyTime) {
      selectedTimes = timeWithOutImageArray.filter(
        (item) => {
          return item.isSelected == true
        }
      )
    } else {
      selectedTimes = timeWithImageArray.filter(
        (item) => {
          return item.isSelected == true
        }
      )
    }

    let selectedDaysArray = daysArray.filter(
      (item) => {
        return item.isSelected == true
      }
    )

    for (let date of selectedDaysArray) {
      for (let time of selectedTimes) {
        let dic = {
          day: moment(date).format("YYYY-MM-DD"),
          time: time.time,
          isSkipped: false,
          isTaken: false,
        }
        pillReminderSlots.push(dic)
        if (recurringValue != "") {
          let numberOfweeks = this.getWeekCount()

          // let dataArray = new Array(numberOfweeks)

          for (let i = 1; i <= numberOfweeks; i++) {
            let reurringDate = moment(
              moment(date).format("YYYY-MM-DD")
            )
              .add(1 * i, "week")
              .format("YYYY-MM-DD")

            let dicNew = {
              day: moment(reurringDate).format(
                "YYYY-MM-DD"
              ),
              time: time.time,
              isSkipped: false,
              isTaken: false,
            }
            pillReminderSlots.push(dicNew)
          }
        }
      }
    }

    return pillReminderSlots
  }

  _onMomentumScrollEnd = ({ nativeEvent }: any) => {
    // the current offset, {x: number, y: number}
    const { isOnlyTime } = this.state
    const position = nativeEvent.contentOffset
    // page index
    const index = Math.round(
      nativeEvent.contentOffset.x /
        (isOnlyTime ? 800 : 200)
    )

    console.log(position.x)

    if (index !== this.state.timeSlotPageIndex) {
      // onPageDidChanged
      this.setState({ timeSlotPageIndex: index })
    }
  }

  selectedItem = (item) => {
    let url =
      item.Images.length > 0
        ? item.Images[0].Src
        : ""

    this.setState({
      medicineName: item.Name,
      image: url,
      isUrl: true,
      imageAdd: true,
      ManufactureName: item.ManufactureName,
      GenericName: item.GenericName,
    })
  }

  showRecurring() {
    const {
      recurringArray,
      isRecurring,
      image,
      shapeIndex,
      selectedColor,
    } = this.state
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={isRecurring}
        onRequestClose={() => {
          this.setState({ showColor: false })
        }}>
        <View
          style={{
            flex: 1,
            width: "100%",
            height: "100%",
          }}>
          <TouchableOpacity
            onPress={() => {
              this.setState({
                isRecurring: false,
                recurringValue: "",
              })
            }}
            activeOpacity={1}
            style={{
              flex: 1,
              backgroundColor: "rgba(0,0,0,0.4)",
            }}></TouchableOpacity>
          <View
            style={{
              width: "100%",
              padding: 15,
              height: 220,
              backgroundColor: "#fff",
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
            }}>
            <Text
              style={[
                { fontSize: 14, marginTop: 8 },
                BaseStyle.boldFont,
              ]}>
              Recurring
            </Text>

            {recurringArray.map((item, index) => {
              return (
                <Text
                  onPress={() => {
                    this.setState({
                      recurringValue: item,
                      isRecurring: false,
                    })
                  }}
                  style={[
                    {
                      height: 30,
                      marginTop:
                        index == 0 ? 12 : 0,
                      fontSize: 14,
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {item}
                </Text>
              )
            })}
          </View>
        </View>
      </Modal>
    )
  }

  showImageOptions() {
    const { imageOptions } = this.state

    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={imageOptions}
        onRequestClose={() => {}}>
        <TouchableOpacity
          activeOpacity={2}
          onPress={() => {
            this.setState({ imageOptions: false })
          }}
          style={{
            flex: 1,
            backgroundColor: "rgba(0,0,0,0.5)",
            position: "absolute",
            bottom: 0,
            top: 0,
            left: 0,
            right: 0,
          }}
        />
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}>
          <View style={styles.popupCard}>
            <View
              style={{
                backgroundColor:
                  Constant.appColorAlpha(),
                alignItems: "center",
                height: 50,
                justifyContent: "center",
              }}>
              <Text
                style={[
                  { color: "#ffffff" },
                  BaseStyle.regularFont,
                ]}>
                Upload image
              </Text>
            </View>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                alignItems: "center",
              }}
              onPress={this._pickCamImage}>
              <View style={styles.containerView}>
                <Image
                  style={{ height: 30, width: 30 }}
                  source={images.camera}
                />
              </View>
              <View
                style={{ marginLeft: 15, top: 11 }}>
                <Text
                  style={[BaseStyle.regularFont]}>
                  Camera
                </Text>
              </View>
            </TouchableOpacity>
            <View
              style={{
                borderBottomWidth: 0.5,
                borderColor: "#c7c7c7",
                top: 1,
              }}>
              <Text></Text>
            </View>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                alignItems: "center",
              }}
              onPress={this._pickImage}>
              <View style={styles.containerView}>
                <Image
                  source={images.gallery}
                  style={{ height: 30, width: 30 }}
                />
              </View>
              <View
                style={{ marginLeft: 15, top: 10 }}>
                <Text
                  style={[BaseStyle.regularFont]}>
                  Choose from Gallery
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  _pickCamImage = async () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then((image) => {
      console.log(image)
      this.setState({ imageOptions: false })

      this.setState({
        image: image.path,
        imageAdd: true,
        isUrl: false,
      })
    })
  }

  _pickImage = async () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then((image) => {
      console.log(image)
      this.setState({ imageOptions: false })

      this.setState({
        image: image.path,
        imageAdd: true,
        isUrl: false,
      })
    })
  }
}

AddMedicine.defaultProps = {
  isEdit: false,
}

const styles = StyleSheet.create({
  popupCard: {
    height: 180,
    width: width - 90,
    backgroundColor: "#fff",
  },
  containerView: {
    top: 10,
    height: 40,
    width: 40,
    left: 5,
    right: 5,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#c7c7c7",
    alignItems: "center",
    justifyContent: "center",
  },
  popupimageStyles: {
    height: 20,
    width: 20,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    marginTop: 10,
    bottom: 5,
  },
  searchMainView: {
    width: "100%",
    paddingTop: 10,
    paddingRight: 5,
    paddingLeft: 5,
    height: 45,
    marginBottom: 20,
    alignItems: "center",
    justifyContent: "center",
  },

  searchInner: {
    marginRight: 5,
    marginLeft: 5,
    elevation: 0,
    height: 40,
    backgroundColor: "#fff",
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  searchIcon: {
    marginLeft: 0,
    color: Constant.textColor(),
    textAlign: "left",
    left: 10,
    fontSize: 12,
    flex: 1,
  },
  doseView: {
    height: 70,
    width: 70,
    borderRadius: 35,
    overflow: "hidden",
    elevation: 1,
    marginTop: 6,
  },
  timeView: {
    height: 70,
    width: 70,
    overflow: "hidden",
    elevation: 1,
    marginTop: 6,
  },
})

export default AddMedicine
