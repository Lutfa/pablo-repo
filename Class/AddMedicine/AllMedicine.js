import React, { Component } from "react"
import {
  View,
  Text,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import { FlatList } from "react-native-gesture-handler"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import { AnimatedCircularProgress } from "react-native-circular-progress"
import CalendarStrip from "react-native-calendar-strip"
import moment from "moment"
import IndicatorViewPager from "./../ExtraClass/viewpager/IndicatorViewPager"
import PagerTitleIndicator from "./../ExtraClass/viewpager/indicator/PagerTitleIndicator"
import ViewPager from "@react-native-community/viewpager"
import SwipeListView from "./../ExtraClass/SwipeList/SwipeListView"
import language from "./../BaseClass/language"
import urls from "./../BaseClass/ServiceUrls"
import CartDb from "./../DB/CartDb"
import Db from "./../DB/Realm"
import ImageLoad from "./../BaseClass/ImageLoader"
const { width, height } = Dimensions.get("window")
const MAX_POINTS = 90
import PushNotification, {
  Importance,
} from "react-native-push-notification"
import DateTimePicker from "@react-native-community/datetimepicker"

let datesBlacklist = [moment().add(1, "days")] // 1 day disabled

class AllMedicine extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pillArray: [],
      summaryArray: [],
      pillShiftArray: [],
      morningArray: [],
      afternoonArray: [],
      eveningArray: [],
      points: 80,
      selectedPill: "",
      showDate: false,
      showReschedule: false,
      dayViewIndex: 0,
      pageIndex: 0,
      isLoading: false,
      showTimePicker: false,
      formattedDate: moment().format("MMM DD"),
      selectedDate: moment().format("YYYY-MM-DD"),
      morningDemoArray: [
        "12:00 AM",
        "01:00 AM",
        "02:00 AM",
        "03:00 AM",
        "04:00 AM",
        "05:00 AM",
        "06:00 AM",
        "07:00 AM",
        "08:00 AM",
        "09:00 AM",
        "10:00 AM",
        "11:00 AM",
      ],
      afternoonDemoArray: [
        "12:00 PM",
        "01:00 PM",
        "02:00 PM",
        "03:00 PM",
        "04:00 PM",
        "05:00 PM",
      ],
      eveningDemoArray: [
        "06:00 PM",
        "07:00 PM",
        "08:00 PM",
        "09:00 PM",
        "10:00 PM",
        "11:00 PM",
      ],
    }

    this.viewPager = React.createRef()
  }

  componentDidMount() {
    SplashScreen.hide()

    // Db.write(() => {
    //   // Create a book object

    //   let allBooks = Db.objects('Pill');
    //   Db.delete(allBooks); // Deletes all books
    // });
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          AsyncStorage.getItem(
            "PillReminderTimeTicks"
          ).then((asyncStorageRes) => {
            if (
              asyncStorageRes != null &&
              asyncStorageRes != ""
            ) {
              this.getAllPillReminder(0)
            } else {
              this.getAllPillReminder(0)
            }
          })
          // this.getLocalPills()
        }
      )
  }

  getLocalPills() {
    const {
      morningDemoArray,
      afternoonDemoArray,
      eveningDemoArray,
    } = this.state
    let allPillData = Db.objects("Pill").filtered(
      "Day = $0",
      new Date(this.state.selectedDate)
    )

    let MorningArray = []
    let afternoonArray = []
    let eveningArray = []
    allPillData.map((item) => {
      var split_afternoon = 12 //24hr time to split the afternoon
      var split_evening = 17 //24hr time to split the evening
      var currentHour = moment(
        item.Time,
        "hh:mm a"
      ).format("HH")
      console.log("24 hours", currentHour)
      if (
        currentHour >= split_afternoon &&
        currentHour <= split_evening
      ) {
        afternoonArray.push(item)
      } else if (currentHour >= split_evening) {
        eveningArray.push(item)
      } else {
        MorningArray.push(item)
      }

      // if (morningDemoArray.includes(item.Time)) {
      //   MorningArray.push(item)
      // } else if (
      //   afternoonDemoArray.includes(item.Time)
      // ) {
      //   afternoonArray.push(item)
      // } else if (
      //   eveningDemoArray.includes(item.Time)
      // ) {
      //   eveningArray.push(item)
      // }
    })
    let summaryArray = Db.objects("PillReminder")

    this.setState({
      pillArray: allPillData,
      morningArray: MorningArray,
      afternoonArray: afternoonArray,
      eveningArray: eveningArray,
      summaryArray: summaryArray,
    })
  }

  onDateSelected = (date) => {
    this.setState(
      {
        formattedDate: date.format("MMM DD"),
        selectedDate:
          moment(date).format("YYYY-MM-DD"),
      },
      () => {
        this.getLocalPills()
      }
    )
  }
  render() {
    var fill =
      (this.getTakenCount() /
        this.state.pillArray.length) *
      100

    fill = fill > 0 ? fill : 0
    const {
      showDate,
      formattedDate,
      pageIndex,
      isLoading,
    } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {isLoading ? Constant.showLoader() : null}

        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: pageIndex == 0 ? 4 : 2,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              left: 0,
              right: 0,
              marginTop: 10,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 10,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              {language.pillreminder.toUpperCase()}
            </Text>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}>
              {/* <Text style = {[{left:0,flex:1,fontSize:16},BaseStyle.boldFont,BaseStyle.headerFont]}>PILL REMINDER</Text> */}

              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    showDate: !showDate,
                  })
                }}
                style={{
                  marginRight: 15,
                  alignItems: "center",
                  flexDirection: "row",
                  justifyContent: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 12,
                      color:
                        Constant.selectedTextColor(),
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {formattedDate}
                </Text>
                <Image
                  resizeMode={"center"}
                  tintColor={Constant.selectedTextColor()}
                  style={{
                    height: 10,
                    width: 10,
                    marginLeft: 4,
                  }}
                  source={images.downArrow}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              height: 50,
              width: "100%",
              paddingTop: 8,
              flexDirection: "row",
            }}>
            <TouchableOpacity
              onPress={() => {
                this.viewPager.current.setPage(0)
              }}
              style={{
                flex: 0.5,
                alignItems: "center",
                justifyContent: "center",
                borderBottomColor:
                  Constant.selectedTextColor(),
                borderBottomWidth:
                  pageIndex == 0 ? 2 : 0,
              }}>
              <Text
                style={[
                  {
                    fontSize: 16,
                    color:
                      Constant.selectedTextColor(),
                  },
                  BaseStyle.regularFont,
                ]}>
                {language.reminder}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.viewPager.current.setPage(1)
              }}
              style={{
                flex: 0.5,
                alignItems: "center",
                justifyContent: "center",
                borderBottomColor:
                  Constant.selectedTextColor(),
                borderBottomWidth:
                  pageIndex == 1 ? 2 : 0,
              }}>
              <Text
                style={[
                  {
                    fontSize: 16,
                    color:
                      Constant.selectedTextColor(),
                  },
                  BaseStyle.regularFont,
                ]}>
                {language.summary}
              </Text>
            </TouchableOpacity>
          </View>

          {showDate ? (
            <View>
              <CalendarStrip
                calendarAnimation={{
                  type: "sequence",
                  duration: 30,
                }}
                selectedDate={new Date()}
                daySelectionAnimation={{
                  type: "background",
                  duration: 200,
                  borderWidth: 1,
                  highlightColor: "#67af23",
                }}
                style={{
                  height: 80,
                  paddingTop: 10,
                  paddingBottom: 0,
                }}
                calendarHeaderStyle={{
                  color:
                    Constant.selectedTextColor(),
                }}
                calendarColor={Constant.headerColor()}
                highlightDateNumberStyle={[
                  BaseStyle.boldFont,
                  {
                    color:
                      Constant.selectedTextColor(),
                    marginTop: 5,
                    fontSize: 18,
                  },
                ]}
                highlightDateNameStyle={[
                  BaseStyle.regularFont,
                  {
                    color:
                      Constant.selectedTextColor(),
                    fontSize: 13,
                  },
                ]}
                disabledDateNameStyle={{
                  color:
                    Constant.selectedTextColor(),
                }}
                disabledDateNumberStyle={{
                  color:
                    Constant.selectedTextColor(),
                }}
                scrollable
                onDateSelected={this.onDateSelected}
                useNativeDriver
                dateNameStyle={[
                  BaseStyle.regularFont,
                  {
                    color:
                      Constant.selectedTextColor(),
                    fontSize: 13,
                  },
                ]}
                dateNumberStyle={[
                  BaseStyle.boldFont,
                  {
                    color:
                      Constant.selectedTextColor(),
                    fontSize: 18,
                    marginTop: 5,
                  },
                ]}
                calendarHeaderStyle={[
                  BaseStyle.regularFont,
                  {
                    color:
                      Constant.selectedTextColor(),
                    height: 0,
                  },
                ]}
                iconLeftStyle={{
                  tintColor: "#fff",
                }}
                iconRightStyle={{
                  tintColor: "#fff",
                }}
                iconContainer={{ flex: 0.1 }}
              />
            </View>
          ) : null}

          {pageIndex == 0 ? (
            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                paddingTop: 10,
                marginBottom: 20,
                flexDirection: "row",
                justifyContent: "space-between",
              }}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  width: "100%",
                  alignItems: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 18,
                      color:
                        Constant.selectedTextColor(),
                      marginTop: 8,
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {this.getTakenCount()}/
                  {this.state.pillArray.length}{" "}
                  taken
                </Text>

                <View
                  style={{
                    backgroundColor: "#fff",
                    elevation: 4,
                    borderRadius: 27,
                    padding: 2,
                  }}>
                  <AnimatedCircularProgress
                    size={40}
                    duration={500}
                    width={2}
                    fill={fill}
                    tintColor={Constant.appColorAlpha()}
                    onAnimationComplete={() =>
                      console.log(
                        "onAnimationComplete"
                      )
                    }
                    backgroundColor="#D3D3D3">
                    {(fill) => (
                      <Text
                        style={[
                          {
                            color:
                              Constant.appColorAlpha(),
                            fontSize: 11,
                          },
                          BaseStyle.boldFont,
                        ]}>
                        {fill.toFixed(1)}%
                      </Text>
                    )}
                  </AnimatedCircularProgress>
                </View>
              </View>
            </View>
          ) : null}
        </View>

        <ViewPager
          style={{ flex: 1 }}
          ref={this.viewPager}
          scrollEnabled={false}
          initialPage={0}
          onPageSelected={this.onPageSelected}>
          <View key="1">{this.reminderUi()}</View>
          <View key="2">{this.summaryUi()}</View>
        </ViewPager>

        <View
          style={{
            position: "absolute",
            right: 30,
            bottom: 30,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate(
                "AddMedicine"
              )
            }}
            style={{
              height: 50,
              justifyContent: "center",
              alignItems: "center",
              width: 50,
              borderRadius: 25,
              elevation: 5,
              backgroundColor: "#fff",
            }}>
            <Image
              tintColor={Constant.appFullColor()}
              style={{
                borderRadius: 20,
                width: 25,
                height: 25,
              }}
              source={images.plus}
            />
          </TouchableOpacity>
        </View>

        {this.showReschedule()}
      </SafeAreaView>
    )
  }

  getTakenCount() {
    let data = this.state.pillArray.filter(
      (item) => {
        return item.IsTaken == true
      }
    )
    return data.length
  }

  getRenderArray() {
    const {
      dayViewIndex,
      morningArray,
      eveningArray,
      afternoonArray,
    } = this.state
    if (dayViewIndex == 0) {
      return morningArray
    } else if (dayViewIndex == 1) {
      return afternoonArray
    } else if (dayViewIndex == 2) {
      return eveningArray
    }

    return []
  }

  showReschedule() {
    const { showReschedule, selectedPill } =
      this.state

    let pillData = Db.objectForPrimaryKey(
      "PillReminder",
      selectedPill.PillReminderId
    )

    if (pillData == null) {
      return
    }

    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={showReschedule}
        onRequestClose={() => {
          this.setState({ showReschedule: false })
        }}>
        {this.state.showTimePicker && (
          <DateTimePicker
            testID="dateTimePicker"
            value={new Date()}
            mode={"time"}
            is24Hour={false}
            display="default"
            onChange={(event, selectedDate) => {
              const date =
                moment(selectedDate).format(
                  "hh:mm a"
                )
              const { selectedPill } = this.state
              console.log(
                "SelectedPillData",
                selectedPill
              )
              Db.write(() => {
                selectedPill.Time = `${date}`
              })
              this.setState({
                showReschedule: false,
                showTimePicker: false,
              })
              this.getLocalPills()
            }}
          />
        )}
        <View
          style={{
            flex: 1,
            width: "100%",
            height: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}>
          <TouchableOpacity
            onPress={() => {
              this.setState({
                showReschedule: false,
              })
            }}
            activeOpacity={1}
            style={{
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              backgroundColor: "rgba(0,0,0,0.4)",
              position: "absolute",
            }}></TouchableOpacity>
          {/* <View style = {{width:width,height:height,backgroundColor:'red',justifyContent:'center',alignItems:'center'}}> */}

          <View
            style={{
              width: width - 40,
              height: 228,
              overflow: "hidden",
              padding: 10,
              backgroundColor: "#fff",
              borderRadius: 6,
            }}>
            <Text
              style={[
                { fontSize: 23 },
                BaseStyle.boldFont,
              ]}>
              {selectedPill.Time}
            </Text>
            <View
              style={{
                flexDirection: "row",
                marginTop: 15,
              }}>
              <ImageLoad
                borderRadius={20}
                placeholderStyle={{
                  height: 40,
                  width: 40,
                }}
                style={{ height: 40, width: 40 }}
                loadingStyle={{
                  size: "large",
                  color: Constant.appColorAlpha(),
                }}
                source={{
                  uri: !!pillData.Image
                    ? pillData.Image
                    : "url",
                }}
                placeholderSource={
                  images.placeholder
                }
              />

              {/* <Image style = {{height:40,width:40,borderRadius:20}} source ={images.testNew}/> */}
              <View style={{ paddingLeft: 10 }}>
                <Text
                  numberOfLines={1}
                  style={[
                    { fontSize: 15 },
                    BaseStyle.boldFont,
                  ]}>
                  {pillData.PillName}
                </Text>
                <Text
                  style={[
                    {
                      fontSize: 13,
                      color: Constant.textColor(),
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {pillData.Dosage} pill,{" "}
                  {this.getdoseTimes(
                    pillData.DoseTimes
                  )}
                </Text>
              </View>
            </View>

            <View
              style={{
                height: 50,
                flexDirection: "row",
                marginTop: 15,
                justifyContent: "space-between",
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.updatePillStatus(false)
                }}
                style={{
                  flex: 0.44,
                  marginLeft: 10,
                  borderRadius: 25,
                  borderWidth: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  borderColor: Constant.textColor(),
                }}>
                <Text
                  style={[
                    {
                      fontSize: 15,
                      color: Constant.textColor(),
                    },
                    BaseStyle.regularFont,
                  ]}>
                  Skip for now
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    showTimePicker: true,
                  })
                }}
                style={{
                  flex: 0.44,
                  marginLeft: 10,
                  borderRadius: 25,
                  borderWidth: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  borderColor: Constant.textColor(),
                }}>
                <Text
                  style={[
                    {
                      fontSize: 15,
                      color: Constant.textColor(),
                    },
                    BaseStyle.regularFont,
                  ]}>
                  Re-schedule
                </Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              onPress={() => {
                this.updatePillStatus(true)
              }}
              style={{
                height: 50,
                width: width - 40,
                marginLeft: -10,
                flexDirection: "row",
                marginTop: 15,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor:
                  Constant.appColorAlpha(),
              }}>
              <Text
                style={[
                  { fontSize: 16, color: "#fff" },
                  BaseStyle.boldFont,
                ]}>
                TAKEN
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }
  getdoseTimes(dosetime) {
    var txt = "Once per day"
    if (dosetime == "1/4") {
      txt = "Once per day"
    } else if (dosetime == "2/4") {
      txt = "Twice per day"
    } else if (dosetime == "3/4") {
      txt = "Three times per day"
    } else {
      txt = "Four times per day"
    }
    return txt
  }
  updatePillStatus(isTaken) {
    this.setState({ isLoading: true })
    const { selectedPill } = this.state
    let params = {
      id: selectedPill.Id,
      isTaken: isTaken
        ? true
        : selectedPill.IsTaken,
      isSkipped: isTaken
        ? selectedPill.IsSkipped
        : true,
    }
    Constant.postMethod(
      urls.updatePillStatus,
      params,
      (result) => {
        this.setState({
          isLoading: false,
          showReschedule: false,
        })
        console.log(
          "pill status" + JSON.stringify(result)
        )
        if (result.success) {
          try {
            Db.write(() => {
              let pillData = Db.objectForPrimaryKey(
                "Pill",
                selectedPill.Id
              )
              if (isTaken) {
                pillData.IsTaken = !pillData.IsTaken
              } else {
                pillData.IsSkipped =
                  !pillData.IsSkipped
              }
            })
          } catch (e) {
            console.log("Error on creation")
            console.log(e)
          }

          this.setState({ showReschedule: false })
        }
      }
    )
  }

  reminderUi() {
    const {
      showDate,
      formattedDate,
      dayViewIndex,
    } = this.state
    let renderArray = this.getRenderArray()
    return (
      <View>
        <View style={styles.dayMainView}>
          <TouchableOpacity
            onPress={() => {
              this.setState({ dayViewIndex: 0 })
            }}
            style={[
              styles.dayView,
              {
                backgroundColor:
                  dayViewIndex == 0
                    ? Constant.appColorAlpha()
                    : Constant.selectedTextColor(),
              },
            ]}>
            <Text
              style={[
                styles.dayText,
                BaseStyle.boldFont,
                {
                  color:
                    dayViewIndex == 0
                      ? Constant.selectedTextColor()
                      : Constant.headerColor(),
                },
              ]}>
              {language.morning}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.setState({ dayViewIndex: 1 })
            }}
            style={[
              styles.dayView,
              {
                backgroundColor:
                  dayViewIndex == 1
                    ? Constant.appColorAlpha()
                    : Constant.selectedTextColor(),
              },
            ]}>
            <Text
              style={[
                styles.dayText,
                BaseStyle.boldFont,
                {
                  color:
                    dayViewIndex == 1
                      ? Constant.selectedTextColor()
                      : Constant.headerColor(),
                },
              ]}>
              {language.afternoon}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.setState({ dayViewIndex: 2 })
            }}
            style={[
              styles.dayView,
              {
                backgroundColor:
                  dayViewIndex == 2
                    ? Constant.appColorAlpha()
                    : Constant.selectedTextColor(),
              },
            ]}>
            <Text
              style={[
                styles.dayText,
                BaseStyle.boldFont,
                {
                  color:
                    dayViewIndex == 2
                      ? Constant.selectedTextColor()
                      : Constant.headerColor(),
                },
              ]}>
              {language.evening}
            </Text>
          </TouchableOpacity>
        </View>

        <SwipeListView
          data={renderArray}
          keyExtractor={(item) => item.Id}
          extraData={this.state}
          renderItem={(data) => {
            let pillData = Db.objectForPrimaryKey(
              "PillReminder",
              data.item.PillReminderId
            )
            console.log(
              "pilldata" + JSON.stringify(pillData)
            )
            if (pillData == null) {
              return <View></View>
            } else {
              return (
                <View
                  key={data.index}
                  style={{
                    width: "100%",
                    padding: 10,
                    marginBottom:
                      data.index ==
                      renderArray.length - 1
                        ? 50
                        : 0,
                  }}>
                  <View
                    style={{
                      elevation: 4,
                      borderRadius: 8,
                      backgroundColor: "#fff",
                      flexDirection: "row",
                    }}>
                    {/* <Image resizeMode={'center'} style = {{width:100,height:110}} source = {images.testNew}/> */}

                    <ImageLoad
                      placeholderStyle={{
                        width: 100,
                        height: 110,
                      }}
                      style={{
                        width: 100,
                        height: 110,
                      }}
                      loadingStyle={{
                        size: "large",
                        color:
                          Constant.appColorAlpha(),
                      }}
                      source={{
                        uri: !!pillData.Image
                          ? pillData.Image
                          : "url",
                      }}
                      placeholderSource={
                        images.placeholder
                      }
                    />

                    <View
                      style={{
                        flex: 1,
                        marginTop: 15,
                        paddingLeft: 10,
                        justifyContent:
                          "space-between",
                        marginBottom: 15,
                      }}>
                      <View>
                        <Text
                          numberOfLines={1}
                          style={[
                            {
                              fontSize: 18,
                              marginRight: 10,
                            },
                            BaseStyle.boldFont,
                          ]}>
                          {pillData.PillName}
                        </Text>
                        <Text
                          style={[
                            {
                              fontSize: 13,
                              marginTop: 2,
                              color:
                                Constant.textAlpha(),
                            },
                            BaseStyle.regularFont,
                          ]}>
                          {pillData.Dosage} pill,{" "}
                          {this.getdoseTimes(
                            pillData.DoseTimes
                          )}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent:
                            "space-between",
                          width: "94%",
                        }}>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent:
                              "center",
                            alignItems: "center",
                          }}>
                          <Image
                            style={{
                              width: 15,
                              height: 15,
                            }}
                            source={
                              images.clockFill
                            }
                          />
                          <Text
                            style={[
                              {
                                fontSize: 13,
                                marginLeft: 5,
                                color:
                                  Constant.appFullColor(),
                              },
                              BaseStyle.regularFont,
                            ]}>
                            {data.item.Time}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent:
                              "center",
                          }}>
                          <Image
                            tintColor={
                              data.item.IsTaken
                                ? Constant.appColorAlpha()
                                : Constant.textAlpha()
                            }
                            resizeMode={"center"}
                            style={{
                              width: 20,
                              height: 20,
                            }}
                            source={images.tick}
                          />
                          <Image
                            resizeMode={"center"}
                            style={{
                              marginLeft: 12,
                              width: 20,
                              height: 20,
                            }}
                            source={
                              data.index == 1
                                ? images.bell
                                : images.bellOff
                            }
                          />
                          <Image
                            resizeMode={"center"}
                            style={{
                              marginLeft: 12,
                              width: 20,
                              height: 20,
                            }}
                            source={
                              images.nextThree
                            }
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              )
            }
          }}
          renderHiddenItem={(data, rowMap) => (
            <TouchableOpacity
              onPress={() => {
                this.closeRow(rowMap, data.item)
                this.setState({
                  selectedPill: data.item,
                  showReschedule: true,
                })
              }}
              key={data}
              style={{
                height: "84%",
                width: width - 20,
                position: "absolute",
                borderRadius: 8,
                right: 10,
                marginTop: 10,
                paddingBottom: 10,
                height: 110,
                backgroundColor:
                  Constant.headerColor(),
              }}>
              <View
                style={{
                  width: 90,
                  position: "absolute",
                  right: 10,
                  height: "100%",
                  borderTopRightRadius: 8,
                  borderBottomRightRadius: 8,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor:
                    Constant.headerColor(),
                }}>
                <Image
                  style={{ height: 20, width: 20 }}
                  source={images.calendar}
                />
                <Text
                  style={[
                    {
                      fontSize: 13,
                      marginTop: 2,
                      marginTop: 5,
                      color:
                        Constant.selectedTextColor(),
                    },
                    BaseStyle.regularFont,
                  ]}>
                  Reschedule
                </Text>
              </View>
            </TouchableOpacity>
          )}
          leftOpenValue={0}
          rightOpenValue={-100}
        />
      </View>
    )
  }

  closeRow(rowMap, rowKey) {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow()
    }
  }

  summaryUi() {
    const {
      showDate,
      formattedDate,
      dayViewIndex,
      summaryArray,
    } = this.state

    return (
      <View>
        <FlatList
          data={this.state.summaryArray}
          keyExtractor={(item) => item.Id}
          renderItem={({ item, index }) => (
            <View
              style={{
                width: "100%",
                padding: 10,
                backgroundColor: "#fff",
                marginBottom:
                  index ==
                  this.state.summaryArray.length - 1
                    ? 50
                    : 0,
              }}>
              <View
                style={{
                  elevation: 4,
                  borderRadius: 8,
                  backgroundColor: "#fff",
                  flexDirection: "row",
                }}>
                {/* <Image resizeMode={'center'} style = {{width:100,height:110}} source = {images.PillName}/> */}

                <ImageLoad
                  placeholderStyle={{
                    width: 100,
                    height: 110,
                  }}
                  style={{
                    width: 100,
                    height: 110,
                  }}
                  loadingStyle={{
                    size: "large",
                    color: Constant.appColorAlpha(),
                  }}
                  source={{
                    uri: !!item.Image
                      ? item.Image
                      : "url",
                  }}
                  placeholderSource={
                    images.placeholder
                  }
                />

                <View
                  style={{
                    flex: 1,
                    marginLeft: 10,
                    marginTop: 15,
                    justifyContent: "space-between",
                    height: 100,
                  }}>
                  <View
                    style={{ paddingRight: 10 }}>
                    <View
                      style={{
                        flexDirection: "row",
                        width: "100%",
                        justifyContent:
                          "space-between",
                      }}>
                      <Text
                        numberOfLines={1}
                        style={[
                          { fontSize: 18, flex: 1 },
                          BaseStyle.boldFont,
                        ]}>
                        {item.PillName}
                      </Text>
                      <TouchableOpacity
                        activeOpacity={2}
                        onPress={() => {
                          this.deletePopUP(item.Id)
                        }}>
                        <Image
                          style={{
                            marginLeft: 10,
                            width: 20,
                            height: 20,
                          }}
                          source={images.delete}
                        />
                      </TouchableOpacity>
                    </View>
                    <Text
                      style={[
                        {
                          fontSize: 13,
                          marginTop: 2,
                          lineHeight: 16,
                          color:
                            Constant.textAlpha(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {item.Brand}
                    </Text>
                  </View>

                  <View
                    style={{
                      height: 45,
                      marginBottom: 10,
                      width: "95%",
                      flexDirection: "row",
                      justifyContent:
                        "space-between",
                      paddingTop: 10,
                    }}>
                    <View
                      style={{
                        flex: 0.47,
                        justifyContent: "center",
                        alignItems: "center",
                        borderColor:
                          Constant.appFullColor(),
                        borderWidth: 1,
                        borderRadius: 6,
                      }}>
                      <Text
                        style={[
                          {
                            fontSize: 13,
                            textAlign: "center",
                            lineHeight: 15,
                            color:
                              Constant.appFullColor(),
                          },
                          BaseStyle.regularFont,
                        ]}>
                        {language.orderMedicine}
                      </Text>
                    </View>

                    <TouchableOpacity
                      style={{
                        flex: 0.47,
                        justifyContent: "center",
                        alignItems: "center",
                        borderColor:
                          Constant.appFullColor(),
                        borderWidth: 1,
                        borderRadius: 6,
                      }}
                      onPress={() => {
                        this.props.navigation.navigate(
                          "AddMedicine",
                          {
                            isEdit: true,
                            selectedItem: item,
                          }
                        )
                      }}>
                      <Text
                        style={[
                          {
                            fontSize: 13,
                            textAlign: "center",
                            lineHeight: 15,
                            color:
                              Constant.appFullColor(),
                          },
                          BaseStyle.regularFont,
                        ]}>
                        {language.setreminder}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          )}
        />
      </View>
    )
  }
  async createNotifications(pills) {
    pills.PillReminderSlots.map((item) => {
      const dateOld = item.Day
      let date =
        moment(dateOld).format("YYYY-MM-DD")
      const time = item.Time
      //moment datetime obj
      const dateTime = moment(
        date + " " + time,
        "YYYY-MM-DD hh:mm a"
      )
      //format you wanted
      const formatedDateTime = dateTime.format(
        "YYYY-MM-DD hh:mm a"
      )

      if (new Date(dateTime) > new Date()) {
        console.log(
          "future Pill reminder fire time",
          new Date(dateTime)
        )
        PushNotification.cancelLocalNotification(id)

        const pillData = {
          pillType: pills.PillType,
          shape: pills.ShapeType,
          color: pills.ColorCode,
          image: pills.Image,
          dose: pills.Dosage,
          name: pills.PillName,
          id: pills.Id,
        }

        PushNotification.localNotificationSchedule({
          id: pills.Id,
          title: pills.PillName,
          date: new Date(dateTime),
          message: pills.PillName,
          allowWhileIdle: false,
          data: JSON.stringify(pillData),
          channelId: "default-channel-id",
        })
      }
    })
  }
  getAllPillReminder(time) {
    this.setState({
      isLoading: time != 0 ? false : true,
    })

    let params = {
      CustomerId: global.CustomerId,
      LastUpdatedTimeTicks: 0,
    }
    Constant.postMethod(
      urls.getPillReminder,
      params,
      (result) => {
        this.setState({ isLoading: false })
        if (result.success) {
          if (result.result.Response != null) {
            let pillReminder =
              result.result.Response.pillReminder

            pillReminder.map((pill) => {
              this.createNotifications(pill)
              CartDb.addPillReminder(pill)
            })
          }
        }
        AsyncStorage.setItem(
          "PillReminderTimeTicks",
          JSON.stringify(
            result.result.Response
              .LastUpdatedTimeTicks
          )
        )

        this.getLocalPills()
      }
    )
  }

  deletePopUP(Id) {
    Alert.alert(
      "",
      "Do you want to delete this Medication Reminder?",
      [
        { text: "CANCEL" },
        {
          text: "DELETE",
          onPress: () => this.deletePill(Id),
        },
      ]
      // { cancelable: true }
    )
  }

  deletePill(Id) {
    this.setState({ isLoading: true })
    let params = {
      id: Id,
      customerId: global.CustomerId,
    }

    console.log(params)

    Constant.deleteMethod(
      urls.deletePillReminder,
      params,
      (result) => {
        this.setState({ isLoading: false })
console.log(JSON.stringify(result))
        // alert(JSON.stringify(result))
        if (result.success) {
          const deletedData =
            Db.objectForPrimaryKey(
              "PillReminder",
              Id
            )
          deletedData.PillReminderSlots.map(
            (item) => {
              PushNotification.cancelLocalNotification(
                item.id
              )
              try {
                Db.write(() => {
                  Db.delete(
                    Db.objectForPrimaryKey(
                      "Pill",
                      item.Id
                    )
                  )
                })
              } catch (e) {
                console.log(
                  "Error on pill data delete"
                )
                console.log(JSON.stringify(e))
              }
              return true
            }
          )

          try {
            Db.write(() => {
              Db.delete(
                Db.objectForPrimaryKey(
                  "PillReminder",
                  Id
                )
              )
            })
          } catch (e) {
            console.log("Error on pill delete")
            console.log(JSON.stringify(e))
          }
          this.getLocalPills()
        } else {
          //   this.setState({isNumberCorrect:false})
        }
      }
    )
  }

  onPageSelected = (e) => {
    this.setState({
      pageIndex: e.nativeEvent.position,
    })
  }
}

const styles = StyleSheet.create({
  dayView: {
    flex: 0.3,
    backgroundColor: Constant.appColorAlpha(),
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    elevation: 4,
  },
  dayMainView: {
    height: 50,
    padding: 5,
    width: "100%",
    flexDirection: "row",
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 8,
    justifyContent: "space-between",
  },
  dayText: {
    color: Constant.selectedTextColor(),
    fontSize: 16,
  },
})

export default AllMedicine
