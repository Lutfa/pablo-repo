import React, { Component } from "react"
import {
  View,
  ImageBackground,
  FlatList,
  AsyncStorage,
  AppState,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import SwipeListView from "./../ExtraClass/SwipeList/SwipeListView"
import SplashScreen from "react-native-splash-screen"
import ViewPager from "@react-native-community/viewpager"
import language from "./../BaseClass/language"
import Urls from "./../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import CartDb from "./../DB/CartDb"
import Db from "./../DB/Realm"
import moment from "moment"

class Orders extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showLoader: false,
      orderdArray: [],
      prescriptionArray: [],
      nonPresscriptionArray: [],
      pageIndex: 0,
    }

    this.viewPager = React.createRef()
  }

  componentDidMount() {
    let orders = Db.objects("Orders").sorted(
      "OrderId",
      true
    )
    let prescriptionArray = orders.filter(
      (item) => {
        return item.HasPrescription
      }
    )
    let nonPresscriptionArray = orders.filter(
      (item) => {
        return !item.HasPrescription
      }
    )
    this.setState({
      orderdArray: orders,
      prescriptionArray,
      nonPresscriptionArray,
    })

    //   CartDb.deleteDb('Orders')
    //   CartDb.deleteDb('OrderItems')
    //   CartDb.deleteDb('OrderStatusLogs')

    AsyncStorage.getItem("OrdersTimeTicks").then(
      (asyncStorageRes) => {
        if (
          asyncStorageRes != null &&
          asyncStorageRes != ""
        ) {
          this.getAllOrders(asyncStorageRes)
        } else {
          this.getAllOrders(0)
        }
      }
    )

    SplashScreen.hide()
  }

  render() {
    const {
      pageIndex,
      showLoader,
      prescriptionArray,
      orderdArray,
      nonPresscriptionArray,
    } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {showLoader ? Constant.showLoader() : null}
        <View style={styles.container}>
          <View style={styles.headerView}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              {language.orders.toUpperCase()}
            </Text>
          </View>

          <View
            style={{
              height: 30,
              width: "100%",
              marginBottom: 1,
              flexDirection: "row",
              justifyContent: "space-between",
            }}>
            <TouchableOpacity
              onPress={() => {
                this.viewPager.current.setPage(0)
              }}
              style={[
                styles.tabStyle,
                {
                  borderBottomWidth:
                    pageIndex == 0 ? 2 : 0,
                },
              ]}>
              <Text
                style={[
                  styles.tabText,
                  BaseStyle.regularFont,
                ]}>
                {language.all}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.viewPager.current.setPage(1)
              }}
              style={[
                styles.tabStyle,
                {
                  borderBottomWidth:
                    pageIndex == 1 ? 2 : 0,
                },
              ]}>
              <Text
                style={[
                  styles.tabText,
                  BaseStyle.regularFont,
                ]}>
                {language.prescription}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.viewPager.current.setPage(2)
              }}
              style={[
                styles.tabStyle,
                {
                  borderBottomWidth:
                    pageIndex == 2 ? 2 : 0,
                },
              ]}>
              <Text
                style={[
                  styles.tabText,
                  BaseStyle.regularFont,
                ]}>
                {language.non_pres}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <ViewPager
          style={{ flex: 1 }}
          ref={this.viewPager}
          scrollEnabled={true}
          initialPage={0}
          onPageSelected={this.onPageSelected}>
          <View key="1">
            {this.renderUi(orderdArray)}
          </View>
          <View key="2">
            {this.renderUi(prescriptionArray)}
          </View>

          <View key="3">
            {this.renderUi(nonPresscriptionArray)}
          </View>
        </ViewPager>
      </SafeAreaView>
    )
  }

  getColor(text) {
    if (text == "Pending") {
      return Constant.yellowColor()
    } else if (text == "Delivered") {
      return Constant.blueColor()
    } else if (text == "Cancelled") {
      return Constant.redColor()
    } else if (text == "Complete") {
      return Constant.appFullColor()
    }

    return Constant.yellowColor()
  }

  getText(text) {
    if (text == "Pending") {
      return "Your order is under review"
    } else if (text == "Delivered") {
      return "Your order will be delivered"
    } else if (text == "Cancelled") {
      return "Your order is under review"
    } else if (text == "Complete") {
      return "your order has been completed"
    }
    return "Your order is under review"
  }

  renderUi(data) {
    return (
      <FlatList
        data={data}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate(
                "OrderDetails",
                { data: item }
              )
            }}
            activeOpacity={2}
            style={{
              height: 164,
              width: "100%",
              padding: 15,
            }}>
            <View style={styles.orderView}>
              <View
                style={{
                  width: "70%",
                  height: "100%",
                }}>
                <View
                  style={{
                    flexDirection: "row",
                    padding: 8,
                    width: "100%",
                  }}>
                  <View style={styles.itemImage}>
                    <Image
                      resizeMode={"contain"}
                      style={{
                        width: 24,
                        height: 24,
                      }}
                      source={images.uReport}
                    />
                  </View>
                  <View
                    style={{
                      marginLeft: 8,
                      marginRight: 8,
                      flex: 1,
                    }}>
                    <Text
                      style={[
                        { fontSize: 13 },
                        BaseStyle.boldFont,
                      ]}>
                      {language.order_no.toUpperCase()}{" "}
                      - {item.OrderId}
                    </Text>
                    <Text
                      numberOfLines={2}
                      style={[
                        styles.addres,
                        BaseStyle.regularFont,
                      ]}>
                      {item.Address1}
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    padding: 8,
                    paddingTop: 3,
                  }}>
                  <View style={{ flex: 0.4 }}>
                    <Text
                      style={[
                        styles.status,
                        BaseStyle.boldFont,
                      ]}>
                      {language.status.toUpperCase()}
                    </Text>
                    <Text
                      style={[
                        {
                          fontSize: 10,
                          color: this.getColor(
                            item.OrderStatus
                          ),
                        },
                        BaseStyle.boldFont,
                      ]}>
                      {item.OrderStatus}
                    </Text>
                  </View>

                  <View style={{ flex: 0.6 }}>
                    <Text
                      style={[
                        styles.status,
                        BaseStyle.boldFont,
                      ]}>
                      {language.purchasedate.toUpperCase()}
                    </Text>
                    <Text
                      style={[
                        { fontSize: 10 },
                        BaseStyle.boldFont,
                      ]}>
                      {
 moment.utc(item.CreatedOnUtc).utcOffset(moment().utcOffset()).format("DD/MM/YYYY hh:mm a")}

                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.lineView}></View>
              <View style={styles.amountView}>
                <Text
                  style={[
                    { fontSize: 15 },
                    BaseStyle.boldFont,
                  ]}>
                  {Constant.price()}
                  {parseFloat(
                    item.OrderSubtotalInclTax
                  ).toFixed(2)}
                </Text>
                <Text
                  numberOfLines={2}
                  style={[
                    styles.paid,
                    BaseStyle.regularFont,
                  ]}>
                  {item.PaymentStatus}
                </Text>

                {item.HasPrescription ? (
                  <View style={styles.rxView}>
                    <Image
                      resizeMode={"contain"}
                      style={{
                        width: 16,
                        height: 16,
                      }}
                      source={images.prescription}
                    />
                  </View>
                ) : null}
              </View>
            </View>

            <View
              style={{
                zIndex: 99,
                height: 35,
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
              }}>
              <View
                style={{
                  height: 35,
                  width: "90%",
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6,
                  backgroundColor: "#fff",
                  elevation: 3,
                }}>
                <Text
                  style={[
                    {
                      fontSize: 12,
                      textAlign: "center",
                      marginTop: 6,
                      color: this.getColor(
                        item.OrderStatus
                      ),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {this.getText(
                    item.OrderStatus
                  ).toUpperCase()}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
    )
  }

  onPageSelected = (e) => {
    this.setState({
      pageIndex: e.nativeEvent.position,
    })
  }

  getAllOrders(time) {
    // alert(time)
    //   global.CustomerId = 5879
    this.setState({
      showLoader: time == 0 ? true : false,
    })
    let params = { CustomerId: global.CustomerId }

    Constant.postMethod(
      Urls.getAllOrder,
      params,
      (result) => {
        this.setState({ showLoader: false })
        if (result.success) {
          if (result.result.Response != null) {
            result.result.Response.map((item) => {
              CartDb.addOrderData(item)
            })
          }
        }
        // AsyncStorage.setItem(
        //   "OrdersTimeTicks",
        //   JSON.stringify(
        //     result.result.Response
        //       .LastUpdatedTimeTicks
        //   )
        // )

        let orders = Db.objects("Orders").sorted(
          "OrderId",
          true
        )
        let prescriptionArray = orders.filter(
          (item) => {
            return item.HasPrescription
          }
        )
        let nonPresscriptionArray = orders.filter(
          (item) => {
            return !item.HasPrescription
          }
        )
        this.setState({
          orderdArray: orders,
          prescriptionArray,
          nonPresscriptionArray,
        })

        // this.setState({bestOffersArray:DiscountData})
      }
    )
  }
}

const styles = StyleSheet.create({
  mainView: {
    marginTop: 0,

    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
  },
  itemImage: {
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    elevation: 3,
    width: 40,
    backgroundColor: Constant.appLightColor(),
    borderRadius: 6,
  },
  rxView: {
    position: "absolute",
    top: 8,
    right: 0,
    height: 25,
    justifyContent: "center",
    alignItems: "center",
    width: 30,
    borderBottomLeftRadius: 12.5,
    borderTopLeftRadius: 12.5,
    backgroundColor: Constant.appColorAlpha(),
  },
  addres: {
    fontSize: 11,
    marginTop: 2,
    marginRight: 2,
    width: "100%",
    color: Constant.textAlpha(),
    lineHeight: 14,
  },
  paid: {
    fontSize: 11,
    marginTop: 5,
    color: Constant.textAlpha(),
    lineHeight: 14,
  },
  amountView: {
    width: "30%",
    height: null,
    justifyContent: "center",
    alignItems: "center",
  },
  lineView: {
    height: "100%",
    width: 1,
    backgroundColor: "#AEB1BB",
  },
  status: {
    fontSize: 10,
    color: Constant.textAlpha(),
  },
  orderView: {
    zIndex: 100,
    height: null,
    width: "100%",
    borderRadius: 6,
    flexDirection: "row",
    backgroundColor: "#fff",
    elevation: 3,
  },
  container: {
    backgroundColor: Constant.headerColor(),
    elevation: 3,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  headerView: {
    paddingTop: 10,
    paddingBottom: 15,
    backgroundColor: Constant.headerColor(),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  tabStyle: {
    flex: 0.31,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "#fff",
    borderBottomWidth: 2,
  },
  tabText: { fontSize: 14, color: "#fff" },
})

export default Orders
