import React, { Component } from "react"
import {
  View,
  ImageBackground,
  FlatList,
  AsyncStorage,
  AppState,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Modal,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import SwipeListView from "./../ExtraClass/SwipeList/SwipeListView"
import SplashScreen from "react-native-splash-screen"
import ViewPager from "@react-native-community/viewpager"
import language from "./../BaseClass/language"
import Urls from "./../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import CartDb from "./../DB/CartDb"
import Db from "./../DB/Realm"
import moment from "moment"
import ImageZoom from "react-native-image-pan-zoom"
import ImageViewer from "react-native-image-zoom-viewer"

class OrderDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showLoader: false,
      orderData:
        props.route.params != null
          ? props.route.params.data
          : {},
      presArray: [],
      nonPresArray: [],
      showAllLogs: false,
      showPrescription: false,
    }
  }

  componentDidMount() {
    SplashScreen.hide()
    const { orderData } = this.state

    let productArray = orderData.OrderItems
    var presArray = []
    var nonPresArray = []

    productArray.map((item) => {
      if (
        item.ColorCode ==
        Constant.prescriptionColorCode()
      ) {
        presArray.push(item)
      } else {
        nonPresArray.push(item)
      }
    })

    this.setState({ presArray, nonPresArray })
  }

  render() {
    const { orderData, showAllLogs } = this.state

    let lastObj =
      orderData.OrderStatusLogs.length > 0
        ? orderData.OrderStatusLogs[
            orderData.OrderStatusLogs.length - 1
          ]
        : {}

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        <View
          style={{
            backgroundColor: Constant.headerColor(),
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
            paddingBottom: 15,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 10,
              backgroundColor:
                Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              ORDER DETAILS
            </Text>

            {/* <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginRight: 15,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate(
                    "Chat"
                  )
                }}
                style={{
                  paddingRight: 12,
                  marginRight: 8,
                }}>
                <Image
                  resizeMode={"contain"}
                  tintColor={"#fff"}
                  style={{ width: 30, height: 30 }}
                  source={images.chatIcon}
                />
              </TouchableOpacity>
            </View> */}
          </View>
        </View>
        <ScrollView>
          <View
            style={{
              margin: 10,
              elevation: 2,
              backgroundColor: "#fff",
            }}>
            <Text
              style={[
                {
                  padding: 10,
                  fontSize: 14,
                  color: Constant.appFullColor(),
                },
                BaseStyle.regularFont,
              ]}>
              Note : For order related queries
              please connect us through chat
            </Text>
          </View>

          {orderData.OrderStatusLogs.length > 0 ? (
            <View
              style={{
                margin: 10,
                elevation: 2,
                backgroundColor: "#fff",
                borderRadius: 6,
                overflow: "hidden",
              }}>
              {showAllLogs ? (
                orderData.OrderStatusLogs.map(
                  (item, index) => {
                    return (
                      <View>
                        <View
                          style={{
                            flexDirection: "row",
                            padding: 6,
                          }}>
                          <Image
                            tintColor={Constant.appColorAlpha()}
                            style={{
                              height: 30,
                              width: 30,
                            }}
                            source={this.getImage(
                              lastObj.OrderStatusId
                            )}
                          />
                          <View
                            style={{
                              marginLeft: 8,
                              marginRight: 8,
                              flex: 1,
                            }}>
                            <Text
                              style={[
                                { fontSize: 13 },
                                BaseStyle.boldFont,
                              ]}>
                              {this.getValue(
                                item.OrderStatusId
                              )}
                            </Text>
                            <Text
                              numberOfLines={2}
                              style={[
                                {
                                  fontSize: 12,
                                  color:
                                    Constant.textColor(),
                                },
                                BaseStyle.regularFont,
                              ]}>
                              {moment.utc(item.CreatedOnUtc).utcOffset(moment().utcOffset()).format("DD/MM/YYYY hh:mm a")}
                            </Text>
                          </View>
                          {index == 0 ? (
                            <TouchableOpacity
                              onPress={() => {
                                this.setState({
                                  showAllLogs:
                                    !showAllLogs,
                                })
                              }}>
                              <Image
                                resizeMode={
                                  "contain"
                                }
                                style={{
                                  height: 20,
                                  width: 20,
                                }}
                                source={
                                  images.upArrow
                                }
                              />
                            </TouchableOpacity>
                          ) : null}
                        </View>
                      </View>
                    )
                  }
                )
              ) : (
                <View>
                  <View
                    style={{
                      flexDirection: "row",
                      padding: 6,
                    }}>
                    <Image
                      tintColor={Constant.appColorAlpha()}
                      style={{
                        height: 30,
                        width: 30,
                      }}
                      source={this.getImage(
                        lastObj.OrderStatusId
                      )}
                    />
                    <View
                      style={{
                        marginLeft: 8,
                        marginRight: 8,
                        flex: 1,
                      }}>
                      <Text
                        style={[
                          { fontSize: 13 },
                          BaseStyle.boldFont,
                        ]}>
                        {this.getValue(
                          lastObj.OrderStatusId
                        )}
                      </Text>
                      <Text
                        numberOfLines={2}
                        style={[
                          {
                            fontSize: 12,
                            color:
                              Constant.textColor(),
                          },
                          BaseStyle.regularFont,
                        ]}>
                          {moment.utc(lastObj.CreatedOnUtc).utcOffset(moment().utcOffset()).format("DD/MM/YYYY hh:mm a")}
                      </Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          showAllLogs: !showAllLogs,
                        })
                      }}>
                      <Image
                        resizeMode={"contain"}
                        style={{
                          height: 20,
                          width: 20,
                        }}
                        source={images.downArrow}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              )}

              <View
                style={{
                  padding: 10,
                  width: "100%",
                  backgroundColor:
                    Constant.appColorAlpha(),
                  flexDirection: "row",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 13,
                      color: "#fff",
                      flex: 1,
                      textAlign: "center",
                    },
                    BaseStyle.boldFont,
                  ]}>
                  Ask Order Status
                </Text>
                <Image
                  tintColor={"#fff"}
                  resizeMode={"contain"}
                  style={{ height: 20, width: 20 }}
                  source={images.leftarrow}
                />
              </View>
            </View>
          ) : null}

          {this.showddressUI()}
          {this.showProductUi()}
        </ScrollView>

        <View
          style={{
            height: 60,
            width: "100%",
            padding: 8,
            paddingLeft: 20,
            paddingRight: 20,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}>
          {orderData.PrescriptionUrl != null &&
          orderData.PrescriptionUrl != "null" ? (
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  showPrescription: true,
                })
              }}
              style={{
                flex: 1,
                borderRadius: 22,
                borderColor:
                  Constant.appColorAlpha(),
                padding: 5,
                borderWidth: 1,
                height: 44,
                alignItems: "center",
                justifyContent: "center",
                marginRight: 10,
              }}>
              <Text
                style={[
                  {
                    fontSize: 14,
                    textAlign: "center",
                    color: Constant.appColorAlpha(),
                  },
                  BaseStyle.boldFont,
                ]}>
                {language.ViewPrescription}
              </Text>
            </TouchableOpacity>
          ) : null}
          {/* {this.showCancel() ?
                 <TouchableOpacity
                 onPress = {()=>{
                     alert('cancel')
                 }}
                 style = {{flex:1,borderRadius:22,backgroundColor:Constant.appColorAlpha(),padding:5 ,height:44,alignItems:'center',justifyContent:'center'}}>
                 <Text style = {[{fontSize:14,textAlign:'center', color:'#fff'},BaseStyle.boldFont]}>Cancel Order</Text>
                 </TouchableOpacity>
                 : null } */}
        </View>
        {this.ViewPrescription()}
      </SafeAreaView>
    )
  }

  ViewPrescription() {
    const { showPrescription, orderData } =
      this.state

    let data = (
      orderData.PrescriptionUrl || ""
    ).split(",")

    if (data.length <= 0) {
      return
    }
    const images = data.map((item) => {
      return { url: item }
    })
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={showPrescription}
        onRequestClose={() => {
          this.setState({ showPrescription: false })
        }}>
        <View
          style={{
            backgroundColor: "rgba(0,0,0,0.5)",
            height: height,
            width: width,
          }}>
          <ImageZoom
            cropWidth={width}
            cropHeight={height}
            imageWidth={400}
            imageHeight={370}>
            <ImageViewer imageUrls={images} />
          </ImageZoom>

          <TouchableOpacity
            style={{
              position: "absolute",
              top: 20,
              right: 20,
              alignItems: "center",
              justifyContent: "center",
              height: 60,
              width: 60,
            }}
            onPress={() => {
              this.setState({
                showPrescription: false,
              })
            }}>
            <Image
              tintColor={"#fff"}
              style={{ height: 15, width: 15 }}
              source={images.close}
            />
          </TouchableOpacity>
        </View>
      </Modal>
    )
  }

  getValue(id) {
    if (id == 10) {
      return "Pending"
    } else if (id == 20) {
      return "Awaiting"
    } else if (id == 30) {
      return "Confirmed"
    } else if (id == 40) {
      return "Processing"
    } else if (id == 50) {
      return "Dispatched"
    } else if (id == 60) {
      return "Delivered"
    } else if (id == 70) {
      return "Complete"
    } else if (id == 80) {
      return "Cancelled"
    }
  }

  getImage() {
    if (id == 80) {
      return images.cancel
    }

    return images.verified
  }

  showCancel() {
    const { orderData } = this.state
    if (orderData.OrderStatus == "Complete") {
      return false
    }

    return true
  }

  getTotal(products) {
    let toalPrice = 0
    products.map((item) => {
      let oldPrice =
        item.Quantity *
        parseFloat(item.Product.Price)

      toalPrice = toalPrice + parseFloat(oldPrice)
    })

    return toalPrice
  }

  showProductUi() {
    const { nonPresArray, presArray, orderData } =
      this.state
    return (
      <View style={{ width: "100%", padding: 15 }}>
        <View
          style={{
            width: "100%",
            elevation: 4,
            backgroundColor: "#fff",
            padding: 6,
            marginTop: 15,
            borderRadius: 6,
          }}>
          <Text
            style={[
              { fontSize: 14 },
              BaseStyle.boldFont,
            ]}>
            {language.billDetails}
          </Text>
          <View
            style={{
              height: 3,
              width: 40,
              borderRadius: 1.5,
              backgroundColor:
                Constant.appColorAlpha(),
            }}
          />

          {presArray.length > 0 ? (
            <View>
              <Text
                style={[
                  {
                    fontSize: 12,
                    color: Constant.textColor(),
                    marginTop: 8,
                  },
                  BaseStyle.regularFont,
                ]}>
                {language.prescriptionMedicines.toUpperCase()}
              </Text>

              <FlatList
                data={presArray}
                renderItem={this.renderItem}
              />
            </View>
          ) : null}

          {nonPresArray.length > 0 ? (
            <View>
              <Text
                style={[
                  {
                    fontSize: 12,
                    color: Constant.textColor(),
                    marginTop: 10,
                  },
                  BaseStyle.regularFont,
                ]}>
                {language.nonPrescriptionMedicines.toUpperCase()}
              </Text>

              <FlatList
                data={nonPresArray}
                renderItem={this.renderItem}
              />
            </View>
          ) : null}
        </View>

        <View
          style={{
            width: "100%",
            elevation: 4,
            padding: 8,
            backgroundColor: "#fff",
            marginTop: 15,
            borderRadius: 6,
          }}>
          <View
            style={{
              flexDirection: "row",
              paddingBottom: 10,
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 11,
                  flex: 1,
                  color: Constant.textColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {language.subtotal}(
              {language.prescriptionMedicines})
            </Text>
            <Text
              style={[
                {
                  fontSize: 11,
                  textAlign: "right",
                  paddingRight: 8,
                  width: 100,
                  color: Constant.redColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {Constant.price()}{" "}
              {this.getTotal(presArray)}
            </Text>
          </View>

          <View
            style={{
              flexDirection: "row",
              paddingBottom: 10,
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 11,
                  flex: 1,
                  color: Constant.textColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {language.subtotal}(
              {language.nonPrescriptionMedicines})
            </Text>
            <Text
              style={[
                {
                  fontSize: 11,
                  textAlign: "right",
                  paddingRight: 8,
                  width: 100,
                  color: Constant.redColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {Constant.price()}{" "}
              {this.getTotal(nonPresArray)}
            </Text>
          </View>

          <View
            style={{
              flexDirection: "row",
              paddingBottom: 10,
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 11,
                  flex: 1,
                  color: Constant.textColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {language.combined}{" "}
              {language.subtotal}
            </Text>
            <Text
              style={[
                {
                  fontSize: 11,
                  textAlign: "right",
                  paddingRight: 8,
                  width: 100,
                  color: Constant.blueColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {Constant.price()}{" "}
              {parseFloat(
                orderData.OrderTotal
              ).toFixed(2)}
            </Text>
          </View>

          <View
            style={{
              flexDirection: "row",
              paddingBottom: 10,
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 11,
                  flex: 1,
                  color: Constant.textColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {language.servicefee}
            </Text>
            <Text
              style={[
                {
                  fontSize: 11,
                  textAlign: "right",
                  paddingRight: 8,
                  width: 100,
                  color: Constant.headerColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {Constant.price()}{" "}
              {parseFloat(
                orderData.ServiceCharge
              ).toFixed(2)}
            </Text>
          </View>

          <View
            style={{
              flexDirection: "row",
              paddingBottom: 10,
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 11,
                  flex: 1,
                  color: Constant.textColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {language.discount}
            </Text>
            <Text
              style={[
                {
                  fontSize: 11,
                  textAlign: "right",
                  paddingRight: 8,
                  width: 100,
                  color: Constant.headerColor(),
                },
                BaseStyle.boldFont,
              ]}>
              -{Constant.price()}{" "}
              {parseFloat(
                orderData.OrderDiscount
              ).toFixed(2)}
            </Text>
          </View>

          <View
            style={{
              marginTop: 0,
              backgroundColor: Constant.lightGray(),
              width: "100%",
              height: 1,
            }}
          />

          <View
            style={{
              flexDirection: "row",
              paddingBottom: 16,
              marginTop: 8,
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 14,
                  flex: 1,
                  color: Constant.textColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {language.total}
            </Text>
            <Text
              style={[
                {
                  fontSize: 14,
                  textAlign: "right",
                  paddingRight: 8,
                  width: 100,
                  color: Constant.pinkColor(),
                },
                BaseStyle.boldFont,
              ]}>
              {Constant.price()}{" "}
              {parseFloat(
                orderData.OrderSubtotalInclTax
              ).toFixed(2)}
            </Text>
          </View>
        </View>
      </View>
    )
  }
  showddressUI() {
    const { orderData } = this.state

    return (
      <View style={{ width: "100%", padding: 15 }}>
        <View style={styles.orderView}>
          <View
            style={{
              width: "70%",
              height: "100%",
            }}>
            <View
              style={{
                flexDirection: "row",
                padding: 8,
                width: "100%",
              }}>
              <View style={styles.itemImage}>
                <Image
                  resizeMode={"contain"}
                  style={{ width: 24, height: 24 }}
                  source={images.uReport}
                />
              </View>
              <View
                style={{
                  marginLeft: 8,
                  marginRight: 8,
                  flex: 1,
                }}>
                <Text
                  style={[
                    { fontSize: 13 },
                    BaseStyle.boldFont,
                  ]}>
                  {language.order_no.toUpperCase()}{" "}
                  - {orderData.OrderId}
                </Text>
                <Text
                  numberOfLines={2}
                  style={[
                    styles.addres,
                    BaseStyle.regularFont,
                  ]}>
                  {orderData.Address1}
                </Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                padding: 8,
                paddingTop: 3,
              }}>
              <View style={{ flex: 0.4 }}>
                <Text
                  style={[
                    styles.status,
                    BaseStyle.boldFont,
                  ]}>
                  {language.status.toUpperCase()}
                </Text>
                <Text
                  style={[
                    {
                      fontSize: 10,
                      color: this.getColor(
                        orderData.OrderStatus
                      ),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {orderData.OrderStatus}
                </Text>
              </View>

              <View style={{ flex: 0.6 }}>
                <Text
                  style={[
                    styles.status,
                    BaseStyle.boldFont,
                  ]}>
                  {language.purchasedate.toUpperCase()}
                </Text>
                <Text
                  style={[
                    { fontSize: 10 },
                    BaseStyle.boldFont,
                  ]}>
                    {moment.utc(orderData.CreatedOnUtc).utcOffset(moment().utcOffset()).format("DD/MM/YYYY hh:mm a")}
                </Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                padding: 8,
                paddingTop: 3,
              }}>
              <View style={{ flex: 0.4 }}>
                <Text
                  style={[
                    styles.status,
                    BaseStyle.boldFont,
                  ]}>
                  {language.patientName.toUpperCase()}
                </Text>
                <Text
                  style={[
                    {
                      fontSize: 10,
                      color: this.getColor(
                        orderData.OrderStatus
                      ),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {orderData.PatientName}
                </Text>
              </View>
              {orderData.DoctorName != null &&
              orderData.DoctorName != "" ? (
                <View style={{ flex: 0.6 }}>
                  <Text
                    style={[
                      styles.status,
                      BaseStyle.boldFont,
                    ]}>
                    {language.doctorname.toUpperCase()}
                  </Text>
                  <Text
                    style={[
                      { fontSize: 10 },
                      BaseStyle.boldFont,
                    ]}>
                    {orderData.DoctorName}
                  </Text>
                </View>
              ) : null}
            </View>
          </View>
          <View style={styles.lineView}></View>
          <View style={styles.amountView}>
            <Text
              style={[
                { fontSize: 15 },
                BaseStyle.boldFont,
              ]}>
              {Constant.price()}
              {parseFloat(
                orderData.OrderSubtotalInclTax
              ).toFixed(2)}
            </Text>
            <Text
              numberOfLines={2}
              style={[
                styles.paid,
                BaseStyle.regularFont,
              ]}>
              {orderData.PaymentStatus}
            </Text>

            {orderData.HasPrescription ? (
              <View style={styles.rxView}>
                <Image
                  resizeMode={"contain"}
                  style={{ width: 16, height: 16 }}
                  source={images.prescription}
                />
              </View>
            ) : null}
          </View>
        </View>

        <View
          style={{
            zIndex: 99,
            height: 35,
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}>
          <View
            style={{
              height: 35,
              width: "90%",
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6,
              backgroundColor: "#fff",
              elevation: 3,
            }}>
            <Text
              style={[
                {
                  fontSize: 12,
                  textAlign: "center",
                  marginTop: 6,
                  color: this.getColor(
                    orderData.OrderStatus
                  ),
                },
                BaseStyle.boldFont,
              ]}>
              {this.getText(
                orderData.OrderStatus
              ).toUpperCase()}
            </Text>
          </View>
        </View>
      </View>
    )
  }

  renderItem = ({ item, index }) => {
    const { orderData } = this.state

    return (
      <View
        style={{
          width: "100%",
          height: 40,
          paddingTop: 6,
          flexDirection: "row",
          justifyContent: "space-between",
        }}>
        <View style={{ flex: 0.6 }}>
          <Text
            numberOfLines={1}
            style={[
              {
                fontSize: 12,
                color: Constant.blueColor(),
                marginTop: 0,
              },
              BaseStyle.regularFont,
            ]}>
            {item.Product.Name}
          </Text>

          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 8,
                  color: Constant.textAlpha(),
                },
                BaseStyle.regularFont,
              ]}>
              {item.Product.ManufacturerName}
            </Text>
            <View
              style={{
                height: "80%",
                marginLeft: 8,
                width: 1,
                backgroundColor:
                  Constant.lightGray(),
              }}
            />

            <View
              style={{
                marginLeft: 8,
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "row",
              }}>
              {/* <View style = {{height:5,width:5,backgroundColor:Constant.appColorAlpha(),borderRadius:2.5}}/>
                     <Text style = {[{fontSize:8,marginLeft:6, color:Constant.appColorAlpha()},BaseStyle.regularFont]}>Cream</Text> */}
            </View>
          </View>
        </View>
        <View
          style={{
            flex: 0.1,
            justifyContent: "center",
            alignItems: "center",
          }}>
          <Text
            style={[
              {
                fontSize: 10,
                color: Constant.appColorAlpha(),
              },
              BaseStyle.regularFont,
            ]}>
            X {item.Quantity}
          </Text>
        </View>

        <View
          style={{
            flex: 0.2,
            justifyContent: "center",
            alignItems: "center",
          }}>
          <Text
            style={[
              { fontSize: 10 },
              BaseStyle.boldFont,
            ]}>
            {Constant.price()}
            {parseFloat(
              item.Quantity * item.Product.Price
            ).toFixed(2)}
          </Text>
        </View>
      </View>
    )
  }

  getColor(text) {
    if (text == "Pending") {
      return Constant.yellowColor()
    } else if (text == "Delivered") {
      return Constant.blueColor()
    } else if (text == "Cancelled") {
      return Constant.redColor()
    } else if (text == "Complete") {
      return Constant.appFullColor()
    }

    return Constant.yellowColor()
  }

  getText(text) {
    if (text == "Pending") {
      return "Your order is under review"
    } else if (text == "Delivered") {
      return "Your order will be delivered"
    } else if (text == "Cancelled") {
      return "Your order is under review"
    } else if (text == "Complete") {
      return "your order has been completed"
    }
    return "Your order is under review"
  }
}

const styles = StyleSheet.create({
  mainView: {
    marginTop: 0,

    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
  },
  itemImage: {
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    elevation: 3,
    width: 40,
    backgroundColor: Constant.appLightColor(),
    borderRadius: 6,
  },
  rxView: {
    position: "absolute",
    top: 8,
    right: 0,
    height: 25,
    justifyContent: "center",
    alignItems: "center",
    width: 30,
    borderBottomLeftRadius: 12.5,
    borderTopLeftRadius: 12.5,
    backgroundColor: Constant.appColorAlpha(),
  },
  addres: {
    fontSize: 11,
    marginTop: 2,
    color: Constant.textAlpha(),
    lineHeight: 14,
  },
  paid: {
    fontSize: 11,
    marginTop: 5,
    color: Constant.textAlpha(),
    lineHeight: 14,
  },
  amountView: {
    width: "30%",
    height: null,
    justifyContent: "center",
    alignItems: "center",
  },
  lineView: {
    height: "100%",
    width: 1,
    backgroundColor: "#AEB1BB",
  },
  status: {
    fontSize: 10,
    color: Constant.textAlpha(),
  },
  orderView: {
    zIndex: 100,
    height: null,
    width: "100%",
    borderRadius: 6,
    flexDirection: "row",
    backgroundColor: "#fff",
    elevation: 3,
  },
  container: {
    backgroundColor: Constant.headerColor(),
    elevation: 3,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  headerView: {
    paddingTop: 10,
    paddingBottom: 15,
    backgroundColor: Constant.headerColor(),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  tabStyle: {
    flex: 0.31,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "#fff",
    borderBottomWidth: 2,
  },
  tabText: { fontSize: 14, color: "#fff" },
})

export default OrderDetails
