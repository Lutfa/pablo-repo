import React, { Component } from "react"
import {
  View,
  TextInput,
  Modal,
  ImageBackground,
  AppState,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import SwipeListView from "./../ExtraClass/SwipeList/SwipeListView"
import SplashScreen from "react-native-splash-screen"
import MapView from "react-native-maps"
import { FlatList } from "react-native-gesture-handler"
import language from "./../BaseClass/language"
const { width, height } = Dimensions.get("window")
import Db from "./../DB/Realm"
import DeviceInfo from "react-native-device-info"
import urls from "./../BaseClass/ServiceUrls"
import Toast from "react-native-simple-toast"
import ImageZoom from "react-native-image-pan-zoom"
import ImageViewer from "react-native-image-zoom-viewer"

class CreateOrder extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isSpeaking: false,
      isSelf: true,
      patientName: "",
      doctorName: "",
      instructions: "",
      productArray: ["", ""],
      prescriptionMedicines: [],
      nonPrescriptionMedicines: [],
      userData: "",
      showloader: false,
      address: props.route.params.address,
      PrescriptionUrl:
        this.props.route.params.prescriptionUrl,
      showPrescription: false,
      code: "",
      totalDiscount: 0,
    }
  }

  componentDidMount() {
    SplashScreen.hide()

    const userData = global.userData
    let address = Db.objects("Address")
    let presArray = Db.objects("CartDB").filtered(
      "ColorCode = $0",
      Constant.prescriptionColorCode()
    )
    let nonPresArray = Db.objects(
      "CartDB"
    ).filtered(
      "ColorCode != $0",
      Constant.prescriptionColorCode()
    )

    // alert(cartData.length)

    // var presArray = []
    // var nonPresArray = []
    // cartData.map((product)=>{
    //    if(product.ColorCode != null)
    //    {
    //    if (product.ColorCode == "#b20000" || product.ColorCode.includes("#b20000"))
    //    {
    //        presArray.push(product)

    //    }
    //    else
    //    {
    //        nonPresArray.push(product)
    //    }
    //    }
    //    else
    //    {
    //        nonPresArray.push(product)
    //    }

    // })

    this.setState({
      patientName: userData.Username,
      userData: userData,
      prescriptionMedicines: presArray,
      nonPrescriptionMedicines: nonPresArray,
      address:
        address.length > 0 ? address[0] : null,
    })
  }

  render() {
    const {
      isSelf,
      PrescriptionUrl,
      totalDiscount,
      code,
      showPrescription,
      patientName,
      showloader,
      doctorName,
      instructions,
      productArray,
      userData,
      prescriptionMedicines,
      nonPrescriptionMedicines,
    } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {showloader ? Constant.showLoader() : null}

        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: 4,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor:
                Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              {language.confirmOrder}
            </Text>
          </View>
        </View>

        <ScrollView>
          <View style={{ padding: 15 }}>
            {/* Medicines For */}

            <View
              style={{
                width: "100%",
                backgroundColor: "#fff",
                elevation: 4,
                padding: 6,
                borderRadius: 6,
              }}>
              <Text
                style={[
                  { fontSize: 14 },
                  BaseStyle.boldFont,
                ]}>
                {language.medicinesfor}
              </Text>
              <View
                style={{
                  height: 3,
                  width: 50,
                  borderRadius: 1.5,
                  backgroundColor:
                    Constant.appColorAlpha(),
                }}
              />

              <View
                style={{
                  height: 20,
                  width: "100%",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "center",
                  marginTop: 10,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      isSelf: true,
                      patientName:
                        userData.Username,
                    })
                  }}
                  style={styles.selfmainViewTwo}>
                  <View
                    style={[
                      styles.selfView,
                      {
                        borderWidth: !isSelf
                          ? 0
                          : 2,
                        elevation: !isSelf ? 4 : 0,
                        marginLeft: 0,
                      },
                    ]}>
                    {!isSelf ? null : (
                      <View
                        style={styles.selfInner}
                      />
                    )}
                  </View>

                  <Text
                    style={[
                      styles.selfText,
                      BaseStyle.boldFont,
                    ]}>
                    {language.self}
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      isSelf: false,
                      patientName: "",
                    })
                  }}
                  style={styles.selfmainView}>
                  <View
                    style={[
                      styles.selfView,
                      {
                        borderWidth: isSelf ? 0 : 2,
                        elevation: isSelf ? 4 : 0,
                      },
                    ]}>
                    {isSelf ? null : (
                      <View
                        style={styles.selfInner}
                      />
                    )}
                  </View>

                  <Text
                    style={[
                      styles.selfText,
                      BaseStyle.boldFont,
                    ]}>
                    {language.Others}
                  </Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  width: "100%",
                  height: 20,
                  marginTop: 6,
                  flexDirection: "row",
                  height: 20,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Text
                  style={[
                    { fontSize: 12, width: 100 },
                    BaseStyle.regularFont,
                  ]}>
                  {language.patientName}
                </Text>

                <TextInput
                  onChangeText={(text) => {
                    this.setState({
                      patientName: text,
                    })
                  }}
                  placeholder={"Enter Patient Name"}
                  value={patientName}
                  style={[
                    styles.input,
                    {
                      borderBottomWidth: isSelf
                        ? 0
                        : 0,
                    },
                    BaseStyle.regularFont,
                  ]}
                />
              </View>

              <View
                style={{
                  width: "100%",
                  height: 20,
                  marginTop: 6,
                  flexDirection: "row",
                  height: 20,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Text
                  style={[
                    { fontSize: 12, width: 100 },
                    BaseStyle.regularFont,
                  ]}>
                  {language.doctorname}
                </Text>

                <TextInput
                  onChangeText={(text) => {
                    this.setState({
                      doctorName: text,
                    })
                  }}
                  placeholder={"Enter Doctor Name"}
                  value={doctorName}
                  style={[
                    styles.input,
                    {
                      borderBottomWidth: isSelf
                        ? 0
                        : 0,
                    },
                    BaseStyle.regularFont,
                  ]}
                />
              </View>

              <View
                style={{
                  width: "100%",
                  height: 20,
                  marginTop: 6,
                  flexDirection: "row",
                  height: 20,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Text
                  style={[
                    { fontSize: 12, width: 100 },
                    BaseStyle.regularFont,
                  ]}>
                  {language.instructions}
                </Text>

                <TextInput
                  onChangeText={(text) => {
                    this.setState({
                      instructions: text,
                    })
                  }}
                  placeholder={"Enter Instructions"}
                  value={instructions}
                  style={[
                    styles.input,
                    {
                      borderBottomWidth: isSelf
                        ? 0
                        : 0,
                    },
                    BaseStyle.regularFont,
                  ]}
                />
              </View>
            </View>

            {/* Discount Coupon */}

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate(
                  "AddDiscount",
                  {
                    selectedDiscount:
                      this.discountSelect,
                  }
                )
              }}
              style={{
                height: 100,
                width: "100%",
                elevation: 4,
                backgroundColor: "#fff",
                marginTop: 15,
                borderRadius: 6,
              }}>
              <ImageBackground
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
                source={images.shape}>
                <Image
                  resizeMode={"contain"}
                  style={{
                    height: "70%",
                    width: "50%",
                  }}
                  source={images.discountBox}
                />

                <Text
                  style={[
                    {
                      fontSize: 16,
                      color: "#fff",
                      width: "45%",
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {code != ""
                    ? code
                    : language.discountCode}
                </Text>
              </ImageBackground>
            </TouchableOpacity>

            {/* Address */}

            <View
              style={{
                height: 160,
                width: "100%",
                elevation: 4,
                backgroundColor: "#fff",
                marginTop: 15,
                borderRadius: 6,
              }}>
              {this.addressUi()}
            </View>

            {/* Bill Details */}

            {/* {prescriptionMedicines.lenght > 0 || nonPrescriptionMedicines.lenght > 0 ? */}
            <View
              style={{
                width: "100%",
                elevation: 4,
                backgroundColor: "#fff",
                padding: 6,
                marginTop: 15,
                borderRadius: 6,
              }}>
              <Text
                style={[
                  { fontSize: 14 },
                  BaseStyle.boldFont,
                ]}>
                {language.billDetails}
              </Text>
              <View
                style={{
                  height: 3,
                  width: 40,
                  borderRadius: 1.5,
                  backgroundColor:
                    Constant.appColorAlpha(),
                }}
              />

              {prescriptionMedicines.length > 0 ? (
                <View>
                  <Text
                    style={[
                      {
                        fontSize: 12,
                        color: Constant.textColor(),
                        marginTop: 8,
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {language.prescriptionMedicines}
                  </Text>

                  <FlatList
                    data={prescriptionMedicines}
                    renderItem={this.renderItem}
                  />
                </View>
              ) : null}

              {nonPrescriptionMedicines.length >
              0 ? (
                <View>
                  <Text
                    style={[
                      {
                        fontSize: 12,
                        color: Constant.textColor(),
                        marginTop: 10,
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {
                      language.nonPrescriptionMedicines
                    }
                  </Text>

                  <FlatList
                    data={nonPrescriptionMedicines}
                    renderItem={this.renderItem}
                  />
                </View>
              ) : null}
            </View>

            {/* : null } */}

            {/* TOTAl */}

            <View
              style={{
                width: "100%",
                elevation: 4,
                padding: 8,
                backgroundColor: "#fff",
                marginTop: 15,
                borderRadius: 6,
              }}>
              <View
                style={{
                  flexDirection: "row",
                  paddingBottom: 10,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 11,
                      flex: 1,
                      color: Constant.textColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.subtotal}(
                  {language.prescriptionMedicines})
                </Text>
                <Text
                  style={[
                    {
                      fontSize: 11,
                      textAlign: "right",
                      paddingRight: 8,
                      width: 100,
                      color: Constant.redColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {Constant.price()}{" "}
                  {parseFloat(
                    this.getTotal(
                      prescriptionMedicines
                    )
                  ).toFixed(2)}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  paddingBottom: 10,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 11,
                      flex: 1,
                      color: Constant.textColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.subtotal}(
                  {
                    language.nonPrescriptionMedicines
                  }
                  )
                </Text>
                <Text
                  style={[
                    {
                      fontSize: 11,
                      textAlign: "right",
                      paddingRight: 8,
                      width: 100,
                      color: Constant.redColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {Constant.price()}{" "}
                  {parseFloat(
                    this.getTotal(
                      nonPrescriptionMedicines
                    )
                  ).toFixed(2)}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  paddingBottom: 10,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 11,
                      flex: 1,
                      color: Constant.textColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.combined}{" "}
                  {language.subtotal}
                </Text>
                <Text
                  style={[
                    {
                      fontSize: 11,
                      textAlign: "right",
                      paddingRight: 8,
                      width: 100,
                      color: Constant.blueColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {Constant.price()}{" "}
                  {parseFloat(
                    this.getCombinedTotal()
                  ).toFixed(2)}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  paddingBottom: 10,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 11,
                      flex: 1,
                      color: Constant.textColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.servicefee}
                </Text>
                <Text
                  style={[
                    {
                      fontSize: 11,
                      textAlign: "right",
                      paddingRight: 8,
                      width: 100,
                      color: Constant.headerColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {Constant.price()} 0
                </Text>
              </View>

              {code != "" ? (
                <View
                  style={{
                    flexDirection: "row",
                    paddingBottom: 10,
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 11,
                        flex: 1,
                        color: Constant.textColor(),
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {language.discount}({code})
                  </Text>
                  <Text
                    style={[
                      {
                        fontSize: 11,
                        textAlign: "right",
                        paddingRight: 8,
                        width: 100,
                        color:
                          Constant.headerColor(),
                      },
                      BaseStyle.boldFont,
                    ]}>
                    -{Constant.price()}{" "}
                    {totalDiscount}
                  </Text>
                </View>
              ) : null}

              <View
                style={{
                  marginTop: 0,
                  backgroundColor:
                    Constant.lightGray(),
                  width: "100%",
                  height: 1,
                }}
              />

              <View
                style={{
                  flexDirection: "row",
                  paddingBottom: 16,
                  marginTop: 8,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}>
                <Text
                  style={[
                    {
                      fontSize: 14,
                      flex: 1,
                      color: Constant.textColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {language.total}
                </Text>
                <Text
                  style={[
                    {
                      fontSize: 14,
                      textAlign: "right",
                      paddingRight: 8,
                      width: 100,
                      color: Constant.pinkColor(),
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {Constant.price()}{" "}
                  {parseFloat(
                    this.getFinalTotal()
                  ).toFixed(2)}
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>

        <View
          style={{
            height: 60,
            width: "100%",
            padding: 8,
            paddingLeft: 20,
            paddingRight: 20,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}>
          {PrescriptionUrl != null &&
          PrescriptionUrl != "" ? (
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  showPrescription: true,
                })
              }}
              style={{
                flex: 1,
                borderRadius: 22,
                borderColor:
                  Constant.appColorAlpha(),
                padding: 5,
                borderWidth: 1,
                height: 44,
                alignItems: "center",
                justifyContent: "center",
                marginRight: 10,
              }}>
              <Text
                style={[
                  {
                    fontSize: 14,
                    textAlign: "center",
                    color: Constant.appColorAlpha(),
                  },
                  BaseStyle.boldFont,
                ]}>
                {language.ViewPrescription}
              </Text>
            </TouchableOpacity>
          ) : null}

          <TouchableOpacity
            onPress={() => {
              this.validate()
            }}
            style={{
              flex: 1,
              borderRadius: 22,
              backgroundColor:
                Constant.appColorAlpha(),
              padding: 5,
              height: 44,
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 14,
                  textAlign: "center",
                  color: "#fff",
                },
                BaseStyle.boldFont,
              ]}>
              {language.confirmOrder}
            </Text>
          </TouchableOpacity>
        </View>

        {this.ViewPrescription()}
      </SafeAreaView>
    )
  }

  renderItem = ({ item, index }) => {
    return (
      <View
        style={{
          width: "100%",
          height: 40,
          paddingTop: 6,
          flexDirection: "row",
          justifyContent: "space-between",
        }}>
        <View style={{ flex: 0.6 }}>
          <Text
            numberOfLines={1}
            style={[
              {
                fontSize: 12,
                color: Constant.blueColor(),
                marginTop: 0,
              },
              BaseStyle.regularFont,
            ]}>
            {item.Name}
          </Text>

          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 8,
                  color: Constant.textAlpha(),
                },
                BaseStyle.regularFont,
              ]}>
              {item.ManufacturerName}
            </Text>
            <View
              style={{
                height: "80%",
                marginLeft: 8,
                width: 1,
                backgroundColor:
                  Constant.lightGray(),
              }}
            />

            <View
              style={{
                marginLeft: 8,
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "row",
              }}>
              {/* <View style = {{height:5,width:5,backgroundColor:Constant.appColorAlpha(),borderRadius:2.5}}/>
                     <Text style = {[{fontSize:8,marginLeft:6, color:Constant.appColorAlpha()},BaseStyle.regularFont]}>Cream</Text> */}
            </View>
          </View>
        </View>
        <View
          style={{
            flex: 0.1,
            justifyContent: "center",
            alignItems: "center",
          }}>
          <Text
            style={[
              {
                fontSize: 10,
                color: Constant.appColorAlpha(),
              },
              BaseStyle.regularFont,
            ]}>
            X{item.count}
          </Text>
        </View>

        <View
          style={{
            flex: 0.2,
            justifyContent: "center",
            alignItems: "center",
          }}>
          <Text
            style={[
              { fontSize: 10 },
              BaseStyle.boldFont,
            ]}>
            {Constant.price()}
            {parseFloat(
              item.count * item.Price
            ).toFixed(2)}
          </Text>
        </View>
      </View>
    )
  }

  getTotal(products) {
    let toalPrice = 0
    products.map((item) => {
      let oldPrice =
        item.count * parseFloat(item.Price)

      toalPrice = toalPrice + parseFloat(oldPrice)
    })

    return toalPrice
  }

  discountSelect = (discount, code) => {
    //alert('done')
    this.setState({
      code: `${code} coupon applied`,
      totalDiscount: discount.OrderTotalDiscount,
    })
  }

  getCombinedTotal() {
    return (
      this.getTotal(
        this.state.nonPrescriptionMedicines
      ) +
      this.getTotal(
        this.state.prescriptionMedicines
      )
    )
  }

  getFinalTotal() {
    const { totalDiscount } = this.state
    let total =
      this.getTotal(
        this.state.nonPrescriptionMedicines
      ) +
      this.getTotal(
        this.state.prescriptionMedicines
      )

    return (
      parseFloat(total) - parseFloat(totalDiscount)
    )
  }
  addressUi() {
    const { address } = this.state

    return (
      <View style={{ padding: 8, borderRadius: 5 }}>
        <View style={{ flexDirection: "row" }}>
          <Image
            style={{ width: 22, height: 22 }}
            source={images.home}
          />

          <View
            style={{
              flex: 1,
              paddingLeft: 10,
              paddingRight: 10,
              height: 60,
            }}>
            <Text
              style={[
                { flex: 1, fontSize: 13 },
                BaseStyle.regularFont,
              ]}>
              {address.AddressCategory}
            </Text>

            <Text
              numberOfLines={2}
              style={[
                {
                  flex: 1,
                  marginTop: 0,
                  fontSize: 11,
                  lineHeight: 12,
                  color: Constant.textAlpha(),
                },
                BaseStyle.regularFont,
              ]}>
              {address.Address1}
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate(
                "Address",
                { select: this.addressSelect }
              )
            }}
            style={{
              width: 50,
              height: 24,
              alignItems: "center",
              borderRadius: 12,
              backgroundColor:
                Constant.appColorAlpha(),
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Text
              style={[
                { fontSize: 10, color: "#fff" },
                BaseStyle.boldFont,
              ]}>
              {language.change}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{ height: 80, width: "100%" }}>
          <View
            style={{
              position: "absolute",
              borderRadius: 30,
              top: 20,
              bottom: 0,
              left: 0,
              right: 0,
              backgroundColor:
                Constant.headerColor() + "66",
              alignItems: "center",
            }}></View>

          <View
            style={{
              position: "absolute",
              top: 6,
              bottom: 0,
              left: 0,
              right: 0,
              alignItems: "center",
            }}>
            <Image
              resizeMode={"center"}
              style={{ height: 40, width: 40 }}
              source={images.newPin}
            />
          </View>

          <View
            style={{
              height: 60,
              marginTop: 20,
              zIndex: -1,
              borderRadius: 40,
              overflow: "hidden",
              alignItems: "center",
              justifyContent: "center",
            }}>
            <MapView
              style={{
                flex: 1,
                height: "100%",
                width: "100%",
              }}
              region={{
                latitude: Number(address.Latitude),
                longitude: Number(
                  address.Longitude
                ),
                latitudeDelta: 0.03,
                longitudeDelta: 0.03,
              }}
              //ref={ map => {currentMap = map }}
              //region={props.region}
              rotateEnabled={false}
              loadingEnabled={true}></MapView>
          </View>
        </View>
      </View>
    )
  }

  addressSelect = (newAddress) => {
    this.setState({ address: newAddress })
  }

  validate() {
    const {
      address,
      showloader,
      PrescriptionUrl,
      patientName,
      doctorName,
    } = this.state

    if (patientName == "") {
      Toast.show("Enter Patient name")
      return
    }
    if (
      doctorName == "" &&
      PrescriptionUrl != null &&
      PrescriptionUrl != ""
    ) {
      Toast.show("Enter Doctor name")
      return
    }
    if (
      address != null ||
      (address != "" && !showloader)
    ) {
      this.setState({ showloader: true })

      // if (
      //   PrescriptionUrl != null &&
      //   PrescriptionUrl != ""
      // ) {
      //   this.uploadImage(PrescriptionUrl)
      // } else {
      this.createOrderService()
      //}
    }
  }

  ViewPrescription() {
    const { PrescriptionUrl, showPrescription } =
      this.state

    let data = PrescriptionUrl.split(",")

    if (data.length <= 0) {
      return
    }
    const images = data.map((item) => {
      return { url: item }
    })
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={showPrescription}
        onRequestClose={() => {
          this.setState({ showPrescription: false })
        }}>
        <View
          style={{
            backgroundColor: "rgba(0,0,0,0.5)",
            height: height,
            width: width,
          }}>
          <ImageZoom
            cropWidth={width}
            cropHeight={height}
            imageWidth={400}
            imageHeight={370}>
            <ImageViewer imageUrls={images} />
          </ImageZoom>

          <TouchableOpacity
            style={{
              position: "absolute",
              top: 20,
              right: 20,
              alignItems: "center",
              justifyContent: "center",
              height: 60,
              width: 60,
            }}
            onPress={() => {
              this.setState({
                showPrescription: false,
              })
            }}>
            <Image
              tintColor={"#fff"}
              style={{ height: 15, width: 15 }}
              source={images.close}
            />
          </TouchableOpacity>
        </View>
      </Modal>
    )
  }

  uploadImage(url) {
    Constant.uploadImageToServer(
      urls.mediaUpload,
      url,
      (result) => {
        if (result.success) {
          let PictureUrl = result.url.Response.URL

          this.setState(
            {
              PrescriptionUrl: PictureUrl,
            },
            () => {
              this.createOrderService()
            }
          )
        }
        this.setState({ showLoader: false })
        return
      }
    )
  }

  createOrderService() {
    const {
      userData,
      address,
      patientName,
      doctorName,
      instructions,
      PrescriptionUrl,
    } = this.state

    let params = {
      StoreId: 1,
      CustomerId: global.CustomerId,
      PrescriptionId: 0,
      BillRequired: true,
      DeviceType:
        Platform.OS === "android"
          ? "Android"
          : "IOS",
      Version: DeviceInfo.getVersion(),
      FirstName: userData.Username,
      LastName: userData.Username,
      Email: userData.Email,
      Company: address.Company,
      City: address.City,
      AddressId: address.AddressId,
      Address1: address.Address1,
      Address2: address.Address2,
      ZipPostalCode: address.ZipPostalCode,
      PhoneNumber: address.PhoneNumber,
      Longitude: address.Longitude,
      Latitude: address.Latitude,
      AddressCategory: address.AddressCategory,
      PatientName: patientName,
      PrescriptionUrl: PrescriptionUrl,
      HasPrescription:
        PrescriptionUrl != null ||
        PrescriptionUrl != ""
          ? true
          : false,
      IsTest: false,
      DoctorName: doctorName,
      PackageCharge: 0,
      ServiceCharge: 0,
      DeliveryFee: 0,
    }

    console.log("create order params", params)

    Constant.postMethod(
      urls.createOrder,
      params,
      (result) => {
        // this.setState({isLoading:false})
        console.log(
          "order result " + JSON.stringify(result)
        )
        this.setState({ showloader: false })

        if (result.success) {
          if (
            result.result.Response != null &&
            result.result.Response != ""
          ) {
            // if (
            //   result.result.Response.OrderId != null
            // ) {
            // } else {
            //   Toast.show("Sothing went wrong.")
            // }

            try {
              Db.write(() => {
                let cartData = Db.objects("CartDB")
                Db.delete(cartData)
              })
            } catch (e) {
              console.log("Error on creation")
            }

            this.props.navigation.push(
              "SuccessScreen"
            )
          } else {
            Toast.show("Something went wrong.")
          }
        }
      }
    )
  }
}

const styles = StyleSheet.create({
  selfText: {
    fontSize: 12,
    marginLeft: 6,
    color: Constant.appColorAlpha(),
  },
  selfView: {
    height: 16,
    width: 16,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 2,
    elevation: 4,
    backgroundColor: "#fff",
    borderRadius: 10,
    borderColor: Constant.appColorAlpha(),
  },
  selfInner: {
    height: 6,
    width: 6,
    borderRadius: 3,
    backgroundColor: Constant.appColorAlpha(),
  },
  selfmainView: {
    marginLeft: 15,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  selfmainViewTwo: {
    marginLeft: 0,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    color: Constant.appColorAlpha(),
    padding: 0,
    fontSize: 12,
    flex: 1,
    height: "100%",
    borderBottomColor: Constant.textColor(),
  },
})
export default CreateOrder
