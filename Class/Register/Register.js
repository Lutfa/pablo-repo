import React, { Component } from "react"
import {
  SafeAreaView,
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import Images from "./../BaseClass/Images"
import AnimateLoadingButton from "./../ExtraClass/LoadingButton"
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view"
import CountryPicker from "react-native-country-picker-modal"
import urls from "./../BaseClass/ServiceUrls"
import Toast from "react-native-simple-toast"
import DeviceInfo from "react-native-device-info"
import {
  getUniqueId,
  getManufacturer,
} from "react-native-device-info"
import LinearGradient from "react-native-linear-gradient"
import language from "./../BaseClass/language"
import {
  TextField,
  OutlinedTextField,
} from "react-native-material-textfield"
const { width, height } = Dimensions.get("window")
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"

class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      countryName: "IN",
      countryCode: "+91",
      showEnterPin: false,
      isNumberCorrect: "",
      showConfirmPin: false,
      confirmPin: "",
      enterPin: "",
      isConfirm: false,
      isEmailCorrect: "",
      email: "",
      pinColor: "",
      name: "",
      nameError: "",
      phoneNumber: "",
      enterPinError: "",
      confirmPinError: "",
    }
  }

  render() {
    const {
      name,
      nameError,
      enterPinError,
      confirmPinError,
      showEnterPin,
      isNumberCorrect,
      email,
      enterPin,
      isEmailCorrect,
      pinColor,
      phoneNumber,
      showConfirmPin,
      confirmPin,
      isConfirm,
    } = this.state
    return (
      <SafeAreaView style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0.5 }}
          end={{ x: 0, y: 1 }}
          colors={[
            Constant.colorOne(),
            Constant.colorTwo(),
          ]}
          style={{ flex: 1 }}>
          <KeyboardAwareScrollView>
            <View
              style={{
                padding: 15,
                marginTop: 40,
              }}>
              <TextField
                label={language.name}
                value={name}
                lineWidth={1}
                fontSize={15}
                labelFontSize={13}
                labelTextStyle={[
                  { paddingTop: 5 },
                  BaseStyle.regularFont,
                ]}
                baseColor={Constant.appFullColor()}
                tintColor={Constant.appFullColor()}
                textColor={"#fff"}
                onChangeText={(text) =>
                  this.setState({
                    name: text,
                    nameError: "",
                  })
                }
                error={nameError}
              />

              <TextField
                label={language.email}
                value={email}
                lineWidth={1}
                fontSize={15}
                labelTextStyle={[
                  { paddingTop: 5 },
                  BaseStyle.regularFont,
                ]}
                labelFontSize={13}
                baseColor={Constant.appFullColor()}
                tintColor={Constant.appFullColor()}
                textColor={"#fff"}
                onChangeText={(text) =>
                  this.setState({
                    email: text,
                    isEmailCorrect: "",
                  })
                }
                error={isEmailCorrect}
                onBlur={() => {
                  if (
                    Constant.isValidEmail(email)
                  ) {
                    this.isEmailAvailable()
                  } else {
                    this.setState({
                      isEmailCorrect:
                        "Enter valid Email",
                    })
                  }
                }}
              />

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 0,
                    height: 56,
                    borderBottomWidth: 1,
                    borderBottomColor:
                      Constant.appColorAlpha(),
                  }}>
                  <CountryPicker
                    style={{
                      height: 40,
                      width: 60,
                    }}
                    countryCode={
                      this.state.countryName
                    }
                    translation={"ita"}
                    withCallingCodeButton
                    withAlphaFilter
                    withFilter
                    visible={false}
                    onClose={() => {
                      this.setState({
                        showCountry: false,
                      })
                    }}
                    onSelect={(country) => {
                      this.setState({
                        countryName: country.cca2,
                        countryCode:
                          country.callingCode,
                      })
                    }}
                  />
                </View>
                <View
                  style={{
                    width: width - 102,
                    paddingLeft: 10,
                  }}>
                  <TextField
                    label={language.phonenumber}
                    value={phoneNumber}
                    lineWidth={1}
                    fontSize={15}
                    labelFontSize={13}
                    labelTextStyle={[
                      { paddingTop: 5 },
                      BaseStyle.regularFont,
                    ]}
                    keyboardType="numeric"
                    baseColor={Constant.appFullColor()}
                    tintColor={Constant.appFullColor()}
                    textColor={"#fff"}
                    error={isNumberCorrect}
                    onBlur={() => {
                      if (
                        Constant.isValidPhone(
                          phoneNumber
                        )
                      ) {
                        this.isPhoneAvailable()
                      } else {
                        this.setState({
                          isNumberCorrect:
                            "Enter valid phone number",
                        })
                      }
                    }}
                    onChangeText={(text) => {
                      let number = text.replace(
                        /[^0-9]/g,
                        ""
                      )
                      this.setState({
                        phoneNumber: number,
                        isNumberCorrect: "",
                      })
                    }}
                  />
                </View>
              </View>

              <View>
                <TextField
                  label={language.enterpin}
                  value={enterPin}
                  lineWidth={1}
                  fontSize={15}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  secureTextEntry={!showEnterPin}
                  labelFontSize={13}
                  baseColor={Constant.appFullColor()}
                  tintColor={Constant.appFullColor()}
                  textColor={"#fff"}
                  keyboardType="number-pad"
                  onChangeText={(text) =>
                    this.setState({
                      enterPin: text,
                      isConfirm: "",
                    })
                  }
                  error={enterPinError}
                  onBlur={() => {
                    this.setState({ isConfirm: "" })
                  }}
                />

                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      showEnterPin: !showEnterPin,
                    })
                  }}
                  style={{
                    position: "absolute",
                    top: 30,
                    height: 28,
                    width: 28,
                    right: 15,
                  }}>
                  <Image
                    tintColor={Constant.appFullColor()}
                    style={{
                      height: 28,
                      width: 28,
                    }}
                    source={
                      showEnterPin
                        ? Images.showPassword
                        : Images.hidePassword
                    }
                  />
                </TouchableOpacity>
              </View>

              <View>
                <TextField
                  label={language.confirmPin}
                  value={confirmPin}
                  lineWidth={1}
                  fontSize={15}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  secureTextEntry={!showConfirmPin}
                  labelFontSize={13}
                  baseColor={Constant.appFullColor()}
                  tintColor={Constant.appFullColor()}
                  textColor={"#fff"}
                  keyboardType="number-pad"
                  onChangeText={(text) =>
                    this.setState({
                      confirmPin: text,
                      isConfirm: "",
                    })
                  }
                  error={confirmPinError}
                  onBlur={() => {
                    //this.setState({confirmPin: ''})
                  }}
                />

                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      showConfirmPin:
                        !showConfirmPin,
                    })
                  }}
                  style={{
                    position: "absolute",
                    top: 30,
                    height: 28,
                    width: 28,
                    right: 15,
                  }}>
                  <Image
                    tintColor={Constant.appFullColor()}
                    style={{
                      height: 28,
                      width: 28,
                    }}
                    source={
                      showConfirmPin
                        ? Images.showPassword
                        : Images.hidePassword
                    }
                  />
                </TouchableOpacity>
              </View>
            </View>
            {/* </KeyboardAvoidingScrollView> */}

            <TouchableOpacity
              style={styles.startView}>
              <AnimateLoadingButton
                ref={(c) =>
                  (this.loadingButton = c)
                }
                width={width - 30}
                height={50}
                title={language.signup}
                titleFontSize={16}
                titleWeight={"100"}
                titleColor={Constant.appFullColor()}
                backgroundColor={"#fff"}
                borderRadius={6}
                onPress={() => {
                  this.loginAction()
                }}
              />
            </TouchableOpacity>

            <View
              style={{
                marginTop: 15,
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
              }}>
              <Text
                onPress={() => {
                  this.props.navigation.pop(1)
                }}
                style={[
                  {
                    color: Constant.appFullColor(),
                  },
                  BaseStyle.regularFont,
                ]}>
                {language.allReadyHaveAccount}
                <Text
                  style={[
                    { fontSize: 16, color: "#fff" },
                    BaseStyle.boldFont,
                  ]}>
                  {" "}
                  {language.signin}
                </Text>{" "}
              </Text>
            </View>
          </KeyboardAwareScrollView>
        </LinearGradient>
      </SafeAreaView>
    )
  }

  isEmailAvailable() {
    const { email } = this.state
    let params = { Email: email }
    Constant.postMethod(
      urls.validateEmail,
      params,
      (result) => {
        console.log(
          "email log" + JSON.stringify(result)
        )
        if (result.success) {
          if (result.result.Status == "Success") {
            this.setState({ isEmailCorrect: "" })
          } else {
            Toast.show(result.result.Error)

            this.setState({
              isEmailCorrect:
                "Customer already exist with this email",
            })
          }
        } else {
          this.setState({
            isEmailCorrect:
              "Customer already exist with this email",
          })
        }
      }
    )
  }

  isPhoneAvailable() {
    const { phoneNumber } = this.state
    let params = { PhoneNumber: phoneNumber }
    Constant.postMethod(
      urls.validatePhone,
      params,
      (result) => {
        if (result.success) {
          if (result.result.Status == "Success") {
            this.setState({ isNumberCorrect: "" })
          } else {
            // Toast.show('test')

            Toast.show(result.result.Error)
            this.setState({
              isNumberCorrect:
                "Customer already exist with this phone number",
            })
          }
        } else {
          this.setState({ isNumberCorrect: false })
        }
      }
    )
  }

  validate() {
    const {
      email,
      name,
      phoneNumber,
      enterPin,
      confirmPin,
    } = this.state
    var Status = true
    if (name == "") {
      this.setState({ nameError: "Enter Name" })
      Status = false
    }
    if (!Constant.isValidEmail(email)) {
      this.setState({
        isEmailCorrect: "Enter Valid Email",
      })
      Status = false
    }

    if (phoneNumber == "") {
      this.setState({ isNumberCorrect: false })
      Status = false
    }

    if (enterPin == "") {
      this.setState({ enterPinError: "Enter Pin" })
      Status = false
    }
    if (confirmPin == "") {
      this.setState({
        confirmPinError: "Enter Confim pin",
      })
      Status = false
    }

    if (enterPin != confirmPin) {
      this.setState({
        confirmPinError: "pin is not matching",
      })
      Status = false
    }

    return Status
  }

  loginAction() {
    const { isNumberCorrect, isEmailCorrect } =
      this.state
    if (
      this.validate() &&
      isNumberCorrect == "" &&
      isEmailCorrect == ""
    ) {
      this.loadingButton.showLoading(true)
      this.serviceCall()
    }
    // this.props.navigation.push('Otp',{number:'9959814490'})
  }
  async serviceCall() {
    const {
      email,
      confirmPin,
      countryCode,
      phoneNumber,
      name,
    } = this.state

    const deviceToken = await AsyncStorage.getItem(
      "TOKEN"
    )

    let parameters = {
      Email: email,
      Password: confirmPin,
      CountryCode: countryCode,
      PhoneNumber: phoneNumber,
      UserName: name,
      FirstName: name,
      LastName: name,
      DeviceId: deviceToken,
      DeviceType:
        Platform.OS === "android"
          ? "Android"
          : "IOS",
      DeviceUniqueId: DeviceInfo.getUniqueId(),
      Version: DeviceInfo.getVersion(),
      DeviceModel: DeviceInfo.getModel(),
      DeviceBrand: DeviceInfo.getBrand(),
      DeviceVersion: DeviceInfo.getSystemVersion(),
    }
    this.loadingButton.showLoading(false)

    this.props.navigation.push("Otp", {
      number: phoneNumber,
      countryCode: countryCode,
      parameters: parameters,
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  scrrenStyle: {
    marginTop: 10,
    width: width,
    height: "100%",
  },

  forgot: {
    marginTop: 25,
    color: Constant.appColorAlpha(),
    marginLeft: 15,
    fontSize: 16,
  },
  register: {
    marginTop: 25,
    color: Constant.appColorAlpha(),
    marginLeft: 15,
    fontSize: 16,
  },

  startView: {
    marginTop: 10,
    borderRadius: 25,
    height: 50,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  startBtn: {
    color: "#fff",
    fontSize: 16,
  },
  pageNoStyle: {
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  pageStyle: {
    width: 60,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  dotStyle: {
    marginTop: 5,
    height: 6,
    width: 6,
    backgroundColor: Constant.appColorAlpha(),
    borderRadius: 3,
  },
  title: {
    marginLeft: 20,
    marginRight: 20,
    lineHeight: 30,
    fontSize: 20,
    color: Constant.appColorAlpha(),
  },
  subtitle: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    lineHeight: 20,
  },
  pageCount: {
    fontSize: 16,
  },

  startBtn: {
    color: "#fff",
    fontSize: 16,
  },
})

export default Register
