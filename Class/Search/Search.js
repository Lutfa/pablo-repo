import React, { Component } from "react"
import {
  View,
  Text,
  ActivityIndicator,
  Alert,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import { TextInput } from "react-native-gesture-handler"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import urls from "./../BaseClass/ServiceUrls"
import MapView from "react-native-maps"
const axios = require("axios")
import CartDb from "./../DB/CartDb"
import Db from "./../DB/Realm"
import { withNavigation } from "react-navigation"
const { width, height } = Dimensions.get("window")
import ImageLoad from "./../BaseClass/ImageLoader"

class Search extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchArray: [],
      historyArray: [],
      isLoading: false,
      searchValue: "",
      isFromPill:
        props.route.params != null
          ? props.route.params.isFromPill
          : false,
    }
  }

  componentDidMount() {
    SplashScreen.hide()
    // this._unsubscribe = this.props.navigation.addListener('focus', () => {
    //     this.getAddress()
    //   })

    let searchHistory = Db.objects("SearchHistory")

    this.setState({ historyArray: searchHistory })
  }
  render() {
    const {
      isLoading,
      searchValue,
      historyArray,
      isFromPill,
    } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: 4,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor:
                Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack(null)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              SEARCH MEDICINES
            </Text>

            {isFromPill ? null : (
              <View
                style={{
                  flexDirection: "row",
                  marginRight: 15,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate(
                      "Wishlist"
                    )
                  }}
                  style={{
                    marginLeft: 15,
                    height: 35,
                    borderRadius: 15,
                    alignItems: "center",
                    justifyContent: "center",
                  }}>
                  <Image
                    resizeMode={"center"}
                    tintColor="#fff"
                    style={{
                      height: 20,
                      width: 20,
                      left: 0,
                    }}
                    source={images.pwishlist}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate(
                      "Cart"
                    )
                  }}
                  style={{
                    marginLeft: 15,
                    height: 35,
                    borderRadius: 15,
                    alignItems: "center",
                    justifyContent: "center",
                  }}>
                  <Image
                    resizeMode={"center"}
                    tintColor="#fff"
                    style={{
                      height: 20,
                      width: 20,
                      left: 0,
                    }}
                    source={images.cart}
                  />
                </TouchableOpacity>
              </View>
            )}
          </View>

          <View style={styles.searchMainView}>
            <TouchableOpacity
              onPress={() => {
                // this.props.navigation.push("Search")
              }}
              style={[styles.searchInner]}>
              <Image
                source={images.search}
                style={{
                  marginLeft: 10,
                  width: 15,
                  height: 15,
                }}
              />
              {/* <Text style={[BaseStyle.boldFont, styles.searchIcon]}>Search Location</Text> */}
              <TextInput
                style={[
                  styles.searchIcon,
                  BaseStyle.regularFont,
                ]}
                value={searchValue}
                onChangeText={(text) => {
                  this.setState({
                    searchValue: text,
                  })
                  if (text.length > 1) {
                    this.getSearchData(text)
                  }
                }}
                placeholder="Search medicines"
              />

              {/* <Image
                tintColor={Constant.appColorAlpha()}
                source={images.mic}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  width: 15,
                  height: 15,
                }}
              /> */}
              {isLoading ? (
                <View
                  style={{
                    marginLeft: 10,
                    marginRight: 10,
                    width: 15,
                    height: 15,
                  }}>
                  <ActivityIndicator
                    color={Constant.appColorAlpha()}
                  />
                </View>
              ) : null}
            </TouchableOpacity>
          </View>
        </View>

        {searchValue != "" ? null : (
          <View
            style={{
              height: 45,
              paddingTop: 10,
              paddingLeft: 15,
            }}>
            <Text
              style={[
                { flex: 1, fontSize: 16 },
                BaseStyle.boldFont,
              ]}>
              Recent search history
            </Text>
            <View
              style={{
                marginTop: 4,
                width: 60,
                height: 4,
                backgroundColor:
                  Constant.appColorAlpha(),
              }}
            />
          </View>
        )}

        <FlatList
          style={{ marginTop: 10 }}
          keyExtractor={(item) => item.Id}
          data={
            searchValue == ""
              ? historyArray
              : this.state.searchArray
          }
          extraData={this.state}
          renderItem={({ item, index }) => (
            <View
              style={{
                width: "100%",
                padding: 10,
                backgroundColor: "#fff",
              }}>
              <TouchableOpacity
                onPress={() => {
                  if (isFromPill) {
                    this.props.route.params.selectedItem(
                      item
                    )
                    this.props.navigation.pop(1)
                  } else {
                    if (searchValue != "") {
                      CartDb.AddSearchHistory(item)
                    }
                    this.props.navigation.push(
                      "ProductDetails",
                      { product: item }
                    )
                  }
                }}
                style={{
                  backgroundColor: "#fff",
                  padding: 8,
                  borderRadius: 5,
                  borderBottomColor: "#B5BFC8",
                  borderBottomWidth: 1,
                }}>
                <View
                  style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      height: 35,
                      width: 35,
                      justifyContent: "center",
                      alignItems: "center",
                      elevation: 4,
                      borderRadius: 17.5,
                      backgroundColor: "#fff",
                    }}>
                    {/* <Image resizeMode={'center'} style = {{width:22,height:22}} source = {images.testThree}/> */}

                    <ImageLoad
                      placeholderStyle={{
                        width: 22,
                        height: 22,
                      }}
                      style={{
                        width: 22,
                        height: 22,
                      }}
                      loadingStyle={{
                        size: "large",
                        color:
                          Constant.appColorAlpha(),
                      }}
                      source={{
                        uri:
                          item.Images.length > 0
                            ? item.Images[0].Src
                            : "null",
                      }}
                      placeholderSource={
                        images.placeholder
                      }
                    />
                  </View>

                  <View
                    style={{
                      flex: 1,
                      paddingLeft: 10,
                      paddingRight: 10,
                    }}>
                    <Text
                      style={[
                        { flex: 1, fontSize: 13 },
                        BaseStyle.boldFont,
                      ]}>
                      {item.Name}
                    </Text>

                    <Text
                      numberOfLines={1}
                      style={[
                        {
                          flex: 1,
                          marginTop: 5,
                          fontSize: 11,
                          lineHeight: 14,
                          color:
                            Constant.textAlpha(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {item.ManufacturerName}/
                      {item.GenericCode}
                    </Text>
                  </View>

                  <View
                    style={{
                      width: 60,
                      height: "100%",
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}>
                    <Text
                      style={[
                        { flex: 1, fontSize: 15 },
                        BaseStyle.boldFont,
                      ]}>
                      {/* {Constant.price()} */}
                      {item.PriceValue}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
          ListEmptyComponent={() => {
            return searchValue != "" ? (
              <View
                style={{
                  height: 45,
                  paddingTop: 10,
                  paddingLeft: 15,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Text
                  style={[
                    { flex: 1, fontSize: 16 },
                    BaseStyle.regularFont,
                  ]}>
                  Product not found
                </Text>
              </View>
            ) : null
          }}
        />
      </SafeAreaView>
    )
  }

  getSearchData(searchText) {
    this.setState({ isLoading: true })
    let params = { keyword: searchText }

    Constant.postMethod(
      urls.serachMedicine,
      params,
      (result) => {
        this.setState({ isLoading: false })
        if (result.success) {
          if (result.result.Response != null) {
            this.setState({
              searchArray: result.result.Response,
            })
          }
        }
      }
    )
  }
}

const styles = StyleSheet.create({
  searchMainView: {
    width: "100%",
    paddingTop: 10,
    paddingRight: 5,
    paddingLeft: 5,
    height: 50,
    marginBottom: 20,
    alignItems: "center",
    justifyContent: "center",
  },

  searchInner: {
    marginRight: 5,
    marginLeft: 5,
    elevation: 0,
    height: 45,
    backgroundColor: "#fff",
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  searchIcon: {
    marginLeft: 0,

    color: Constant.textColor(),
    textAlign: "left",
    left: 10,
    fontSize: 14,
    flex: 1,
  },
})

export default Search
