import React, { Component } from "react"
import {
  View,
  Text,
  Alert,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import urls from "./../BaseClass/ServiceUrls"
import {
  FlatList,
  TextInput,
} from "react-native-gesture-handler"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
const { width, height } = Dimensions.get("window")
import language from "./../BaseClass/language"
import Db from "../DB/Realm"
import ImagePicker from "react-native-image-crop-picker"
import Toast from "react-native-simple-toast"

class UploadPrescription extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      searchValue: "",
      showWhy: true,
      showValid: true,
      url: "",
    }
  }

  componentDidMount() {
    SplashScreen.hide()
    // this._unsubscribe = this.props.navigation.addListener('focus', () => {
    //     this.getAddress()
    //   })
  }

  _pickCamImage = async () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      this.uploadImage(image.path)
    })
  }

  _pickImage = async () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      multiple: false,
      compressImageQuality: 0.7,
    }).then((image) => {
      this.uploadImage(image.path)
    })
  }
  addToCart(url = "") {
    // this.setState({isLoading:true})

    let address = Db.objects("Address")
    if (address.length > 0) {
      let dataArray = []

      let dic = {
        Id: 0,
        Quantity: 1,
        ProductId: 1,
        ShoppingCartTypeId: 1,
        //  1- Cart, 2-WishList
      }
      dataArray.push(dic)

      let params = {
        StoreId: 1,
        CustomerId: global.CustomerId,
        OrderItems: dataArray,
      }

      this.setState({ isLoading: true })
      Constant.postMethod(
        urls.addToCart,
        params,
        (result) => {
          // this.setState({isLoading:false})

          this.setState({ isLoading: false })
          console.log(
            "upload pres res " +
              JSON.stringify(result)
          )
          if (result.success) {
            if (
              result.result.Response == true ||
              result.result.Response == "true"
            ) {
              this.props.navigation.navigate(
                "CreateOrder",
                {
                  address:
                    address.length > 0
                      ? address[0]
                      : "",
                  prescriptionUrl: url,
                }
              )
            } else {
              Toast.show("Something went wrong")
            }
          }
        }
      )
    } else {
      this.props.navigation.navigate("AddAddress", {
        onSelect: this.addressSelect,
        data: null,
      })
    }
  }

  addressSelect = () => {
    this.addToCart(this.state.url)
  }

  uploadImage(url) {
    this.setState({ isLoading: true })

    Constant.uploadImageToServer(
      urls.mediaUpload,
      url,
      (result) => {
        this.setState({ isLoading: false })

        if (result.success) {
          let PictureUrl = result.url.Response.Url

          this.setState({ url: PictureUrl }, () => {
            this.addToCart(PictureUrl)
          })
        }
      }
    )
  }

  render() {
    const {
      isLoading,
      searchValue,
      showWhy,
      showValid,
    } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {isLoading ? Constant.showLoader() : null}

        <View
          style={{
            backgroundColor: Constant.headerColor(),
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor:
                Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              activeOpacity={2}
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              {language.uploadPres.toUpperCase()}
            </Text>
          </View>
          <View
            style={{ height: 40, width: "100%" }}
          />
        </View>
        <ScrollView style={{ marginTop: -30 }}>
          <View style={styles.mainView}>
            <View
              style={{
                flexDirection: "row",
                height: 90,
                width: "100%",
                justifyContent: "space-between",
                paddingLeft: 8,
                paddingRight: 8,
              }}>
              <TouchableOpacity
                onPress={this._pickCamImage}
                activeOpacity={2}
                style={{
                  flex: 0.28,
                  alignItems: "center",
                  justifyContent: "center",
                  elevation: 4,
                  backgroundColor: "#fff",
                  borderRadius: 15,
                }}>
                <View style={styles.itemImage}>
                  <Image
                    resizeMode={"contain"}
                    style={{
                      width: 24,
                      height: 24,
                    }}
                    source={images.uCam}
                  />
                </View>
                <Text
                  style={[
                    {
                      color:
                        Constant.appColorAlpha(),
                      marginTop: 5,
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.Camera}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={2}
                style={{
                  flex: 0.28,
                  alignItems: "center",
                  justifyContent: "center",
                  elevation: 4,
                  backgroundColor: "#fff",
                  borderRadius: 15,
                }}
                onPress={this._pickImage}>
                <View style={styles.itemImage}>
                  <Image
                    resizeMode={"contain"}
                    style={{
                      width: 24,
                      height: 24,
                    }}
                    source={images.uGallery}
                  />
                </View>

                <Text
                  style={[
                    {
                      color:
                        Constant.appColorAlpha(),
                      marginTop: 5,
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.Gallery}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={2}
                style={{
                  flex: 0.28,
                  alignItems: "center",
                  justifyContent: "center",
                  elevation: 4,
                  backgroundColor: "#fff",
                  borderRadius: 15,
                }}
                onPress={() => {
                  this.props.navigation.navigate(
                    "HealthRecords",
                    {
                      isGoBack: true,
                      onSelect: (selectedData) => {
                        console.log(
                          "selected prescription",
                          selectedData
                        )
                        var url = ""
                        selectedData.HealthRecordImages.map(
                          (item) => {
                            console.log(
                              "item",
                              item
                            )
                            if (url == "") {
                              url = item.PictureUrl
                            } else {
                              url = `${url},${item.PictureUrl}`
                            }
                            return item
                          }
                        )
                        console.log("Url", url)
                        this.setState(
                          { url: url },
                          () => {
                            this.addToCart(url)
                          }
                        )
                      },
                    }
                  )
                }}>
                <View style={styles.itemImage}>
                  <Image
                    resizeMode={"contain"}
                    style={{
                      width: 24,
                      height: 24,
                    }}
                    source={images.uReport}
                  />
                </View>

                <Text
                  style={[
                    {
                      color:
                        Constant.appColorAlpha(),
                      marginTop: 5,
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.myrecords}
                </Text>
              </TouchableOpacity>

              {/* <View
                style={{
                  flex: 0.22,
                  alignItems: "center",
                  justifyContent: "center",
                  elevation: 4,
                  backgroundColor: "#fff",
                  borderRadius: 15,
                }}>
                <View style={styles.itemImage}>
                  <Image
                    resizeMode={"contain"}
                    style={{
                      width: 24,
                      height: 24,
                    }}
                    source={images.uReport}
                  />
                </View>

                <Text
                  style={[
                    {
                      color: Constant.appColorAlpha(),
                      marginTop: 8,
                      textAlign: "center",
                      lineHeight: 15,
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.talkToDoctor}
                </Text>
              </View> */}
            </View>

            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginTop: 20,
                backgroundColor: "#fff",
              }}>
              <Image
                resizeMode={"contain"}
                style={{ height: 100, width: 100 }}
                source={images.shild}
              />
              <Text
                style={[
                  {
                    color: Constant.appColorAlpha(),
                    textAlign: "center",
                    marginTop: 5,
                  },
                  BaseStyle.regularFont,
                ]}>
                {language.secure}
              </Text>
            </View>

            <View
              style={{ backgroundColor: "#fff" }}>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  this.setState({
                    showWhy: !showWhy,
                  })
                }}
                style={{
                  width: "100%",
                  marginTop: 15,
                }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <Text
                    style={[
                      { fontSize: 16, flex: 1 },
                      BaseStyle.boldFont,
                    ]}>
                    {language.whyPres}
                  </Text>
                  <Image
                    style={{
                      height: 15,
                      width: 15,
                      right: 10,
                    }}
                    source={
                      showWhy
                        ? images.upArrow
                        : images.downArrow
                    }
                  />
                </View>
                {showWhy ? (
                  <Image
                    resizeMode={"contain"}
                    style={{
                      height: 200,
                      width: "100%",
                    }}
                    source={images.whyP}
                  />
                ) : null}
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  this.setState({
                    showValid: !showValid,
                  })
                }}
                style={{
                  width: "100%",
                  marginTop: 15,
                }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <Text
                    style={[
                      { fontSize: 16, flex: 1 },
                      BaseStyle.boldFont,
                    ]}>
                    {language.validPres}
                  </Text>
                  <Image
                    style={{
                      height: 15,
                      width: 15,
                      right: 10,
                    }}
                    source={
                      showValid
                        ? images.upArrow
                        : images.downArrow
                    }
                  />
                </View>
                {showValid ? (
                  <Image
                    resizeMode={"contain"}
                    style={{
                      height: 500,
                      width: "100%",
                    }}
                    source={images.validP}
                  />
                ) : null}
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  mainView: {
    marginTop: 0,

    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
  },
  itemImage: {
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    elevation: 4,
    width: 40,
    backgroundColor: Constant.appLightColor(),
    borderRadius: 6,
  },
})

export default UploadPrescription
