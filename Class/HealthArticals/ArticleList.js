import React,{Component}from "react"
import { View,Text,ScrollView,Image,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from './../BaseClass/BaseStyles'
import  Constant from './../BaseClass/Constant'
import Images from './../BaseClass/Images'
import SplashScreen from 'react-native-splash-screen'
import ListItem from './ListItem'
import { FlatList } from "react-native-gesture-handler";
import images from "./../BaseClass/Images";
import Db from "./../DB/Realm";
import HTMLView from 'react-native-htmlview';
import ImageLoad from './../BaseClass/ImageLoader'
import moment from 'moment';
import urls from "./../BaseClass/ServiceUrls";
const { width,height } = Dimensions.get('window');


class ArticleList extends Component
{
    constructor(props)
    {
        super(props);
        this.state={
          categoryIndex:0,
    articleTodayArray  : [],
    articleWeekArray  : [],
    articleAllArray  : [],
    articleBookmarkArray  : [],
    isLoading:false,
}}

    componentDidMount() {

        SplashScreen.hide();
    
        this.props.navigation.addListener(
          "focus",
          () => {
            this.forceUpdate()
          }
        )
  
      
  let articleData = Db.objects('Articles').filtered('CreatedOnUtc < $0',new Date())

  this.setState({articleArray:articleData})

    }

  render()
  {

    const {categoryIndex,isLoading} = this.state

      return(
          <View  style = {{flex:1}}>
          {isLoading ? Constant.showLoader() : null}
            <View style = {{width:'100%',backgroundColor:'#fff',padding:8}}>
            <FlatList
            data = {this.state.articleArray}
            horizontal = {true}
            renderItem={({item,index})=>(
              <TouchableOpacity 
              onPress = {()=>{
                this.setState({categoryIndex:index})
              }}
              style = {{height: 36,padding:5,marginRight:6 , alignItems:'center',justifyContent:'center',backgroundColor: categoryIndex == index ?  Constant.appColorAlpha() : '#fff' ,borderWidth: categoryIndex != index ? 1 : 0, borderColor:Constant.headerColor() ,borderRadius:18}}>
               

               <Text numberOfLines={1} style = {[{fontSize:14,color: categoryIndex == index ?  '#fff' : Constant.headerColor() ,textAlign:'center' ,width:60},BaseStyle.boldFont]}>diabetes</Text>


               </TouchableOpacity>
            )}
            />
            </View>
            <FlatList
            data = {this.state.articleArray}
            renderItem={this.renderItem}
            />
         </View>
      )
  }

  renderItem=({item,index}) => {
    return(
      <View 
          
          style = {{height:310,width:'100%',padding:15,backgroundColor:'#fff'}}>
              <TouchableOpacity 

              activeOpacity = {1}
              onPress = {()=>{
                  this.props.navigation.navigate('ArticleDetail',{item})
          }}
              style = {{backgroundColor:'#fff',flex:1,borderRadius:8,elevation:4}}>

              <View>
              {/* <Image resizeMode="cover" style = {{width:'100%',height:190,borderTopLeftRadius:8,borderTopRightRadius:8}} source={item.icon}/> */}

                <ImageLoad
                    placeholderStyle={{width:'100%',height:190,borderTopLeftRadius:8,borderTopRightRadius:8}}
                    style ={{width:'100%',height:190,borderTopLeftRadius:8,borderTopRightRadius:8}}
                    loadingStyle={{ size: 'large', color: Constant.appColorAlpha() }}
                    source={{ uri:item.Image}}
                    placeholderSource={images.placeholder}
                />
              
              </View>



              <TouchableOpacity 
               onPress = {()=>{
                   this.bookmarkService(item.Id)
               }}
               style = {{position:'absolute',bottom:70,right:15,height:40,width:40,elevation:3,justifyContent:'center',alignItems:'center'  ,borderRadius:20,backgroundColor:Constant.appColorAlpha()}}>
                   <Image tintColor={'#fff'} style = {{height:20,width:15}} source = {item.HasBookmarkedIt ? images.bookmark : images.unBookmark}/>
                </TouchableOpacity>


         
             <View style ={{padding:6,height:70}}>
              <Text numberOfLines={1} style = {[{fontSize:14,paddingLeft:0},BaseStyle.boldFont]}>{item.Title}</Text>
             <View style = {{height:35,overflow:'hidden'}}>
              <HTMLView
        value={item.Description}
        stylesheet={styles}
      />

     

      </View>
              {/* <Text numberOfLines={2} style = {[{fontSize:12,paddingLeft:0,lineHeight:14,marginTop:5,color:Constant.textColor()},BaseStyle.regularFont]}>{item.Description}</Text> */}
                <View style = {{height:20,width:'100%',justifyContent:'flex-end'}}>
                <Text numberOfLines={2} style = {[{fontSize:12,paddingLeft:0,lineHeight:14,marginTop:5,width:'100%',textAlign:'right'},BaseStyle.regularFont]}>{moment(item.CreatedOnUtc).format('DD-MMM-YYYY')}</Text>
                </View>
              </View>
              </TouchableOpacity>
          </View>
      
    )
  }


  bookmarkService(ArticleId)
 {
  
   this.setState({isLoading:true})
let params = {"CustomerId":global.CustomerId,
              "ArticleId":ArticleId}
    Constant.postMethod(urls.bookmarkArticle,params, (result) => {
        this.setState({isLoading:false})
      if(result.success)
      {
        
          

          try {
                Db.write(() => {
                let record = Db.objectForPrimaryKey('Articles', ArticleId)
                 record.HasBookmarkedIt = !record.HasBookmarkedIt
                        });
                            } catch (e) {
                            console.log("Error on creation");
                   }
                   this.forceUpdte()


      }
    })
 }

}

const styles = StyleSheet.create({
  
   em : {fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color:Constant.textColor()
   },
   header : {fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color:Constant.textColor()
   }

});
export default ArticleList