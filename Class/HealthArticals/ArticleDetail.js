import React, { Component } from "react"
import {
  View,
  ImageBackground,
  AppState,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import Images from "./../BaseClass/Images"
import images from "./../BaseClass/Images"
import ArticalList from "./ArticleList"
import { useNavigation } from "@react-navigation/native"
import Tts from "react-native-tts"
import { BlurView } from "@react-native-community/blur"
import { SafeAreaView } from "react-native-safe-area-context"
import moment from "moment"
const { width, height } = Dimensions.get("window")
import HTMLView from "react-native-htmlview"
import ImageLoad from "./../BaseClass/ImageLoader"
import Db from "./../DB/Realm"
import urls from "./../BaseClass/ServiceUrls"
class ArticleDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      article: props.route.params.item,
      isSpeaking: false,
      isLoading: false,
    }
  }

  componentDidMount() {
    AppState.addEventListener(
      "change",
      this._handleAppStateChange
    )

    Tts.addEventListener("tts-start", (event) => {
      this.setState({ isSpeaking: true })
    })
    Tts.addEventListener("tts-finish", (event) => {
      this.setState({ isSpeaking: false })
    })
    Tts.addEventListener("tts-cancel", (event) => {
      this.setState({ isSpeaking: false })
    })
  }

  componentWillUnmount() {
    AppState.removeEventListener(
      "change",
      this._handleAppStateChange
    )

    Tts.removeEventListener(
      "tts-start",
      (event) => {}
    )
    Tts.removeEventListener(
      "tts-finish",
      (event) => {}
    )
    Tts.removeEventListener(
      "tts-cancel",
      (event) => {}
    )
    Tts.stop()
  }

  _handleAppStateChange = (nextAppState) => {
    if (
      nextAppState === "background" ||
      nextAppState == "inactive"
    ) {
      Tts.stop()
      this.setState({ isSpeaking: false })
    }
  }

  startSpeak(text) {
    if (this.state.isSpeaking) {
      Tts.stop()
    } else {
      Tts.setDefaultRate(0.5)

      Tts.setDefaultLanguage("en-IE")

      Tts.speak(text)
    }
  }

  bookmarkService(ArticleId) {
    this.setState({ isLoading: true })
    let params = {
      CustomerId: global.CustomerId,
      ArticleId: ArticleId,
    }
    Constant.postMethod(
      urls.bookmarkArticle,
      params,
      (result) => {
        this.setState({ isLoading: false })
        if (result.success) {
          try {
            Db.write(() => {
              let record = Db.objectForPrimaryKey(
                "Articles",
                ArticleId
              )
              record.HasBookmarkedIt = !record.HasBookmarkedIt
            })
          } catch (e) {
            console.log("Error on creation")
          }
          this.forceUpdte()
        }
      }
    )
  }

  render() {
    const {
      article,
      isSpeaking,
      isLoading,
    } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {isLoading ? Constant.showLoader() : null}
        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: 4,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor: Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              HEALTH RECORDS
            </Text>
          </View>
        </View>
        <ScrollView>
          <View
            style={{
              padding: 15,
              marginBottom: 20,
            }}>
            <View style={styles.topImage}>
              <ImageLoad
                placeholderStyle={styles.imageView}
                style={styles.imageView}
                loadingStyle={{
                  size: "large",
                  color: Constant.appColorAlpha(),
                }}
                source={{ uri: article.Image }}
                placeholderSource={
                  images.placeholder
                }
              />
              {/* <ImageBackground resizeMode="cover" style = {styles.imageView} source={article.Image}/> */}
            </View>

            <Text
              numberOfLines={1}
              style={[
                { fontSize: 18, marginTop: 8 },
                BaseStyle.boldFont,
              ]}>
              {article.Title}
            </Text>

            <Text
              numberOfLines={1}
              style={[
                {
                  fontSize: 13,
                  marginTop: 4,
                  color: Constant.textAlpha(),
                },
                BaseStyle.regularFont,
              ]}>
              {moment(article.CreatedOnUtc).format(
                "DD-MMM-YYYY"
              )}
            </Text>

            <View
              style={{
                width: "100%",
                paddingTop: 10,
              }}>
              <ScrollView
                style={{ height: 30 }}
                horizontal={true}>
                {article.CategoryIds.length > 0
                  ? article.CategoryIds.map(
                      (category, index) => (
                        <View
                          key={category.id}
                          style={
                            styles.categoriesScroll
                          }>
                          <View
                            style={
                              styles.categoriesImage
                            }>
                            <Image
                              resizeMode={"contain"}
                              style={{
                                height: 12,
                                width: 12,
                              }}
                              source={
                                images.uReport
                              }
                            />
                          </View>
                          <Text
                            style={[
                              {
                                fontSize: 12,
                                marginLeft: 8,
                                color: Constant.appColorAlpha(),
                              },
                              Constant.regularFont,
                            ]}>
                            {this.getCategoriesName(
                              category.id
                            )}
                          </Text>
                        </View>
                      )
                    )
                  : null}
              </ScrollView>
            </View>

            <HTMLView
              value={article.Description}
              stylesheet={styles}
            />
            {/* <Text activeOpacity={2} style = {[styles.description,BaseStyle.regularFont]}>{article.Description}</Text> */}

            {/* <View style = {{marginTop:10}}>
                <Text activeOpacity={2} style = {[{fontSize:13},BaseStyle.boldFont]}>Similar Articles</Text>
                <View style={{height:3,width:50,borderRadius:1.5,backgroundColor:Constant.appColorAlpha()}}/> */}

            {/* <ScrollView style = {{marginTop:8}}  horizontal={true}>
            { article.categories.length > 0 ?
                article.categories.map((category, index) => (
                <View 
                    key = {category.id}
                 
                style = {styles.similarView}>


                <Image resizeMode={'cover'} style ={{height:'64%',width:'100%'}} source ={images.testImage}/>

                <Text activeOpacity={2} style = {[styles.similarText,BaseStyle.regularFont]}>Check Diabetes patients</Text>


                   </View>
                )) : null
            }
                </ScrollView> */}

            {/* </View> */}
          </View>
        </ScrollView>

        <TouchableOpacity
          onPress={() => {
            this.bookmarkService(article.Id)
          }}
          style={{
            height: 40,
            width: 40,
            position: "absolute",
            bottom: 15,
            right: 15,
            backgroundColor: Constant.appColorAlpha(),
            borderRadius: 25,
            elevation: 4,
            justifyContent: "center",
            alignItems: "center",
          }}>
          <Image
            tintColor={"#fff"}
            style={{ height: 22, width: 18 }}
            source={
              article.HasBookmarkedIt
                ? images.bookmark
                : images.unBookmark
            }
          />
        </TouchableOpacity>
      </SafeAreaView>
    )
  }

  getCategoriesName(id) {
    let categoryData = Db.objectForPrimaryKey(
      "Categories",
      parseInt(id)
    )

    return categoryData.Name
  }
}

const styles = StyleSheet.create({
  topImage: {
    overflow: "hidden",
    borderRadius: 8,
    backgroundColor: "#fff",
    elevation: 4,
  },
  imageView: {
    width: "100%",
    height: height / 2 - 50,
  },
  categoriesScroll: {
    padding: 10,
    paddingLeft: 0,
    height: 26,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginRight: 10,
  },

  categoriesImage: {
    height: 26,
    width: 26,
    borderRadius: 6,
    backgroundColor: Constant.appLightColor(),
    justifyContent: "center",
    alignItems: "center",
  },
  description: {
    fontSize: 12,
    lineHeight: 20,
    marginTop: 8,
    textAlign: "justify",
    color: Constant.textAlpha(),
  },
  similarView: {
    height: 120,
    width: 120,
    backgroundColor: "#fff",
    elevation: 3,
    borderRadius: 4,
    overflow: "hidden",
    marginRight: 8,
    marginBottom: 5,
  },
  similarText: {
    fontSize: 12,
    height: "26%",
    margin: 4,
    lineHeight: 15,
    marginTop: 5,
    color: Constant.textColor(),
  },

  em: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  header: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  p: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
})

export default ArticleDetail
