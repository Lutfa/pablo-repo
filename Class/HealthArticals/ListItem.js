import React,{Component}from "react"
import { View,Text,ScrollView,Image,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from './../BaseClass/BaseStyles'
import  Constant from './../BaseClass/Constant'
import Images from './../BaseClass/Images'
import images from "./../BaseClass/Images";
import { useNavigation } from '@react-navigation/native';
import { BlurView } from "@react-native-community/blur";
import moment from 'moment';
const { width,height } = Dimensions.get('window');
import HTMLView from 'react-native-htmlview';
import ImageLoad from './../BaseClass/ImageLoader'

export default ({item }) => {
      
    const { navigate, isFocused } = useNavigation();
     

     
      return(
          <View 
          
          style = {{height:310,width:'100%',padding:15,backgroundColor:'#fff'}}>
              <TouchableOpacity 

              activeOpacity = {1}
              onPress = {()=>{
                  navigate('ArticleDetail',{item})
          }}
              style = {{backgroundColor:'#fff',flex:1,borderRadius:8,elevation:4}}>

              <View>
              {/* <Image resizeMode="cover" style = {{width:'100%',height:190,borderTopLeftRadius:8,borderTopRightRadius:8}} source={item.icon}/> */}

                <ImageLoad
                    placeholderStyle={{width:'100%',height:190,borderTopLeftRadius:8,borderTopRightRadius:8}}
                    style ={{width:'100%',height:190,borderTopLeftRadius:8,borderTopRightRadius:8}}
                    loadingStyle={{ size: 'large', color: Constant.appColorAlpha() }}
                    source={{ uri:item.Image}}
                    placeholderSource={images.placeholder}
                />
              
              </View>



              <TouchableOpacity 
               onPress = {()=>{
                   alert('test')
               }}
               style = {{position:'absolute',bottom:70,right:15,height:40,width:40,elevation:3,justifyContent:'center',alignItems:'center'  ,borderRadius:20,backgroundColor:Constant.appColorAlpha()}}>
                   <Image tintColor={ item.HasBookmarkedIt ? Constant.appFullColor() : '#fff'} style = {{height:20,width:15}} source = {images.bookmark}/>
                </TouchableOpacity>


         
             <View style ={{padding:6,height:70}}>
              <Text numberOfLines={1} style = {[{fontSize:14,paddingLeft:0},BaseStyle.boldFont]}>{item.Title}</Text>
             <View style = {{height:35,overflow:'hidden'}}>
              <HTMLView
        value={item.Description}
        stylesheet={styles}
      />

     

      </View>
              {/* <Text numberOfLines={2} style = {[{fontSize:12,paddingLeft:0,lineHeight:14,marginTop:5,color:Constant.textColor()},BaseStyle.regularFont]}>{item.Description}</Text> */}
                <View style = {{height:20,width:'100%',justifyContent:'flex-end'}}>
                <Text numberOfLines={2} style = {[{fontSize:12,paddingLeft:0,lineHeight:14,marginTop:5,width:'100%',textAlign:'right'},BaseStyle.regularFont]}>{moment(item.CreatedOnUtc).format('DD-MMM-YYYY')}</Text>
                </View>
              </View>
              </TouchableOpacity>
          </View>
      )



 

    
  
}


const styles = StyleSheet.create({
  
   em : {fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color:Constant.textColor()
   },
   header : {fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color:Constant.textColor()
   }

});