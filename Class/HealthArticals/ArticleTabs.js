import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import React, { Component } from "react"
import IndicatorViewPager from "./../ExtraClass/viewpager/IndicatorViewPager"
import PagerTitleIndicator from "./../ExtraClass/viewpager/indicator/PagerTitleIndicator"
import Article from "./ArticleList"
import Bookmarks from "./BookmarkList"
import Constant from "./../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import { FlatList } from "react-native-gesture-handler"
import BaseStyle from "./../BaseClass/BaseStyles"

import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import ViewPager from "@react-native-community/viewpager"
import language from "./../BaseClass/language"
import Db from "./../DB/Realm"
import HTMLView from "react-native-htmlview"
import ImageLoad from "./../BaseClass/ImageLoader"
import moment from "moment"
import urls from "./../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")

export default class ArticleTabs extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pageIndex: 0,
      categoryIndex: -1,
      isLoading: false,
      articleTodayArray: [],
      articleWeekArray: [],
      articleAllArray: [],
      articleBookmarkArray: [],
      categoryArray: [],
      isFilter: false,
      filterArray: [],
    }
    this.viewPager = React.createRef()
  }

  componentDidMount() {
    // var date = new Date();

    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )
    let article = Db.objects("Articles").filtered(
      "CategoryIds.@size > 0"
    )

    let newArray = []
    for (let category of article) {
      for (let Id of category.CategoryIds) {
        let categoryData = Db.objectForPrimaryKey(
          "Categories",
          parseInt(Id.id)
        )

        if (
          newArray.filter(
            (data) => data.Id == Id.id
          ).length <= 0
        ) {
          newArray.push(categoryData)
        }
      }
    }

    this.setState({ categoryArray: newArray })

    let articleData = Db.objects(
      "Articles"
    ).filtered(
      "CreatedOnUtc > $0",
      new Date(
        new Date().setDate(new Date().getDate() - 1)
      )
    )
    let articleWeekData = Db.objects(
      "Articles"
    ).filtered(
      "CreatedOnUtc > $0",
      new Date(
        new Date().setDate(new Date().getDate() - 7)
      )
    )
    let articleAllData = Db.objects("Articles")

    let articleBookmarkData = Db.objects(
      "Articles"
    ).filtered("HasBookmarkedIt == true")

    this.setState({
      articleTodayArray: articleData,
      articleWeekArray: articleWeekData,
      articleAllArray: articleAllData,
      articleBookmarkArray: articleBookmarkData,
    })
  }

  render() {
    const {
      categoryIndex,
      isFilter,
      filterArray,
      isLoading,
      pageIndex,
      categoryArray,
      articleTodayArray,
      articleWeekArray,
      articleAllArray,
      articleBookmarkArray,
    } = this.state
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        <View
          style={{
            backgroundColor: Constant.headerColor(),
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 5,
              backgroundColor: Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyles.headerFont,
              ]}>
              {language.healthArticles}
            </Text>
          </View>

          <View
            style={{
              height: 40,
              width: "100%",
              marginBottom: 1,
              flexDirection: "row",
              justifyContent: "space-between",
            }}>
            <TouchableOpacity
              onPress={() => {
                this.viewPager.current.setPage(0)
              }}
              style={[
                styles.tabStyle,
                {
                  borderBottomWidth:
                    pageIndex == 0 ? 2 : 0,
                },
              ]}>
              <Text
                style={[
                  { fontSize: 15, color: "#fff" },
                  BaseStyles.regularFont,
                ]}>
                {language.today}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.viewPager.current.setPage(1)
              }}
              style={[
                styles.tabStyle,
                {
                  borderBottomWidth:
                    pageIndex == 1 ? 2 : 0,
                },
              ]}>
              <Text
                style={[
                  { fontSize: 15, color: "#fff" },
                  BaseStyles.regularFont,
                ]}>
                Week
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.viewPager.current.setPage(2)
              }}
              style={[
                styles.tabStyle,
                {
                  borderBottomWidth:
                    pageIndex == 2 ? 2 : 0,
                },
              ]}>
              <Text
                style={[
                  { fontSize: 15, color: "#fff" },
                  BaseStyles.regularFont,
                ]}>
                All
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.viewPager.current.setPage(3)
              }}
              style={[
                styles.tabStyle,
                {
                  borderBottomWidth:
                    pageIndex == 3 ? 2 : 0,
                },
              ]}>
              <Text
                style={[
                  { fontSize: 15, color: "#fff" },
                  BaseStyles.regularFont,
                ]}>
                Bookmarks
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            width: "100%",
            backgroundColor: "#fff",
            padding: 8,
          }}>
          {pageIndex == 3 ? null : (
            <FlatList
              data={categoryArray}
              horizontal={true}
              renderItem={({ item, index }) => (
                <TouchableOpacity
                  onPress={() => {
                    var filter = []
                    if (categoryIndex == index) {
                      this.setState({
                        categoryIndex: -1,
                        isFilter: false,
                        filterArray: filter,
                      })
                    } else {
                      if (pageIndex == 0) {
                        filter = this.getArray(
                          articleTodayArray,
                          item.Id
                        )
                      } else if (pageIndex == 1) {
                        filter = this.getArray(
                          articleWeekArray,
                          item.Id
                        )
                      } else if (pageIndex == 2) {
                        filter = this.getArray(
                          articleAllArray,
                          item.Id
                        )
                      }
                      this.setState({
                        categoryIndex: index,
                        isFilter: true,
                        filterArray: filter,
                      })
                    }
                  }}
                  style={{
                    height: 36,
                    padding: 5,
                    marginRight: 6,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor:
                      categoryIndex == index
                        ? Constant.appColorAlpha()
                        : "#fff",
                    borderWidth:
                      categoryIndex != index
                        ? 1
                        : 0,
                    borderColor: Constant.headerColor(),
                    borderRadius: 18,
                  }}>
                  <Text
                    numberOfLines={1}
                    style={[
                      {
                        fontSize: 14,
                        color:
                          categoryIndex == index
                            ? "#fff"
                            : Constant.headerColor(),
                        textAlign: "center",
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {item.Name}
                  </Text>
                </TouchableOpacity>
              )}
            />
          )}
        </View>

        <ViewPager
          style={{ flex: 1 }}
          ref={this.viewPager}
          scrollEnabled={true}
          initialPage={0}
          onPageSelected={this.onPageSelected}>
          <View key="1">
            {/* <Article {...this.props}/> */}
            {this.renderList(
              isFilter
                ? filterArray
                : articleTodayArray
            )}
          </View>
          <View key="2">
            {this.renderList(
              isFilter
                ? filterArray
                : articleWeekArray
            )}
          </View>

          <View key="3">
            {this.renderList(
              isFilter
                ? filterArray
                : articleAllArray
            )}
          </View>
          <View key="4">
            {this.renderList(articleBookmarkArray)}
          </View>
        </ViewPager>
      </SafeAreaView>
    )
  }

  onPageSelected = (e) => {
    this.setState({
      pageIndex: e.nativeEvent.position,
      isFilter: false,
      categoryIndex: -1,
    })
  }

  getArray(array, categoryId) {
    return array.filter((data) => {
      return (
        data.CategoryIds.filter(
          (newItem) =>
            parseInt(newItem.id) ==
            parseInt(categoryId)
        ).length > 0
      )
      //data.CategoryIds.includes(item.Id)
    })
  }

  renderList(renderArray) {
    const {
      categoryIndex,
      isLoading,
      pageIndex,
      articleTodayArray,
      articleWeekArray,
      articleAllArray,
      articleBookmarkArray,
    } = this.state

    return (
      <View style={{ flex: 1 }}>
        {isLoading ? Constant.showLoader() : null}
        {!!renderArray && renderArray.length > 0 ? (
          <FlatList
            data={renderArray}
            renderItem={this.renderItem}
          />
        ) : (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Text
              style={[
                {
                  fontSize: 14,
                },
                BaseStyle.regularFont,
              ]}>
              No data found
            </Text>
          </View>
        )}
      </View>
    )
  }

  renderItem = ({ item, index }) => {
    return (
      <View
        style={{
          height: 310,
          width: "100%",
          padding: 15,
          paddingTop: 5,
          backgroundColor: "#fff",
        }}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            this.props.navigation.navigate(
              "ArticleDetail",
              { item }
            )
          }}
          style={{
            backgroundColor: "#fff",
            flex: 1,
            borderRadius: 8,
            elevation: 4,
          }}>
          <View>
            {/* <Image resizeMode="cover" style = {{width:'100%',height:190,borderTopLeftRadius:8,borderTopRightRadius:8}} source={item.icon}/> */}

            <ImageLoad
              placeholderStyle={{
                width: "100%",
                height: 190,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
              }}
              style={{
                width: "100%",
                height: 190,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
              }}
              loadingStyle={{
                size: "large",
                color: Constant.appColorAlpha(),
              }}
              source={{ uri: item.Image }}
              placeholderSource={images.placeholder}
            />
          </View>

          <TouchableOpacity
            onPress={() => {
              this.bookmarkService(item.Id)
            }}
            style={{
              position: "absolute",
              bottom: 70,
              right: 15,
              height: 40,
              width: 40,
              elevation: 3,
              justifyContent: "center",
              alignItems: "center",
              borderRadius: 20,
              backgroundColor: Constant.appColorAlpha(),
              zIndex: 100,
            }}>
            <Image
              tintColor={"#fff"}
              style={{ height: 20, width: 15 }}
              source={
                item.HasBookmarkedIt
                  ? images.bookmark
                  : images.unBookmark
              }
            />
          </TouchableOpacity>

          <View style={{ padding: 6, height: 70 }}>
            <Text
              numberOfLines={1}
              style={[
                { fontSize: 14, paddingLeft: 0 },
                BaseStyle.boldFont,
              ]}>
              {item.Title}
            </Text>
            <View
              style={{
                height: 35,
                overflow: "hidden",
              }}>
              <HTMLView
                value={item.Description}
                stylesheet={styles}
              />
            </View>
            {/* <Text numberOfLines={2} style = {[{fontSize:12,paddingLeft:0,lineHeight:14,marginTop:5,color:Constant.textColor()},BaseStyle.regularFont]}>{item.Description}</Text> */}
            <View
              style={{
                height: 20,
                width: "100%",
                justifyContent: "flex-end",
              }}>
              <Text
                numberOfLines={2}
                style={[
                  {
                    fontSize: 12,
                    paddingLeft: 0,
                    lineHeight: 14,
                    marginTop: 5,
                    width: "100%",
                    textAlign: "right",
                  },
                  BaseStyle.regularFont,
                ]}>
                {moment(item.CreatedOnUtc).format(
                  "DD-MMM-YYYY"
                )}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  bookmarkService(ArticleId) {
    this.setState({ isLoading: true })
    let params = {
      CustomerId: global.CustomerId,
      ArticleId: ArticleId,
    }
    Constant.postMethod(
      urls.bookmarkArticle,
      params,
      (result) => {
        this.setState({ isLoading: false })
        if (result.success) {
          try {
            Db.write(() => {
              let record = Db.objectForPrimaryKey(
                "Articles",
                ArticleId
              )
              record.HasBookmarkedIt = !record.HasBookmarkedIt
            })
          } catch (e) {
            console.log("Error on creation")
          }
          this.forceUpdte()
        }
      }
    )
  }
}

const styles = StyleSheet.create({
  viewPager: {
    flex: 1,
  },
  tabStyle: {
    flex: 0.23,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "#fff",
    borderBottomWidth: 2,
  },
  em: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  header: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
})
