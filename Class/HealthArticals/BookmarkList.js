import React,{Component}from "react"
import { View,Text,ScrollView,Image,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from './../BaseClass/BaseStyles'
import  Constant from './../BaseClass/Constant'
import Images from './../BaseClass/Images'
import SplashScreen from 'react-native-splash-screen'
import ListItem from './ListItem'
import { FlatList } from "react-native-gesture-handler";
import images from "./../BaseClass/Images";


const { width,height } = Dimensions.get('window');


class BookmarkList extends Component
{
    constructor(props)
    {
        super(props);
        this.state={
    
      bookmarkArray  : [
        {
          "key": "11",
          "title": "health psychology articles 2019",
          "description": "Description is the pattern of narrative development that aims to make vivid a place, object, character, or group. Description is one of four rhetorical modes, along with exposition, argumentation, and narration. In practice it would be difficult to write literature that drew on just one of the four basic modes.",
          "icon": images.testImage,
          "cost": 0,
          "categories":[{
            "id":1,
             "name":'diabetes'
          },
          {
            "id":2,
             "name":'Cardiovascular'
          }
        
          ]
        },
        {
          "key": "12",
          "title": "health psychology articles 2019",
          "description": "Description is the pattern of narrative development that aims to make vivid a place, object, character, or group. Description is one of four rhetorical modes, along with exposition, argumentation, and narration. In practice it would be difficult to write literature that drew on just one of the four basic modes.",
          "icon": images.testImage,
          "cost": 0,
          "categories":[{
            "id":1,
             "name":'diabetes'
          },
          {
            "id":2,
             "name":'Cardiovascular'
          }
        
          ]
        }
        
        
          ]
}}

    componentDidMount() {
        SplashScreen.hide();
    }
  render()
  {

    const {isAll}  = this.state

      return(
          <View  style = {{flex:1}}>
            <View style = {{width:'100%',elevation:5 ,backgroundColor:'#fff'}}>
            <FlatList
            data = {this.state.bookmarkArray}
            horizontal = {true}
            renderItem={({item,index})=>(
              <View style = {{padding:5 , alignItems:'center',justifyContent:'center'}}>
               <View style = {{height:40,width:40,borderRadius:20,borderColor: index%2 == 0 ? Constant.secondColor() : Constant.appColorAlpha(),borderWidth:1,padding:2}}>
                  <Image style = {{height:34,width:34,borderRadius:20}} source = {images.testImage}/>
               </View>

               <Text numberOfLines={2} style = {[{fontSize:12,textAlign:'center' ,width:60},BaseStyle.mediumFont]}>diabetes</Text>


               </View>
            )}
            />
            </View>
            <FlatList
            data = {this.state.bookmarkArray}
            renderItem={({item})=>(
                <ListItem key={item.key} {...{item}} {...this.props} />
            )}
            />
         </View>
      )
  }
}

export default BookmarkList