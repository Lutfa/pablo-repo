import React from 'react'
import { GiftedChat, Message, Send, InputToolbar, Composer, Bubble,Day } from 'react-native-gifted-chat'
import { View, BackHandler, StyleSheet, Platform, Image, Modal, TouchableOpacity, Dimensions, Keyboard, ActivityIndicator, Text, AsyncStorage } from 'react-native'
// import { WebView } from 'react-native-webview';
import moment from 'moment';
// import {HubConnectionBuilder, LogLevel} from "@aspnet/signalr";
import BaseStyles from './../BaseClass/BaseStyles'
import Constant from './../BaseClass/Constant'
import Menu from './CirecleMenu'
import ImagePicker from 'react-native-image-crop-picker';
import DocumentPicker from 'react-native-document-picker';
import ImageViewer from 'react-native-image-zoom-viewer';
import images from './../BaseClass/Images'
import { SafeAreaView } from 'react-native-safe-area-context';
import language from './../BaseClass/language'
import SplashScreen from 'react-native-splash-screen'
import urls from "./../BaseClass/ServiceUrls"
import Db from "../DB/Realm"
import { ImageBackground } from 'react-native';
import ImageLoad from '../BaseClass/ImageLoader';
export default class Chat extends React.Component {


  //   static navigationOptions = ({ navigation }) =>{


  //        return{
  //         title: 'Chat',
  //         headerTitleStyle: BaseStyle.headerStyle,
  //         headerLeft: <HeaderBackButton tintColor = '#000' onPress={() => {
  //         navigation.goBack()}
  //            } /> 
  //        }
  //     };
  // {
  //   _id: 1,
  //   text: 'Hello developer',
  //   createdAt: new Date(),
  //   user: {
  //     _id: 2,
  //     name: 'React Native',
  //     avatar: 'https://placeimg.com/140/140/any',
  //   },
  constructor(props) {
    super(props);
    this.state = {
      hubConnection: null,
      messages: [


      ],
      chatsArray:[],
      showMenu: false,
      isShow: false,
      showImages: [],
      selectedUrl: '',
      showDoc: false,
      userData: {},
      chatId: 0,
      connectionId: 0,
      pharmacyId: Constant.pharmacyId(),
      showProductUi: false,
    };

    //this.getChatId()
  }




  getChatTimeStamp(name) {

    const _this = this
    Constant.timeDb().find({ name: name }, function (err, docs) {
      if (docs.length > 0) {
        _this.setState({ chatId: docs[0].time })
      }
    })
  }


  // getChatId()
  //  {

  //    const _this = this
  //      Constant.chatId().find({}, function (err, docs) {
  //        if(docs.length > 0)
  //        {
  //         _this.setState({chatId:docs[0].chatId})
  //        }

  //      })
  //  }



 

  SaveInDb(message) {
    //  Constant.chatDb().insert((message),function(err,newDocs){
    //     console.log(newDocs)
    //   });
  }
  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView
      style={{ paddingHorizontal: 15}}
      >
        {/* <StatusBar
          translucent
          barStyle="dark-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor={Constant.headerColor()}

        /> */}

        <TouchableOpacity
          activeOpacity={2}
          style={styles.headerInnerView}
          onPress={() => {
            this.props.navigation.pop(1)
          }}
        >
         <View style={[BaseStyles.circle]}>
          <Image
            style={{ width: 10, height: 14,marginLeft:-2,position:'absolute'}}
            source={images.backicon}
          />
 
          </View>
          {/* <ImageLoad  loadingStyle={{
                size: "small",
                color: "blue",
               }}style={{ borderRadius: 8, height:30,width:30, resizeMode:"center" }}
            //  source={{
            //   //uri: item.pictureUrl,
            // }}
            placeholderSource={
              images.manicon
            }></ImageLoad> */}
            <TouchableOpacity onPress={()=>{
              this.props.navigation.navigate("VideoCall")
            }}>
<Image  style={{ borderRadius: 8, height:30,width:30, resizeMode:"contain",}}
            source={images.manicon}></Image>
</TouchableOpacity>
          <Text style={[BaseStyles.headerFontblack]}>
            Chat
          </Text>


        </TouchableOpacity>
      </SafeAreaView>
    )
  }
  getChatFromDb() {
    const _this = this

    //    Constant.chatDb().find({}).sort({ time: -1 }).exec(function (err, docs) {

    //       _this.setState({messages:docs})        
    //   });
  }

  getUnsentData() {
    const _this = this

    //    Constant.chatDb().find({isUpload:true}).sort({ time: -1 }).exec(function (err, docs) {




    //       docs.map((message)=>{
    //         var mimeType = ''
    //         if(message.type == 'PDF')
    //         {
    //           mimeType = 'application/pdf'
    //         }
    //         else if(message.type == 'DOCS')
    //         {
    //            mimeType = 'application/msword'
    //         }
    //         else if(message.type == 'Excel')
    //         {
    //             mimeType = 'application/vnd.ms-excel'
    //         }

    //         _this.uploadImageToServer(message.uri,message.time,mimeType,`sample.${message.type}`,message.type)
    //         })     
    //   });
  }


  async openDocument() {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });

      let url = res.uri
      let type = res.type
      let name = res.name


      //The url you received from the DocumentPicker

      // I STRONGLY RECOMMEND ADDING A SMALL SETTIMEOUT before uploading the url you just got.
      // const split = url.split('/');
      // const name = split.pop();
      // const inbox = split.pop();
      // const realPath = `${RNFS.TemporaryDirectoryPath}${inbox}/${name}`;

      if (res.type.toLowerCase().includes('pdf')) {
        this.addMsgToArray(url, 'PDF', type, name)
      }
      else if (res.type.toLowerCase().includes('msword')) {
        this.addMsgToArray(url, 'DOCS', type, name)
      }
      else if (res.type.toLowerCase().includes('excel')) {
        this.addMsgToArray(url, 'Excel', type, name)
      }
      else {
        alert('Unsupported formate')
      }
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size
      );
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }

  }
  openGallery() {

    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      console.log(image.path)


      let type = 'image/jpeg'
      let name = 'photo.jpg'

      this.addMsgToArray(image.path, 'Image', type, name)
    });
  }


  openCamera() {

    this.setState({ popUp: false })
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image.path)
      let type = 'image/jpeg'
      let name = 'photo.jpg'

      this.addMsgToArray(image.path, 'Image', type, name)
    });
  }


  addMsgToArray(uri, type, mimeType, name) {
    const msg = {
      text: '',
      time: Date.now(),
      createdAt: new Date(),
      user: {
        _id: global.CustomerId,
      },
      image: uri,
      uri: uri,
      isUpload: true,
      type: type
    };

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, msg),
    }))
    this.SaveInDb(msg)
    this.uploadImageToServer(uri, msg.time, mimeType, name, type)

  }


  updateLocalDb(url, time) {

    //     Constant.chatDb().update({time : time}, { $set: { isUpload: false,uri:'',image:url} }, {}, function (err, newDocs) {
    //   console.log(newDocs)
    // });

    //  const _this = this
    //   Constant.chatDb().find({time : time}).exec(function (err, docs) {

    //     if (docs.length > 0)
    //     {
    //       let oldMessage = docs[0]
    //       oldMessage.isUpload = false
    //       oldMessage.uri = ''
    //       oldMessage.image = url

    //       _this.updateDb(oldMessage,time)


    //     }

    //   });
  }


  updateDb(dic, time) {
    const _this = this
    //   Constant.chatDb().update({time : time}, dic, {}, function (err, numReplaced) 
    //   {

    //     _this.sendTextMsg(dic.image,dic.type,null)
    //     _this.getChatFromDb()



    //   });
  }


  uploadImageToServer(uri, time, mimeType, name, type) {



    Constant.uploadChatImageToServer(uri, mimeType, name, (result) => {
      if (result.success === true) {
        //Constant.insertTimeStamp('chatTimeTicks',Date.now())

        this.updateLocalDb(result.url, time)


      }
      else {
        this.setState({ showLoader: false })
      }


    });
  }


  sendTextMsg(message, type, child) {
    let chatDic = {
      "id": 0,
      "senderId": 0,
      "userType": "Patient",
      "messageBody": message,
      "messageType": type,
      "parentChatId": 0,
      "parentChatFeild": child,
      "createdOnUtc": moment().utc().format("YYYY-MM-DDTHH:mm:ssZ"),
      "pictureId": 0,
      "lastUpdatedTimeTicks": 0,
      "deviceId": global.deviceId,
      "patientId": global.CustomerId,
      "PharmacistId": 1,
      "pharmacyId": 1
    }
    console.log("chat Req", JSON.stringify(chatDic))

    Constant.postMethod(
      urls.sendAskPharmachat
      ,
      chatDic,
      (result) => {
        //this.setState({ isLoading: false })
        console.log("chat Result", JSON.stringify(result))

        if (result.success) {
          if (result.result.response != null) {
            if (
              result.result.response.chatDetails
                .length > 0
            ) {

              CartDb.addChatData(
                result.result.response
                  .chatDetails
              )

            } else {
              Toast.show("Somthing went wrong")
            }
          }
        }
      }
    )


    // this.state.hubConnection.invoke('sendPrivateMessage',chatDic)


    //  Constant.insertTimeStamp('chatTimeTicks',Date.now())




  }

  addMessageToList(data) {
    console.log("addingmessage")
    var message = {
      _id:data.chatId,
      time: data.createdOnUtc
      ,

      createdAt: data.createdOnUtc,
      isUpload: false,
      user:{
        
      }
     
    }
    message.type = data.messageType
    if (data.messageType == "Image" || data.messageType == "PDF" || data.messageType == "DOCS" || data.messageType == "Excel") {
      message.image = data.messageBody
      message.text = ''

    }
    else if (data.messageType == "Text") {
      message.text = data.messageBody
      message.type = 'Text'
    }
    message.userType = data.userType,



      this.SaveInDb(message)
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, message),
    }))
  }






  deleteDb() {

    // Constant.chatDb().remove({}, { multi: true }, function (err, numRemoved) {
    // });
  }

  async componentDidMount() {
    SplashScreen.hide();

    //this.deleteDb()



    //   const _this = this

    //  Constant.getUserData((result) => 
    //    {
    //         _this.setState({userData : result})
    //         _this.connectToSignalR(result.PharmacyId)
    //         _this.getChatTimeStamp('chatId')


    //    })

    // this.getUnsentData()
    // this.getChatFromDb()
    // this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
    //   return false;
    // });
    this.getDataFromServer(0)

  }

  getDataFromServer(time) {
    this.setState({
      isLoading: time == "0" ? true : false,
    })
    let params = {
      pharmacyId: Constant.pharmacyId(),
      patientId: global.CustomerId,
      pharmacistId: 1,
      lastUpdatedTimeTicks: time,
    }
    Constant.postMethod(
      urls.getAskPharmaChat,
      params,
      (result) => {
        console.log("getChat ", JSON.stringify(result))
        this.setState({ isLoading: false })

        if (result.success) {
          if (result.result.response != null) {
             this.setState({chatsArray:result.result.response
              .chatDetails.chatMessages})
              console.log("getChatarray ", JSON.stringify(this.state. chatsArray))

             this.state. chatsArray.map((item, index) => {
                this.addMessageToList(item)
              })

            // CartDb.addChatData( result.result.response
            //   .chatDetails)
          } else {
            //    Toast.show(result.result.Response.Message)
          }
        } else {
          //   this.setState({isNumberCorrect:false})
        }
        AsyncStorage.setItem(
          "ChatTimeTicks",
          JSON.stringify(
            result.result
              .lastUpdatedTimeTicks
          )
        )
        // let recordsData = Db.objects(
        //   "Chat"
        // )
       // this.setState({ messages: recordsData })
      }
    )
  }
  onSend(messages = []) {

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))



    //    var jsonStr = ''

    this.sendTextMsg(messages[0].text, 'Text', null)



    // var newMsg = messages[0]
    // newMsg.time = Date.now()


    // newMsg.type = 'Text'

    // newMsg.isUpload = false,
    // this.SaveInDb(newMsg)

    // this.setState(previousState => ({
    //   messages: GiftedChat.append(previousState.messages, newMsg),
    //   showProductUi : false
    // }))
  }

  renderDay(props) {
    return <Day {...props} textStyle={{color: 'black'}}/>
  }
  renderBubble(props) {
    const usertype=props.currentMessage.userType;
    console.log("userTpe "+usertype)
    return (
     
      <Bubble {...props}
      position={usertype == 'Patient' ? 'right' : 'left'}
      textStyle={{
        right: {
            color: "#000",
            fontSize: 12
        },
        left: {
          color: "#000",
          fontSize: 12
        },

    }}
    timeTextStyle={{
      left: {
      color: '#999999',
      },
      right: {
      color: '#999999',
      },
      }}
        wrapperStyle={{
          left: {
            backgroundColor: 'white',
            borderRadius:10,
            marginRight: 5,
            marginLeft:20,
            marginVertical:5

          },
          right: {
            backgroundColor: '#fff',
            borderRadius:10,
            marginRight: 20,
            marginVertical:5
          }
        }} />
    )
  }


  docView(url, type) {

  }

  renderImage(uri, showLoader, image) {
    const images = [{ url: image.image },];

    return (
      <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }} onPress={() => {

        if (image.type == 'Image') {

          this.setState({ isShow: true, showImages: images })
        }
        else {
          this.setState({ showDoc: true, selectedUrl: image.image })
        }

      }}>

        <Image source={uri} style={{ margin: 10, height: 120, width: 130 }}

        />
        {showLoader ?
          <ActivityIndicator style=
            {{
              position: 'absolute', backgroundColor: '#fff',
              elevation: 5, borderRadius: 20
            }} size="large" color={Constant.appColorAlpha()} />
          : null}

      </TouchableOpacity>
    )
  }

  renderMessage = (props) => {



    return (
      <Message {...props} />
    )
  }
  renderAvatar=(props)=>{
    return 
  }

  renderMessageImage = (props) => {
    let image = props.currentMessage;

    if (image.type == "PDF") {
      return this.renderImage(images.medicine, image.isUpload, image)
    }
    else if (image.type == "DOCS") {
      return this.renderImage(images.medicine, image.isUpload, image)
    }
    else if (image.type == "Excel") {
      return this.renderImage(images.medicine, image.isUpload, image)
    }
    else {
      if (image.isUpload) {
        return this.renderImage({ uri: image.uri }, image.isUpload, image)
      }
      else {
        return this.renderImage({ uri: image.image }, image.isUpload, image)
      }
    }

    return null



  }

  renderActions = (props) => {


    return (
      <View style={styles.sendBtn}>
        <TouchableOpacity
          style={{ alignItems: 'center', justifyContent: 'center' }}
          onPress={() => {
            Keyboard.dismiss()
            if (this.state.showMenu) {
              this.hideMenu()
            }
            else {
              this.setState({ showMenu: true })
              this.transitedView.expand()
            }
          }}>
          <Image tintColor={'#fff'} style={{ width: 22, height: 22, resizeMode: 'contain', }}
            source={images.attach} />
        </TouchableOpacity>
      </View>
    )
  }
  hideMenu() {
    this.setState({ showMenu: false })
    this.transitedView.collapse()
  }

  renderMenu() {

    return (


      <Menu
        ref={(ref) => this.transitedView = ref}
        backgroundColor={'#f0f0f0'}
        duration={700}
        style={{ position: 'absolute', bottom: 48, right: 0, left: 0, height: 100 }}
        revealPositionArray={{ bottom: true, left: true }}// must use less than two combination e.g bottom and left or top right or right
      >
        <View style={{ flexDirection: 'row', width: '100%', height: 100, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '70%', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row' }}>
            <TouchableOpacity
              onPress={() => {
                this.hideMenu()
                this.openDocument()
              }}>
              <Image source={images.docs} style={{ height: 48, width: 48 }} />

            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
              this.hideMenu()
              this.openCamera()
            }}>
              <Image source={images.camera} style={{ height: 48, width: 48 }} />

            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
              this.hideMenu()
              this.openGallery()
            }}>
              <Image source={images.gallery} style={{ height: 48, width: 48 }} />

            </TouchableOpacity>

          </View>
        </View>

      </Menu>
    )
  }

  showImage() {

    return (
      <SafeAreaView style={{ flex: 1 }}>


        <Modal
          visible={this.state.isShow}
          transparent={true}
          onRequestClose={() => this.setState({ isShow: false })}>
          <ImageViewer
            enableSwipeDown={true} onSwipeDown={() => {

              console.log('down')
              this.setState({ isShow: false })
            }} imageUrls={this.state.showImages} />


          <TouchableOpacity style={{ position: 'absolute', top: 30, right: 30 }} onPress={() => {
            this.setState({ isShow: false })
          }}>
            <Image source={images.close} style={{ height: 25, width: 25 }} />

          </TouchableOpacity>


        </Modal>
      </SafeAreaView>
    )
  }

  renderLoadingView() {
    const dimensions = Dimensions.get('window');
    const marginTop = dimensions.height / 2 - 75;

    return (
      <ActivityIndicator
        animating={true}
        color='#0076BE'
        size='large'
        hidesWhenStopped={true}
        style={{ marginTop }}
      />

    );
  }


  showDoc() {

    //  let uri = 'http://www.pdf995.com/samples/pdf.pdf';

    //if (/\.pdf$/.test(uri)) {
    let uri = `https://drive.google.com/viewerng/viewer?embedded=true&url=${this.state.selectedUrl}`;
    // }

    return (

      <Modal
        visible={this.state.showDoc}
        transparent={false}
        onRequestClose={() => this.setState({ showDoc: false })}>
        <View style={{ flex: 1 }}>
          {/* <WebView
                  renderLoading={this.renderLoadingView}
                  source={{uri}}
                  startInLoadingState={true}
                /> */}


          <TouchableOpacity style={{ position: 'absolute', top: 30, right: 30 }} onPress={() => {
            this.setState({ showDoc: false })
          }}>
            <Image tintColor='#000' source={images.close} style={{ height: 25, width: 25 }} />

          </TouchableOpacity>
        </View>

      </Modal>


      //      <SafeAreaView style = {{flex:1,position : 'absolute',height : '100%',width : '100%'}}>
      //       <WebView
      //           renderLoading={this.renderLoadingView}
      //           source={{uri}}
      //           startInLoadingState={true}
      //         />


      //       <TouchableOpacity style = {{position : 'absolute',top:30,right : 30}} onPress={()=>{
      //        this.setState({isShow : false})
      //         }}>
      //         <Image source = {require('./../../Asserts/close.png')} style = {{height : 25,width : 25}} />

      //     </TouchableOpacity>



      // </SafeAreaView>

    )
  }





  renderSend(props) {
    return (
      <Send {...props}
        containerStyle={styles.sendBtn}>
        <Image style={{ height: 20, width: 20 }}
          tintColor={'#fff'}
          source={images.send} />
      </Send>

    );
  }

  renderComposer(props) {
    return (
      <Composer {...props} textInputStyle={[styles.textInput,{paddingHorizontal:20,marginBottom:60}]} />
    )
  }

  render() {
    return (
      <SafeAreaView style = {{flex:1,backgroundColor:'#fff'}}>

{/* <View style = {{backgroundColor:Constant.headerColor(),elevation: 4 ,borderBottomLeftRadius:30,borderBottomRightRadius:30}}>

<View style = {{paddingTop:10,paddingBottom:15, backgroundColor:Constant.headerColor() , flexDirection:'row',alignItems:'center',justifyContent:'center',borderBottomLeftRadius:30,borderBottomRightRadius:30}}>
<TouchableOpacity
onPress = {()=>{
    this.props.navigation.pop(1)
}}
style  ={{marginLeft:15,height:35,borderRadius:15,alignItems:'center',justifyContent:'center'}}>
<Image tintColor = '#fff' style = {{height:20,width:20,left:0}} source = {images.back}/>
</TouchableOpacity>
<Text style = {[{flex:1},BaseStyle.headerFont]}>{language.chat.toUpperCase()}</Text>

</View>

</View> */}
{this.headerView()}


      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
        alwaysShowSend={this.state.showProductUi}
        showUserAvatar={false}
        loadEarlier={false}
         isAnimated={true}
          renderSend={this.renderSend}
          renderMessage={this.renderMessage}
         renderMessageImage = {this.renderMessageImage}
          renderActions={this.renderActions}
          renderComposer={this.renderComposer}

         
        renderInputToolbar={(props) => (
          <InputToolbar {...props} containerStyle={{borderTopWidth: 0}} />
        )}
       
        renderBubble = {(props)=> this.renderBubble(props)}
        user={{
          _id: 1,
           avatar: 'https://placeimg.com/140/140/any',
        }}
      />
          { this.state.showMenu ?
        <TouchableOpacity 
        onPress = {()=>{
           this.hideMenu()
        }}
        style = {{position: 'absolute', bottom: 0, right: 0, left: 0,top:0}}>
        </TouchableOpacity> : null}

         {this.renderMenu()}
        {this.state.showDoc ? this.showDoc() : null}
         
         {this.state.isShow ? this.showImage() : null}
        
        
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create(
  {
    topText: {
      flex: 0.7
    }
    ,
    sendBtn: {
      margin: 6,
      elevation: 5,
      backgroundColor: Constant.appColorAlpha(),
      height: 40,
      width: 40,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center'
    },
    textInput: {
      marginHorizontal: 6,
      elevation: 5,
      backgroundColor: '#fff',
      borderRadius: 20,
      padding: 4
    },
    productUi: {
      marginLeft: 60,
      marginRight: 60,
      padding: 10,
      flexDirection: 'row',
      backgroundColor: '#F0EEEE',
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10
    },

    chatProduct: {
      marginRight: 5,
      padding: 10,
      flexDirection: 'row',
      backgroundColor: '#F0EEEE',
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      borderBottomLeftRadius: 10

    },
    headerInnerView: {
      height: 20,
      alignItems: "center",
      flexDirection: "row",
      alignContent: "center"
    },
    headerStyle: {
      flex: 0,
  
  backgroundColor:"#e5e5e5",
  
  
  
  
      justifyContent: "center",
    },

  }

)