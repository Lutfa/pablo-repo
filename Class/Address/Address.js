import React, { Component } from "react"
import {
  View,
  Text,
  AsyncStorage,
  Alert,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import urls from "./../BaseClass/ServiceUrls"
import MapView from "react-native-maps"
import language from "./../BaseClass/language"
import Db from "./../DB/Realm"

import { withNavigation } from "react-navigation"
const { width, height } = Dimensions.get("window")

class Address extends Component {
  constructor(props) {
    super(props)
    this.state = {
      AddressArray: [],
      isLoading: false,
      isSelect:
        props.route.params != null ? true : null,
    }
  }

  componentDidMount() {
    SplashScreen.hide()

    //  let addressData = Db.objects('Address')
    // this.setState({AddressArray:addressData})

    AsyncStorage.getItem("addressTimeTicks").then(
      (asyncStorageRes) => {
        if (
          asyncStorageRes != null &&
          asyncStorageRes != ""
        ) {
          this.getAddress(asyncStorageRes)
        } else {
          this.getAddress("0")
        }
      }
    )

    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          // this.getAddress()

          let addressData = Db.objects("Address")
          this.setState({
            AddressArray: addressData,
          })
        }
      )
  }
  render() {
    const { isLoading, AddressArray, isSelect } =
      this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {isLoading ? Constant.showLoader() : null}

        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: 4,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor:
                Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              {language.addresss.toUpperCase()}
            </Text>
          </View>
        </View>
        <FlatList
          style={{ marginTop: 10 }}
          data={this.state.AddressArray}
          extraData={this.state}
          keyExtractor={(item) => item.AddressId}
          renderItem={({ item, index }) => (
            <View
              style={{
                width: "100%",
                padding: 10,
                backgroundColor: "#fff",
              }}>
              <TouchableOpacity
                onPress={() => {
                  if (isSelect != null) {
                    this.props.route.params.select(
                      item
                    )
                    this.props.navigation.goBack()
                  }
                }}
                activeOpacity={2}
                style={{
                  elevation: 6,
                  backgroundColor: "#fff",
                  padding: 8,
                  borderRadius: 5,
                }}>
                <View
                  style={{ flexDirection: "row" }}>
                  <Image
                    style={{
                      width: 22,
                      height: 22,
                    }}
                    source={images.home}
                  />

                  <View
                    style={{
                      flex: 1,
                      paddingLeft: 10,
                      paddingRight: 10,
                    }}>
                    <Text
                      style={[
                        { flex: 1, fontSize: 13 },
                        BaseStyle.regularFont,
                      ]}>
                      {`${item.AddressCategory}`}
                    </Text>

                    <Text
                      style={[
                        {
                          flex: 1,
                          marginTop: 5,
                          fontSize: 11,
                          lineHeight: 14,
                          color:
                            Constant.textAlpha(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {`${item.Address1}`}
                    </Text>
                  </View>
                  {isSelect ? null : (
                    <View
                      style={{
                        width: 50,
                        height: 30,
                        flexDirection: "row",
                        justifyContent:
                          "space-between",
                      }}>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate(
                            "AddAddress",
                            { data: item }
                          )
                        }}>
                        <Image
                          style={{
                            width: 18,
                            height: 18,
                          }}
                          source={images.edit}
                        />
                      </TouchableOpacity>

                      <TouchableOpacity
                        onPress={() => {
                          this.deletePopUP(
                            item.AddressId
                          )
                        }}>
                        <Image
                          style={{
                            width: 18,
                            height: 18,
                          }}
                          source={images.delete}
                        />
                      </TouchableOpacity>
                    </View>
                  )}
                </View>

                <View
                  style={{
                    height: 80,
                    width: "100%",
                  }}>
                  <View
                    style={{
                      position: "absolute",
                      borderRadius: 30,
                      top: 20,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      backgroundColor:
                        Constant.headerColor() +
                        "66",
                      alignItems: "center",
                    }}></View>

                  <View
                    style={{
                      position: "absolute",
                      top: 6,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      alignItems: "center",
                    }}>
                    <Image
                      resizeMode={"center"}
                      style={{
                        height: 40,
                        width: 40,
                      }}
                      source={images.newPin}
                    />
                  </View>
                  {item.Latitude !== null &&
                  item.Latitude !== "" &&
                  item.Latitude !== undefined ? (
                    <View
                      style={{
                        height: 60,
                        marginTop: 20,
                        zIndex: -1,
                        borderRadius: 40,
                        overflow: "hidden",
                        alignItems: "center",
                        justifyContent: "center",
                      }}>
                      <MapView
                        style={{
                          flex: 1,
                          height: "100%",
                          width: "100%",
                        }}
                        region={{
                          latitude: !!parseFloat(
                            item.Latitude
                          )
                            ? parseFloat(
                                item.Latitude
                              )
                            : 0,
                          longitude: parseFloat(
                            item.Longitude
                          )
                            ? parseFloat(
                                item.Longitude
                              )
                            : 0,
                          latitudeDelta: 0.3,
                          longitudeDelta: 0.4,
                        }}
                        //ref={ map => {currentMap = map }}
                        //region={props.region}
                        rotateEnabled={false}
                        loadingEnabled={
                          true
                        }></MapView>
                    </View>
                  ) : null}
                </View>
              </TouchableOpacity>
            </View>
          )}
          ListEmptyComponent={Constant.noDataView(
            isLoading
          )}
        />

        {/* <View style = {{position:'absolute',right:30,bottom:30}}>
                    <TouchableOpacity 
                    onPress = {()=>{
                        this.props.navigation.navigate('AddAddress')
                    }}
                    style = {{height:50,justifyContent:'center',alignItems:'center' ,width:50,borderRadius:25,elevation:5 ,backgroundColor:'#fff'}}  >
                        <Image tintColor = {Constant.appFullColor()} style={{borderRadius:20, width: 25, height: 25 }} 
                            source={images.plus} />

                    </TouchableOpacity>
                </View> */}

        {isSelect == null ? (
          <View
            style={{
              height: 50,
              width: "100%",
              justifyContent: "center",
              alignItems: "center",
              paddingBottom: 10,
              marginTop: 10,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate(
                  "AddAddress",
                  { data: null }
                )
              }}
              style={{
                height: 50,
                width: "90%",
                backgroundColor:
                  Constant.appColorAlpha(),
                borderRadius: 25,
                justifyContent: "center",
                alignItems: "center",
              }}>
              <Text
                style={[
                  { color: "#fff", fontSize: 18 },
                  BaseStyle.boldFont,
                ]}>
                {language.addNewAddress}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </SafeAreaView>
    )
  }

  deletePopUP(id) {
    Alert.alert(
      "",
      "Do you want to delete this Address?",
      [
        { text: "CANCEL" },
        {
          text: "DELETE",
          onPress: () => this.deleteAddress(id),
        },
      ]
      // { cancelable: true }
    )
  }

  deleteAddress(id) {
    this.setState({ isLoading: true })
    let params = {
      AddressId: id,
      CustomerId: global.CustomerId,
    }

    Constant.postMethod(
      urls.deleteCustomerAddress,
      params,
      (result) => {
        this.setState({ isLoading: false })

        // alert(JSON.stringify(result))
        if (result.success) {
          let newAddress =
            this.state.AddressArray.filter(
              (addresss) => {
                return addresss.AddressId != id
              }
            )

          try {
            Db.write(() => {
              Db.delete(
                Db.objectForPrimaryKey(
                  "Address",
                  id
                )
              )
            })
          } catch (e) {
            console.log("Error on creatione")
            console.log(JSON.stringify(e))
          }

          this.setState({
            AddressArray: newAddress,
          })
        } else {
          //   this.setState({isNumberCorrect:false})
        }
      }
    )
  }

  getAddress(time) {
    this.setState({
      isLoading: time == "0" ? true : false,
    })
    let params = {
      CustomerId: global.CustomerId,
      LastUpdatedTimeTicks: time,
    }
    Constant.postMethod(
      urls.getCustomerAddress,
      params,
      (result) => {
        this.setState({ isLoading: false })
        console.log(
          "address: " + JSON.stringify(result)
        )
        if (result.success) {
          if (result.result.Response != null) {
            this.setState({
              AddressArray: result.result.Response,
            })

            result.result.Response.map(
              (oldItem, index) => {
                let item = oldItem.Result
                try {
                  Db.write(() => {
                    Db.create(
                      "Address",
                      {
                        FirstName: item.FirstName,
                        LastName: item.LastName,
                        Email: item.Email,
                        Company: item.Company,
                        CountryId: item.CountryId,
                        StateProvinceId:
                          item.StateProvinceId,
                        County: item.County,
                        City: item.City,
                        Address1: item.Address1,
                        Address2: item.Address2,
                        ZipPostalCode:
                          item.ZipPostalCode,
                        PhoneNumber:
                          item.PhoneNumber,
                        FaxNumber: item.FaxNumber,
                        CustomAttributes:
                          item.CustomAttributes,
                        CreatedOnUtc:
                          item.CreatedOnUtc,
                        AddressCategory: "Home",
                        Longitude: `${item.Longitude}`,
                        Latitude: `${item.Latitude}`,
                        LocationName:
                          item.LocationName,
                        AddressId: item.AddressId,
                        CustomerId: item.CustomerId,
                        DeviceUniqueId:
                          item.DeviceUniqueId,
                        Landmark: item.Landmark,
                      },
                      "modified"
                    )
                  })
                } catch (e) {
                  console.log(
                    "Error on address creation"
                  )
                  console.log(JSON.stringify(e))
                }
              }
            )

            // AsyncStorage.setItem(
            //   "addressTimeTicks",
            //   JSON.stringify(
            //     result.result.Response
            //       .LastUpdatedTimeTicks
            //   )
            // )
            let addressData = Db.objects("Address")
            this.setState({
              AddressArray: addressData,
            })
          } else {
            //    Toast.show(result.result.Response.Message)
          }
        } else {
          //   this.setState({isNumberCorrect:false})
        }
      }
    )
  }
}

export default Address
