import React,{Component} from 'react'
import {View,TextInput ,Text,
TouchableOpacity, 
Platform,
Keyboard,
DeviceEventEmitter,
  Animated,
  BackHandler,
  AsyncStorage,
  StatusBar,
  Modal,
  PermissionsAndroid,
ScrollView,StyleSheet,Image ,Dimensions} from 'react-native'
import RNGooglePlaces from 'react-native-google-places';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import BottomSheet from 'reanimated-bottom-sheet'
import { TextField,OutlinedTextField } from 'react-native-material-textfield'
import AnimateLoadingButton from './../ExtraClass/LoadingButton'
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';
import MapView from 'react-native-maps';  
import { Marker } from 'react-native-maps';  
import Geocoder from 'react-native-geocoder';
import Constant from './../BaseClass/Constant'
import BaseStyle from './../BaseClass/BaseStyles'
import Geolocation from '@react-native-community/geolocation';
import SplashScreen from 'react-native-splash-screen'
import images from './../BaseClass/Images'
import ScrollTest from './NewTest'
import Toast from 'react-native-simple-toast';
import urls from './../BaseClass/ServiceUrls'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from "react-native-safe-area-context";
import language from './../BaseClass/language'
import { lang } from 'moment';
const { width,height } = Dimensions.get('window');
import Db from './../DB/Realm'

export default class CreateAddress extends Component {

   static navigationOptions = ({navigation}) => {

        return{
          title: 'MAPS',
          headerTintColor: '#ffffff',
    
          headerStyle:
          {
            backgroundColor: Constant.appColorAlpha(),
          },
          headerTitleStyle:
          BaseStyle.headerStyle,
        };
    }

  constructor(props) {
    super(props)
    this.bottomSheet = React.createRef();

    this.state = {
       latitude : props.route.params != null ? Number(props.route.params.data.Latitude) : 0,
        longitude : props.route.params != null ? Number(props.route.params.data.Longitude) : 0,
       address : props.route.params != null ? props.route.params.data.Address1 : '',
        landmark : props.route.params != null ? props.route.params.data.Landmark : '',
        category:props.route.params != null ? props.route.params.data.AddressCategory : '',
     cityName : props.route.params != null ? props.route.params.data.City : '',
     state : props.route.params != null ? props.route.params.data.City : '',
     pincode : props.route.params != null ? props.route.params.data.ZipPostalCode : '',
     country : props.route.params != null ? props.route.params.data.County : '',
     isMenuOpend:false,
     typeIndex:-1,
     flatNo:props.route.params != null ? props.route.params.data.Address2 : '',
     categoryError:false,
  flatError:'',
  landmarkError:'',
  cityError:'',
  countryError:'',
   topSpace : height-150,
   isEdit : this.props.route.params != null ? true : false,
   addressId  : this.props.route.params != null ? this.props.route.params.data.AddressId : 0,
    }
  }
  
async  requestCameraPermission() 
{
  var _this = this;

  if(Platform.OS === 'ios'){
    this.findCoordinates();
  }else{
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
            'title': 'Location Access Required',
            'message': 'This App needs to Access your location'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //To Check, If Permission is granted
        
          _this.findCoordinates();
        } else {
        }
      } catch (err) {
      }
  }    
 }



findCoordinates()
{
var _this = this

Geolocation.getCurrentPosition(
   //Will give you the current location
   (position) => {
     
       const currentLongitude = position.coords.longitude
       const currentLatitude = position.coords.latitude
      _this.setState({latitude : currentLatitude,longitude : currentLongitude})
      
      _this.getAddress(currentLatitude,currentLongitude)

            let newCoords = {
                latitude: currentLatitude,
                longitude: currentLongitude
            }

            this.map.animateToCoordinate(newCoords, 1000)
          


   },
   (error) => console.log(error.message),
   { 
      enableHighAccuracy: false, timeout: 20000,
   }
);

	};

  openSearchModal() {
    RNGooglePlaces.openAutocompleteModal()
    .then((place) => {
		console.log(place.location.latitude);
    this.setState({latitude : place.location.latitude,longitude : place.location.longitude})
	

           let newCoords = {
                latitude: place.location.latitude,
                longitude: place.location.longitude
            }
            this.getAddress(this.state.latitude,this.state.longitude)
            this.map.animateToCoordinate(newCoords, 1000)

    
    })
    .catch(error => console.log(error.message));  // error is a Javascript Error object
  }

   async componentDidMount(){
    SplashScreen.hide();

    if(this.props.route.params != null)
    {
      var data = this.props.route.params.data;
      if(data.AddressCategory != null && data.AddressCategory != '')
      {
        var typeInde = -1

        if(data.AddressCategory == 'Home')
        {
          typeInde = 0
        }
        else if(data.AddressCategory == 'Work')
        {
          typeInde = 1
        }
        else if(data.AddressCategory == 'Others')
        {
          typeInde = 2
        }

       

        this.setState({typeIndex:typeInde})
      }
    }
    else{

     DeviceEventEmitter.addListener('locationProviderStatusChange', function(status) { // only trigger when "providerListener" is enabled
     this.findCoordinates();
   
     });

       this.findCoordinates()
      await this.requestCameraPermission()
      this.enableLocation()

    }
    };


    
    componentWillMount () {
      this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
  
    componentWillUnmount () {
      this.keyboardDidShowListener.remove();
      this.keyboardDidHideListener.remove();
    }
  
    _keyboardDidShow = () => {
      this.setState({topSpace:height/2})
    }
  
    _keyboardDidHide = () =>{
      this.setState({topSpace:height-150})
    }

   enableLocation()
  {

    const _this = this
   LocationServicesDialogBox.checkLocationServicesIsEnabled({
    message: "<h2 style='color: #0af13e'>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
    ok: "YES",
    cancel: "NO",
    enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
    showDialog: true, // false => Opens the Location access page directly
    openLocationServices: true, // false => Directly catch method is called if location services are turned off
    preventOutSideTouch: false, // true => To prevent the location services window from closing when it is clicked outside
    preventBackClick: false, // true => To prevent the location services popup from closing when it is clicked back button
    providerListener: false // true ==> Trigger locationProviderStatusChange listener when the location state changes
}).then(function(success) {
    _this.findCoordinates()
}).catch((error) => {
    console.log(error.message); // error.message => "disabled"
});
  }

    

  getAddress(latitude,longitude)
  {

    // if(this.state.longitude == longitude && this.state.latitude == latitude )
    // {
    //   return
    // }
    var pos = {
  lat: latitude,
  lng: longitude
};



    Geocoder.geocodePosition(pos).then(res => {


    this.setState({address:res[0].formattedAddress,
                  landmark: res[0].streetNumber != null ? res[0].streetNumber : '',
                  cityName : res[0].locality != null ? res[0].locality : '',
                  state : res[0].adminArea != null ? res[0].adminArea : '',
                  pincode : res[0].postalCode != null ? res[0].postalCode : '',
                  country : res[0].country != null ? res[0].country : ''
                  })
    

    // return res[0].formattedAddress
})
.catch(error => {console.log(error)});
  }


onRegionChange = (region) => {

    this.setState({latitude:region.latitude,longitude:region.longitude})
    this.getAddress(region.latitude,region.longitude)
  }


  
  render() {

    const {latitude , longitude , address,landmark,cityName,state,pincode,country,isMenuOpend} = this.state




  


    return (
      <View  style = {{flex:1,backgroundColor:'#fff'}}>

<SafeAreaView style = {{backgroundColor:Constant.headerColor(),borderBottomLeftRadius:30,borderBottomRightRadius:30,overflow:'hidden',zIndex:98 }}>
<View style = {{paddingTop:10,paddingBottom:15, backgroundColor:Constant.headerColor() , flexDirection:'row',alignItems:'center',justifyContent:'center',}}>
<TouchableOpacity
  onPress = {()=>{
      this.props.navigation.pop(1)
  }}
   style  ={{marginLeft:15,height:35,borderRadius:15,alignItems:'center',justifyContent:'center'}}>
  <Image tintColor = '#fff' style = {{height:20,width:20,left:0}} source = {images.back}/>
  </TouchableOpacity>
  <Text style = {[{flex:1},BaseStyle.headerFont]}>{language.addresss}</Text>

  </View>

  {isMenuOpend  ? null :

  <View style={styles.searchMainView}>
          <TouchableOpacity
            onPress={() => {
this.openSearchModal()
            }}
            style={[styles.searchInner]}>
            <Image source={images.search} tintColor={Constant.textAlpha()} style={
              {
                marginLeft: 10,
                width: 20,
                height: 20,
              }
            } />
            <Text style={[BaseStyle.boldFont, styles.searchIcon]}>{language.searchLocation}</Text>
          </TouchableOpacity>
          </View> }

  </SafeAreaView>
  
  
        <MapView  
        ref={map => { this.map = map }}
          style={styles.mapStyle}  
          showsUserLocation
          zoomControlEnabled={false}  
          provider="google"
          followsUserLocation={false}
           zoomEnabled={true}  
          showsMyLocationButton = {false}
          onRegionChangeComplete={this.onRegionChange}

          onMapReady={() => {
            PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            ).then(granted => {
            });
          }}
        
      
  
          initialRegion={{  
            latitude: latitude != null ? latitude : 0,   
            longitude: longitude != null ? longitude : 0,  
            latitudeDelta: 0.1022,  
            longitudeDelta: 0.10421,  
          }}>  
  
        </MapView> 



       
       


           <View style = {{position:'absolute',right:30,bottom:100,zIndex:99}}>
                    <TouchableOpacity style = {{height:50,justifyContent:'center',alignItems:'center' ,width:50,borderRadius:25,elevation:5 ,backgroundColor:'#fff'}} onPress={() => this.findCoordinates()} >
                        <Image style={{borderRadius:20, width: 30, height: 30 }} 
                            source={images.location} />

                    </TouchableOpacity>
                </View>

              

        {/* {!this.state.isMenuOpend ?
        <View style = {{ zIndex:98 ,position : 'absolute',top : 60, width :'100%',height  :50,alignItems : 'center',justifyContent : 'center'}}>
        <TouchableOpacity 
        onPress = {()=>{
          if(!this.state.isMenuOpend)
          {
          this.openSearchModal()
          }
          }}
        style = {[BaseStyle.addShadow,{width :'90%',elevation:8, height : 50,backgroundColor : '#fff',borderRadius : 5,flexDirection:'row', justifyContent : 'center',alignItems:'center'}]}>
         <Image source = {images.search} style = {
             {
                marginLeft:10,
                 width : 20,
                 height : 20,
                }
            }/>
            <Text style = {[BaseStyle.fontFamily, {marginLeft:0, color : '#000',textAlign : 'left', left:20,fontSize:14,flex:1}]}>Search Location</Text>
        </TouchableOpacity>
        </View> : null} */}
        
         {/* <View style = {{width : '100%',backgroundColor : Constant.appColorAlpha()}}>
             
             <Text onPress = {()=>{
               if(this.state.address != '')
               {
               this.props.navigation.state.params.doSomething(this.state.address)
              this.props.navigation.goBack(null);
               }
             }} style = {[BaseStyle.fontFamily, {color : '#fff',textAlign : 'center', padding:20,fontSize:14}]}>{this.state.address}</Text>
        </View> */}
        {this.state.isMenuOpend ?
        <TouchableOpacity
        onPress = {()=>{
                this.bottomSheet.current.snapTo(0)

        }}
         style = {{position:'absolute',zIndex:100 ,top:0,left:0,right:0,bottom:0,backgroundColor:'rgba(255,255,255,0.5)'}}>
        </TouchableOpacity>
        : null}
        <BottomSheet
        style = {{zIndex:100,backgroundColor:'#fff'}}
        ref={this.bottomSheet}
          snapPoints = {[95,this.state.topSpace , 95]}
          renderContent = {this.renderContent}
          
          renderHeader = {this.renderHeader}
          onOpenStart	= {()=>{
            this.setState({isMenuOpend:true})
          }}
          onCloseEnd={()=>{
            this.setState({isMenuOpend:false})

          }}
        />

       <Image resizeMode={'center'} source = {images.newPin} style = {{position:'absolute',width:35,height:35,top:(height/2)-15,left:(width/2)-15}}/>    
      </View>  
    );
  }


   renderContent = () => 
   {
         const {typeIndex,address,landmark,cityName,flatNo,pincode,country,category,flatError,landmarkError,cityError,countryError,categoryError} = this.state
   return (
    // renderBottom()
    // {

    //     return(

    
            // <ScrollView>
            <KeyboardAwareScrollView extraHeight={90} extraScrollHeight={50}>
            <View style = {{height:'100%',flex:1,paddingRight:15,paddingLeft:15,backgroundColor:'#fff'}}>
      <View style  ={{width:'100%',backgroundColor:'#fff',height:'100%',flex:1}}>
        

    <Text numberOfLines ={2} style = {[{marginTop:5,fontSize:16},BaseStyle.boldFont]}>{language.type}</Text>
    
    <View style  = {{marginTop:10,height:70,width:width-40,borderRadius:5 ,flexDirection:'row',justifyContent:'space-between',backgroundColor:'#fff'}}>


    <TouchableOpacity
    onPress = {()=>{
      this.setState({typeIndex :0,category:'Home'})
    }}
     style = {[styles.typeView,{borderWidth:this.state.typeIndex == 0 ? 1 : 0 }]}>

    <Image resizeMode={'center'} style  ={{height:25,width:25}} source = {images.ahome}/>
    <Text numberOfLines ={2} style = {[{marginTop:5,fontSize:13},BaseStyle.regularFont]}>Home</Text>

    </TouchableOpacity>

    <TouchableOpacity
    onPress = {()=>{
      this.setState({typeIndex :1,category:'Work'})
    }}
     style = {[styles.typeView,{borderWidth:this.state.typeIndex == 1 ? 1 : 0 }]}>
    <Image resizeMode={'center'} style  ={{height:25,width:25}} source = {images.awork}/>
    <Text numberOfLines ={2} style = {[{marginTop:5,fontSize:13},BaseStyle.regularFont]}>Work</Text>

    </TouchableOpacity>

    <TouchableOpacity
    onPress = {()=>{
      this.setState({typeIndex :2,category:'Others'})
    }}
     style = {[styles.typeView,{borderWidth:this.state.typeIndex == 2 ? 1 : 0 }]}>
    <Image resizeMode={'center'} style  ={{height:25,width:25}} source = {images.aothers}/>
    <Text numberOfLines ={2} style = {[{marginTop:5,fontSize:13},BaseStyle.regularFont]}>Others</Text>

    </TouchableOpacity>

    </View>
    
           
    
    {/* <TextField
          inputStyle = {[{height:50,color:Constant.appColorAlpha(),fontSize:14}]}
          ref="email"
          label={language.houseFlat}
         textFocusColor={this.state.flatError ? 'red' : '#c0c0c0'}
         value={this.state.flatNo}
          highlightColor={this.state.flatError ? 'red' : Constant.appColorAlpha()}
          onChangeText={(text)=>{
            this.setState({flatNo:text,flatError:false})
          }}
          onFocus={()=>{

          }}
        /> */}

        <TextField
        label={language.houseFlat}
        value={this.state.flatNo}
        lineWidth={1}
        fontSize={15}
        labelFontSize={13}
        labelTextStyle={[{paddingTop:5},BaseStyle.regularFont]}
        baseColor={Constant.textColor()}
        tintColor={Constant.textColor()}
        textColor={'#000'}
        onChangeText = { (text) => this.setState({ flatNo:text , isMailCorrect : '' }) }
        error={this.state.flatError}
        />


 <TextField
        label={language.landmark}
        value={landmark}
        lineWidth={1}
        fontSize={15}
        labelFontSize={13}
        labelTextStyle={[{paddingTop:5},BaseStyle.regularFont]}
        baseColor={Constant.textColor()}
        tintColor={Constant.textColor()}
        textColor={'#000'}
        onChangeText = { (text) => this.setState({ landmark:text , isMailCorrect : '' }) }
        error={this.state.landmarkError}
        />



<TextField
        label={language.city}
        value={cityName}
        lineWidth={1}
        fontSize={15}
        labelFontSize={13}
        labelTextStyle={[{paddingTop:5},BaseStyle.regularFont]}
        baseColor={Constant.textColor()}
        tintColor={Constant.textColor()}
        textColor={'#000'}
        onChangeText = { (text) => this.setState({ cityName:text , isMailCorrect : '' }) }
        />

        <TextField
        label={language.country}
        value={country}
        lineWidth={1}
        fontSize={15}
        labelFontSize={13}
        labelTextStyle={[{paddingTop:5},BaseStyle.regularFont]}
        baseColor={Constant.textColor()}
        tintColor={Constant.textColor()}
        textColor={'#000'}
        onChangeText = { (text) => this.setState({ country:text , isMailCorrect : '' }) }
        />





         <TextField
        label={language.zipcode}
        value={pincode}
        lineWidth={1}
        fontSize={15}
        editable={false}
        labelFontSize={13}
        labelTextStyle={[{paddingTop:5},BaseStyle.regularFont]}
        baseColor={Constant.textColor()}
        tintColor={Constant.textColor()}
        textColor={'#000'}
        />

        <TextField
        label={language.category}
        value={category}
        lineWidth={1}
        fontSize={15}
        editable={false}
        labelFontSize={13}
        labelTextStyle={[{paddingTop:5},BaseStyle.regularFont]}
        baseColor={Constant.textColor()}
        tintColor={Constant.textColor()}
        textColor={'#000'}
         onChangeText={(text)=>{
            this.setState({category:text,categoryError:false})
          }}
          onFocus={()=>{
            if(this.state.category == 'Others')
            {
              this.setState({category:''})
            }
          }}
        />

        <View style = {{height:20,width:300}}>
          </View>

          <TouchableOpacity style = {styles.startView}>
                <AnimateLoadingButton
                 ref={c => (this.loadingButton = c)}
                 width={width-30}
                 activityIndicatorColor={'#fff'}
                 height={50}
                 title={language.save.toUpperCase()}
                 titleFontSize={16}
                 titleWeight={'100'}
                 titleColor="rgb(255,255,255)"
                 backgroundColor={Constant.appColorAlpha()}
                 borderRadius={25}
                 onPress={()=>{
                   this.serviceCall()
                 }}
               />
                </TouchableOpacity>
         
       
    </View>

</View>
</KeyboardAwareScrollView>
    // </ScrollView>
        )
      
 // )
   }
         
 serviceCall()
 {
  
  if(this.validate())
  {
    

  this.loadingButton.showLoading(true)

  const userData = global.userData

const {country,cityName,address,pincode,category,latitude,longitude,landmark,flatNo,addressId,isEdit} = this.state
  let params = {
      "AddressId": addressId,
      "CustomerId": global.CustomerId,
      "County": country,
      "City": cityName,
      "Address1": address,
      "Address2": flatNo,
      "ZipPostalCode": pincode,
      "AddressCategory": category,
      "Longitude": longitude,
      "Latitude": latitude,
      "LocationName": landmark,
      "DeviceUniqueId": null,
      "Landmark": landmark,
  "FirstName": userData.Username,
  "LastName": userData.Username,
  "Email": userData.Email,
 

    }

    Constant.postMethod(isEdit ?  urls.editCustomerAddress :  urls.addCustomerAddress,params, (result) => {
      this.loadingButton.showLoading(false)

    if(result.success)
    {
      if(result.result.Response != null)
      {

            let item = result.result.Response
            try {
                Db.write(() => {
                     Db.create('Address', {
                        FirstName: item.FirstName,
                        LastName:item.LastName,
                        Email:item.Email,
                        Company:item.Company,
                        CountryId:item.CountryId,
                        StateProvinceId:item.StateProvinceId,
                        County:item.County,
                        City:item.City,
                        Address1: item.Address1,
                        Address2:item.Address2,
                        ZipPostalCode:item.ZipPostalCode,
                        PhoneNumber:item.PhoneNumber,
                        FaxNumber:item.FaxNumber,
                        CustomAttributes:item.CustomAttributes,
                        CreatedOnUtc:item.CreatedOnUtc,
                        AddressCategory:item.AddressCategory,
                        Longitude:item.Longitude,
                        Latitude:item.Latitude,
                        LocationName:item.LocationName,
                        AddressId:item.AddressId,
                        CustomerId:item.CustomerId,
                        DeviceUniqueId:item.DeviceUniqueId,
                        Landmark:item.Landmark
                       
                    },'modified');   

                });
                } catch (e) {
                console.log("Error on creatione");
                console.log(e);
                
                }

               

        this.props.navigation.pop(1)
      }
      else
      {
          Toast.show('Something went wrong')
      }

    }
    else
    {
      Toast.show('Something went wrong')
    }

  })
  }

    
 }
 validate()
 {
  const {typeIndex,address,landmark,cityName,flatNo,pincode,country,category,flatError,landmarkError,cityError,countryError,categoryError} = this.state
var boolValue = true
  if(typeIndex == -1 || category == '')
  {
    Toast.show('Please select address type')
    this.setState({categoryError:true})
    boolValue  = false
  }

  // if(flatNo == '')
  // {
  //   this.setState({flatError:true})
  //   boolValue  = false
  // }

  if(landmark == '')
  {
    this.setState({landmarkError:'Please enter landmark'})
    boolValue  = false
  }
  
  if(cityName == '')
  {
    this.setState({cityError:'Please enter city'})
    boolValue  = false
  }

  if(country == '')
  {
    this.setState({countryError:'Please enter country'})
    boolValue  = false
  }

  if(address == '')
  {
    Toast.show('Please select address')
    boolValue = false
  }

  return boolValue
}

  renderHeader = () => (
    /* render */

    <TouchableOpacity 
    onPress = {()=>{
      this.bottomSheet.current.snapTo(1)

    }}
    style  ={{width:'100%',height:100,borderTopLeftRadius:8,  backgroundColor: this.state.isMenuOpend ? '#fff' : null, borderTopRightRadius:8,padding:15,paddingTop:0}}>
        <View style = {{width:'100%',justifyContent:'center',flexDirection:'row',elevation:2 ,overflow:'hidden' ,alignItems:'center',backgroundColor:"#fff",height:'100%',borderRadius:8}}>
          <View style = {{height:'100%',width:'30%', justifyContent:'center',alignItems:'center',backgroundColor:Constant.appColorAlpha()}}>
            <Image style = {{height:20,width:20}} source = {images.direct} />
          </View>
          <View style = {{flex:1,padding:5}}>
              <Text numberOfLines ={2} style = {[{paddingTop:10,paddingLeft:10,paddingRight:10},BaseStyle.mediumFont]}>{this.state.address}</Text>
           </View>
          </View>
        </TouchableOpacity>
  )

}

const styles = StyleSheet.create({  
  MainContainer: {  
   flex: 1,
   backgroundColor:'#fff'
  },  
  startView:{
    marginTop:10,
   borderRadius : 25,
   height:50,
   marginLeft:15,
   marginRight:15,
   marginBottom:10,
   alignItems:'center',justifyContent:'center'
},
typeView:{flex:0.3,borderColor:Constant.appColorAlpha(),alignItems:'center',justifyContent:'center',elevation:3, backgroundColor:'#fff',borderRadius:5 },
header: {
  alignItems: 'center',
  backgroundColor: 'white',
  paddingVertical: 20,
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20
},
contentContainerStyle: {
  padding: 16,
  flex:1,
  backgroundColor: '#F3F4F9',
},
panelHandle: {
  width: 40,
  height: 2,
  backgroundColor: 'rgba(0,0,0,0.3)',
  borderRadius: 4
},
  mapStyle: {  
    width : '100%',
    height:'100%',
    flex:1,
    marginTop:-30
  },  
  searchMainView:{ width: '100%', paddingTop: 10,paddingRight:5,paddingLeft:5,  height: 50, marginBottom:20,  alignItems: 'center', justifyContent: 'center' },

  searchInner:{ marginRight: 5, marginLeft: 5, elevation: 0, height: 45, backgroundColor: '#fff', borderRadius: 5, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' },
  searchIcon:{ marginLeft: 0, color: Constant.textAlpha(), textAlign: 'left', left: 15, fontSize: 14, flex: 1 },
  

  scrollBackView: {
    flex: 1,
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
    justifyContent: 'center',
    flexDirection:'column-reverse',
    alignItems: 'center',
  },
});  