import React, { Component } from "react"
import {
  View,
  ImageBackground,
  AppState,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import SwipeListView from "./../ExtraClass/SwipeList/SwipeListView"
import Db from "./../DB/Realm"
const { width, height } = Dimensions.get("window")
import ImageLoad from "./../BaseClass/ImageLoader"
import CartDb from "./../DB/CartDb"
import Toast from "react-native-simple-toast"
import NoData from "../BaseClass/NoData"

class Wishlist extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isSpeaking: false,
      wishlistArray: [],
    }
  }

  componentDidMount() {
    let wishlistData = Db.objects("WishListDB")
    this.setState({ wishlistArray: wishlistData })
  }

  render() {
    const { article, isSpeaking } = this.state

    const { wishlistArray } = this.state
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: 4,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor: Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              WISHLIST
            </Text>
          </View>
        </View>

        {wishlistArray.length > 0 ? (
          <SwipeListView
            data={wishlistArray}
            keyExtractor={(item) => item.Name}
            disableRightSwipe
            renderItem={(data) => {
              let Isexit = CartDb.productExist(
                data.item.Id
              )
              return (
                <View
                  key={data.index}
                  style={{
                    width: "100%",
                    padding: 10,
                    marginBottom:
                      data.index ==
                      wishlistArray.length - 1
                        ? 50
                        : 0,
                  }}>
                  <View
                    style={{
                      elevation: 4,
                      borderRadius: 8,
                      backgroundColor: "#fff",
                      flexDirection: "row",
                    }}>
                    {/* <Image resizeMode={'center'} style = {{width:100,height:110}} source = {images.testNew}/> */}

                    <ImageLoad
                      resizeMode={"center"}
                      placeholderStyle={{
                        width: 100,
                        height: 110,
                      }}
                      style={{
                        width: 100,
                        height: 110,
                      }}
                      loadingStyle={{
                        size: "large",
                        color: Constant.appColorAlpha(),
                      }}
                      source={{
                        uri:
                          data.item.Images.length >
                          0
                            ? data.item.Images[0]
                                .Src
                            : "null",
                      }}
                      placeholderSource={
                        images.placeholder
                      }
                    />

                    <View
                      style={{
                        flex: 1,
                        paddingLeft: 10,
                        marginTop: 15,
                        justifyContent:
                          "space-between",
                        marginBottom: 15,
                      }}>
                      <View>
                        <Text
                          style={[
                            { fontSize: 18 },
                            BaseStyle.boldFont,
                          ]}>
                          {data.item.Name}
                        </Text>
                        <Text
                          style={[
                            {
                              fontSize: 13,
                              marginTop: 2,
                              color: Constant.textAlpha(),
                            },
                            BaseStyle.regularFont,
                          ]}>
                          {data.item.Description}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent:
                            "space-between",
                          width: "94%",
                          paddingTop: 15,
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            if (Isexit) {
                              this.props.navigation.navigate(
                                "Cart"
                              )
                            } else {
                              let isAdded = CartDb.addData(
                                data.item
                              )
                              if (isAdded) {
                                Toast.show(
                                  "Product added to cart"
                                )
                              }
                              this.forceUpdate()
                            }
                          }}
                          style={{
                            height: 34,
                            width: 90,
                            justifyContent:
                              "center",
                            alignItems: "center",
                            backgroundColor: Constant.appColorAlpha(),
                            borderRadius: 17,
                            elevation: 4,
                          }}>
                          <Text
                            style={[
                              {
                                fontSize: 12,
                                color: "#fff",
                              },
                              BaseStyle.boldFont,
                            ]}>
                            {Isexit
                              ? "Go to Cart"
                              : "Add to cart"}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              )
            }}
            renderHiddenItem={(data, rowMap) => (
              <TouchableOpacity
                onPress={() => {
                  this.closeRow(rowMap, data.item)
                  CartDb.deleteWishList(
                    data.item.Id
                  )
                  this.forceUpdate()
                }}
                key={data.item.Name}
                style={{
                  height: "84%",
                  width: width - 20,
                  position: "absolute",
                  borderRadius: 8,
                  right: 10,
                  marginTop: 10,
                  paddingBottom: 10,
                  height: 125,
                  backgroundColor: Constant.headerColor(),
                }}>
                <View
                  style={{
                    width: 90,
                    position: "absolute",
                    right: 10,
                    height: "100%",
                    borderTopRightRadius: 8,
                    borderBottomRightRadius: 8,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: Constant.headerColor(),
                  }}>
                  <Image
                    tintColor={"#fff"}
                    style={{
                      height: 20,
                      width: 20,
                    }}
                    source={images.delete}
                  />
                  <Text
                    style={[
                      {
                        fontSize: 13,
                        marginTop: 2,
                        marginTop: 5,
                        color: Constant.selectedTextColor(),
                      },
                      BaseStyle.regularFont,
                    ]}>
                    Delete
                  </Text>
                </View>
              </TouchableOpacity>
            )}
            leftOpenValue={0}
            rightOpenValue={-100}
          />
        ) : (
          <NoData />
        )}
      </SafeAreaView>
    )
  }

  closeRow(rowMap, rowKey) {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow()
    }
  }
}
export default Wishlist
