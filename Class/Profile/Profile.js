import React, { Component } from "react"
import {
  View,
  TextInput,
  ImageBackground,
  AppState,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import SwipeListView from "./../ExtraClass/SwipeList/SwipeListView"
import SplashScreen from "react-native-splash-screen"
import MapView from "react-native-maps"
import { FlatList } from "react-native-gesture-handler"
import { AnimatedCircularProgress } from "react-native-circular-progress"
import language from "./../BaseClass/language"
const { width, height } = Dimensions.get("window")
const MAX_POINTS = 90
import moment from "moment"
import ImageLoad from "./../BaseClass/ImageLoader"

class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      categoryIndex: 0,
      points: 80,
      isNew: true,
    }
  }

  componentDidMount() {
    SplashScreen.hide()
    this._unsubscribe = this.props.navigation.addListener(
      "focus",
      () => {
        this.setState({ isNew: false })
      }
    )
  }

  render() {
    const fill =
      (this.state.points / MAX_POINTS) * 100
    const { categoryIndex } = this.state
    const userData = global.userData

    var height = parseInt(userData.Height)
    height = height > 0 ? height : 0

    var weight = parseInt(userData.Weight)
    weight = weight > 0 ? weight : 0
    
     var bmi = weight/((height/100)*(height/100))
    
    if (!!bmi) {
      bmi = bmi.toFixed(2)
    }

    const healthIssueArray =
      userData.HealthIssues != null &&
      userData.HealthIssues != ""
        ? userData.HealthIssues.split(",")
        : []

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        <View
          style={{
            backgroundColor: Constant.headerColor(),
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
            paddingBottom: 50,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor: Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              {language.profile.toUpperCase()}
            </Text>

            <TouchableOpacity
              style={{
                padding: 4,
                paddingRight: 15,
              }}
              onPress={() => {
                this.props.navigation.navigate(
                  "EditProfile"
                )
              }}>
              <Image
                tintColor="#fff"
                style={{ height: 20, width: 20 }}
                source={images.pencil}
              />
            </TouchableOpacity>
          </View>
        </View>

        {/* PROFILE */}

        <ScrollView style={{ marginTop: -55 }}>
          <View>
            {/* imageview */}
            <View
              style={{
                flexDirection: "row",
                paddingLeft: 15,
                paddingRight: 15,
              }}>
              <View
                style={{
                  padding: 2,
                  overflow: "hidden",
                  backgroundColor: "#fff",
                  elevation: 4,
                  borderRadius: 47,
                  height: 94,
                  width: 94,
                  justifyContent: "center",
                  alignItems: "center",
                }}>


                  
                <ImageLoad
                  borderRadius={45}
                  placeholderStyle={{
                    height: 90,
                    width: 90,
                    borderRadius: 45,
                  }}
                  style={{
                    height: 90,
                    width: 90,
                    borderRadius: 45,
                  }}
                  loadingStyle={{
                    size: "large",
                    color: Constant.appColorAlpha(),
                  }}
                  source={{
                    uri: userData.ProfilePicture,
                  }}
                  placeholderSource={images.user}
                />
                {/* <Image resizeMode={'contain'} style = {{height:90,width:90}} source = {images.user}/> */}
              </View>
              <View
                style={{
                  flex: 1,
                  paddingTop: 55,
                  padding: 5,
                }}>
                <View
                  style={{
                    backgroundColor: "#fff",
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 18,
                        color: Constant.appColorAlpha(),
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {userData.Username}
                  </Text>
                  <Text
                    style={[
                      {
                        fontSize: 12,
                        color: Constant.textColor(),
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {userData.Email}
                  </Text>
                </View>
              </View>
            </View>

            {/* details */}

            <View style={styles.detailsView}>
              <View
                style={{
                  flex: 0.5,
                  justifyContent: "space-between",
                  flexDirection: "row",
                  paddingRight: 10,
                }}>
                <View style={styles.backMainView}>
                  <View style={styles.backView}>
                    <Image
                      style={{
                        height: 10,
                        width: 10,
                        tintColor:Constant.appColorAlpha()
                      }}
                      source={images.phone}
                    />
                  </View>
                  <Text
                    style={[
                      styles.detailsText,
                      BaseStyle.regularFont,
                    ]}>
                    {userData.PhoneNumber}
                  </Text>
                </View>

                <View style={styles.backMainView}>
                  <View style={styles.backView}>
                    <Image
                      style={{
                        height: 10,
                        width: 10,
                        tintColor:Constant.appColorAlpha()
                      }}
                      source={images.gender}
                    />
                  </View>
                  <Text
                    style={[
                      styles.detailsText,
                      BaseStyle.regularFont,
                    ]}>
                    {userData.Gender}
                  </Text>
                </View>

                <View style={styles.backMainView}>
                  <View style={styles.backView}>
                    <Image
                      style={{
                        height: 10,
                        width: 10,
                      }}
                      source={images.dob}
                    />
                  </View>
                  <Text
                    style={[
                      styles.detailsText,
                      BaseStyle.regularFont,
                    ]}>
                    {userData.Dob}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flex: 0.5,
                  marginTop: 10,
                  justifyContent: "space-between",
                  flexDirection: "row",
                  paddingRight: 10,
                }}>
                <View style={styles.backMainView}>
                  <View style={styles.backView}>
                    <Image
                      style={{
                        height: 10,
                        width: 10,
                      }}
                      source={images.height}
                    />
                  </View>
                  <Text
                    style={[
                      styles.detailsText,
                      BaseStyle.regularFont,
                    ]}>
                    {userData.Height} Cm
                  </Text>
                </View>

                <View style={styles.backMainView}>
                  <View style={styles.backView}>
                    <Image
                      style={{
                        height: 10,
                        width: 10,
                      }}
                      source={images.bloodGroup}
                    />
                  </View>
                  <Text
                    style={[
                      styles.detailsText,
                      BaseStyle.regularFont,
                    ]}>
                    {userData.BloodGroup}
                  </Text>
                </View>

                <View style={styles.backMainView}>
                  <View style={styles.backView}>
                    <Image
                      style={{
                        height: 10,
                        width: 10,
                      }}
                      source={images.weight}
                    />
                  </View>
                  <Text
                    style={[
                      styles.detailsText,
                      BaseStyle.regularFont,
                    ]}>
                    {userData.Weight} Kg
                  </Text>
                </View>
              </View>
            </View>

            {/* healthIssues */}

            {healthIssueArray.length > 0 ? (
              <View
                style={{
                  padding: 10,
                  paddingRight: 20,
                  paddingLeft: 20,
                  backgroundColor: "#fff",
                }}>
                <Text
                  activeOpacity={2}
                  style={[
                    { fontSize: 16 },
                    BaseStyle.boldFont,
                  ]}>
                  {language.healthissues}
                </Text>
                <View
                  style={{
                    height: 3,
                    width: 50,
                    borderRadius: 1.5,
                    backgroundColor: Constant.appColorAlpha(),
                  }}
                />

                <View
                  style={{
                    padding: 5,
                    paddingTop: 10,
                  }}>
                  <FlatList
                    data={healthIssueArray}
                    horizontal={true}
                    renderItem={({
                      item,
                      index,
                    }) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({
                            categoryIndex: index,
                          })
                        }}
                        style={{
                          height: 36,
                          padding: 5,
                          marginRight: 6,
                          alignItems: "center",
                          justifyContent: "center",
                          backgroundColor:
                            categoryIndex == index
                              ? Constant.appColorAlpha()
                              : "#fff",
                          borderWidth:
                            categoryIndex != index
                              ? 1
                              : 0,
                          borderColor: Constant.headerColor(),
                          borderRadius: 18,
                        }}>
                        <Text
                          numberOfLines={1}
                          style={[
                            {
                              fontSize: 14,
                              color:
                                categoryIndex ==
                                index
                                  ? "#fff"
                                  : Constant.headerColor(),
                              textAlign: "center",
                              width: 60,
                            },
                            BaseStyle.boldFont,
                          ]}>
                          {item}
                        </Text>
                      </TouchableOpacity>
                    )}
                  />
                </View>
              </View>
            ) : null}
            {userData.SCProof != "null" &&
            userData.SCProof != "" ? (
              <View
                style={{
                  padding: 10,
                  paddingTop: 0,
                  paddingRight: 20,
                  paddingLeft: 20,
                  backgroundColor: "#fff",
                }}>
                <Text
                  activeOpacity={2}
                  style={[
                    { fontSize: 16 },
                    BaseStyle.boldFont,
                  ]}>
                  {language.seniorDocument}
                </Text>
                <View
                  style={{
                    height: 3,
                    width: 50,
                    borderRadius: 1.5,
                    backgroundColor: Constant.appColorAlpha(),
                  }}
                />

                <View
                  style={{
                    padding: 6,
                    backgroundColor: "#fff",
                    borderRadius: 6,
                    elevation: 4,
                  }}>
                  {/* <Image style = {{height:250,width:width-55,backgroundColor:'#fff'}} source = {images.sc}/> */}

                  <ImageLoad
                    placeholderStyle={{
                      height: 250,
                      width: width - 55,
                      backgroundColor: "#fff",
                    }}
                    style={{
                      height: 250,
                      width: width - 55,
                      backgroundColor: "#fff",
                    }}
                    loadingStyle={{
                      size: "large",
                      color: Constant.appColorAlpha(),
                    }}
                    source={{
                      uri: userData.SCProof,
                    }}
                    placeholderSource={
                      images.placeholder
                    }
                  />
                </View>
              </View>
            ) : null}

            {userData.PHCDocument != "null" &&
            userData.PHCDocument != "" ? (
              <View
                style={{
                  padding: 10,
                  paddingTop: 5,
                  paddingRight: 20,
                  paddingLeft: 20,
                  backgroundColor: "#fff",
                }}>
                <Text
                  activeOpacity={2}
                  style={[
                    { fontSize: 16 },
                    BaseStyle.boldFont,
                  ]}>
                  {language.handicappedDocument}
                </Text>
                <View
                  style={{
                    height: 3,
                    width: 50,
                    borderRadius: 1.5,
                    backgroundColor: Constant.appColorAlpha(),
                  }}
                />

                <View
                  style={{
                    padding: 6,
                    backgroundColor: "#fff",
                    borderRadius: 6,
                    elevation: 4,
                  }}>
                  <ImageLoad
                    placeholderStyle={{
                      height: 250,
                      width: width - 55,
                      backgroundColor: "#fff",
                    }}
                    style={{
                      height: 250,
                      width: width - 55,
                      backgroundColor: "#fff",
                    }}
                    loadingStyle={{
                      size: "large",
                      color: Constant.appColorAlpha(),
                    }}
                    source={{
                      uri: userData.PHCDocument,
                    }}
                    placeholderSource={
                      images.placeholder
                    }
                  />
                </View>
              </View>
            ) : null}

            {/* Body mass index */}
            <View
              style={{
                padding: 10,
                paddingTop: 5,
                paddingRight: 20,
                paddingLeft: 20,
              }}>
              <Text
                activeOpacity={2}
                style={[
                  { fontSize: 16 },
                  BaseStyle.boldFont,
                ]}>
                {language.bodymass}
              </Text>
              <View
                style={{
                  height: 3,
                  width: 50,
                  borderRadius: 1.5,
                  backgroundColor: Constant.appColorAlpha(),
                }}
              />

              <View
                style={{
                  padding: 6,
                  marginTop: 10,
                  height: 120,
                  width: "100%",
                  backgroundColor: "#fff",
                  borderRadius: 6,
                  elevation: 4,
                  flexDirection: "row",
                }}>
                <View
                  style={{
                    flex: 0.5,
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <AnimatedCircularProgress
                    size={80}
                    duration={3000}
                    width={10}
                    fill={!!bmi ? bmi : 0}
                    tintColor={Constant.appColorAlpha()}
                    onAnimationComplete={() =>
                      console.log(
                        "onAnimationComplete"
                      )
                    }
                    backgroundColor="#D3D3D3">
                    {(fill) => (
                      <Text
                        style={[
                          {
                            color: Constant.appColorAlpha(),
                            fontSize: 18,
                          },
                          BaseStyle.boldFont,
                        ]}>
                        {!!bmi ? bmi : 0}
                      </Text>
                    )}
                  </AnimatedCircularProgress>

                  <Text
                    style={[
                      { fontSize: 14 },
                      BaseStyle.boldFont,
                    ]}>
                    {this.healthStatus(bmi)}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 0.5,
                    padding: 10,
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "flex-start",
                    }}>
                    <View
                      style={styles.backViewNew}>
                      <Image
                        style={{
                          height: 10,
                          width: 10,
                        }}
                        source={images.height}
                      />
                    </View>
                    <View style={{ width: 80 }}>
                      <Text
                        style={[
                          {
                            marginLeft: 10,
                            fontSize: 10,
                          },
                          BaseStyle.regularFont,
                        ]}>
                        {language.height}
                      </Text>
                      <Text
                        style={[
                          {
                            marginLeft: 8,
                            marginTop: -4,
                            color: Constant.appColorAlpha(),
                            fontSize: 16,
                          },
                          BaseStyle.boldFont,
                        ]}>
                        {height} cm
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 13,
                    }}>
                    <View
                      style={styles.backViewNew}>
                      <Image
                        style={{
                          height: 10,
                          width: 10,
                        }}
                        source={images.weight}
                      />
                    </View>
                    <View style={{ width: 80 }}>
                      <Text
                        style={[
                          {
                            marginLeft: 10,
                            fontSize: 10,
                          },
                          BaseStyle.regularFont,
                        ]}>
                        {language.weight}
                      </Text>
                      <Text
                        style={[
                          {
                            marginLeft: 8,
                            marginTop: -4,
                            color: Constant.appColorAlpha(),
                            fontSize: 16,
                          },
                          BaseStyle.boldFont,
                        ]}>
                        {weight}kg
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }

  healthStatus(bmi){
    var txt="";
if(bmi>24.9){
  txt="Over weight"
}else if(bmi<18.5){
  txt="Under weight"
}else{
  txt="Normal"
}
return txt
  }}

const styles = StyleSheet.create({
  backView: {
    height: 25,
    width: 25,
    borderRadius: 6,
    elevation: 4,
    backgroundColor: Constant.appLightColor(),
    justifyContent: "center",
    alignItems: "center",
  },
  backMainView: {
    flex: 0.22,
    flexDirection: "row",
    alignItems: "center",
  },
  detailsView: {
    padding: 15,
    paddingRight: 30,
    paddingLeft: 30,
    height: 100,
    width: width,
    backgroundColor: "#fff",
  },
  detailsText: { fontSize: 10, marginLeft: 7 },

  backViewNew: {
    height: 32,
    width: 32,
    borderRadius: 6,
    elevation: 4,
    backgroundColor: Constant.appLightColor(),
    justifyContent: "center",
    alignItems: "center",
  },
})

export default Profile
