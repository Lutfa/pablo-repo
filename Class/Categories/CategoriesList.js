import React,{Component}from "react"
import { View,Text, Animated, Alert,ScrollView,Image,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from './../BaseClass/BaseStyles'
import  Constant from './../BaseClass/Constant'
import SplashScreen from 'react-native-splash-screen'
import { FlatList } from "react-native-gesture-handler";
import images from "./../BaseClass/Images";
import { SafeAreaView } from "react-native-safe-area-context";
import Accordion from 'react-native-collapsible/Accordion';
import ImageLoad from './../BaseClass/ImageLoader'
import CartDb from "./../DB/CartDb";
import Db from "./../DB/Realm";
import Toast from 'react-native-simple-toast';
import {
    withNavigation,
  } from 'react-navigation';
const { width,height } = Dimensions.get('window');



class CategoriesList extends Component
{

    
    constructor(props)
    {
        super(props);
        this.state={
            categoriesData  : [],
            subCategoriesData : [],
            isLoading:false,
    }}


 componentDidMount() {
        SplashScreen.hide();

        let categoriesData = Db.objects('Categories').filtered('ParentCategoryId = "0"')

        let childArray = []
        for(let category of categoriesData)
        {
          let ChildData = Db.objects('Categories').filtered('ParentCategoryId = $0',category.Id)
          console.log(ChildData);
          
          childArray = [...childArray,...ChildData]
          
        }
        this.setState({categoriesData,subCategoriesData:childArray})

      

    }
  render()
  {
    
        const {isLoading,categoriesData,subCategoriesData} = this.state

      
      return(
          <SafeAreaView  style = {{flex:1,backgroundColor:'#fff'}}>
              {isLoading ? Constant.showLoader() : null}

            
            <View style = {{backgroundColor:Constant.headerColor(),elevation: 4 ,borderBottomLeftRadius:30,borderBottomRightRadius:30}}>

            <View style = {{paddingTop:10,paddingBottom:15, backgroundColor:Constant.headerColor() , flexDirection:'row',alignItems:'center',justifyContent:'center',borderBottomLeftRadius:30,borderBottomRightRadius:30}}>
           <TouchableOpacity
              onPress = {()=>{
                  this.props.navigation.pop(1)
              }}
               style  ={{marginLeft:15,height:35,borderRadius:15,alignItems:'center',justifyContent:'center'}}>
              <Image tintColor = '#fff' style = {{height:20,width:20,left:0}} source = {images.back}/>
              </TouchableOpacity>
              <Text style = {[{flex:1},BaseStyle.headerFont]}>CATEGORIES</Text>

              <View style = {{flexDirection:'row',marginRight:15}}>
              
             

             <TouchableOpacity
            onPress = {()=>{
              this.props.navigation.navigate('Cart')

            }}
              style={{marginRight:15,height:35,borderRadius:15,alignItems:'center',justifyContent:'center'}}
            >

              <Image
              resizeMode={'center'}
                style={{ width: 20, height: 20 }}
                source={images.cart}
              />
            {CartDb.Count() > 0 ?
            <View style = {{position:'absolute',left:12,top:-6,width:20,justifyContent:'center',alignItems:'center', height:20,borderRadius:10 ,backgroundColor:Constant.appColorAlpha()}}>
            <Text style = {[{color:'#fff',fontSize:12},BaseStyle.boldFont]}>{CartDb.Count()}</Text>
            </View> : null }
            </TouchableOpacity>


         <TouchableOpacity
            onPress = {()=>{
              this.props.navigation.navigate('Wishlist')

            }}
              style={{marginLeft:5,marginRight:10,height:35,borderRadius:15,alignItems:'center',justifyContent:'center'}}
            >

              <Image
              resizeMode={'center'}
                style={{ width: 20, height: 20 }}
                
                source={images.pwishlist}
              />
            {CartDb.WishListCount() > 0 ?
            <View style = {{position:'absolute',left:12,top:-6,width:20,justifyContent:'center',alignItems:'center', height:20,borderRadius:10 ,backgroundColor:Constant.appColorAlpha()}}>
            <Text style = {[{color:'#fff',fontSize:12},BaseStyle.boldFont]}>{CartDb.WishListCount()}</Text>
            </View> : null }
            </TouchableOpacity>

            </View>
            </View>
             </View>

            <ScrollView>
            <Text style = {[styles.mainText,BaseStyle.boldFont]}>MAIN CATEGORIES</Text>


            <FlatList
            style = {{paddingBottom:10}}
            data = {categoriesData}
            numColumns={4}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            renderItem = {this.renderItem}
            />

            <Text style = {[styles.mainText,BaseStyle.boldFont]}>AVAILABLE SUB CATEGORIES</Text>


            <FlatList
            data = {subCategoriesData}
            numColumns={4}
            style = {{paddingBottom:20}}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            renderItem = {this.renderItem}
            />

            </ScrollView>

          </SafeAreaView>
  )}



  renderItem = ({item,index})=>{

      return (
          <View style = {{padding:5, paddingLeft:index%4 == 0 ? 10 : 0}}>
          <TouchableOpacity
          activeOpacity={2}
          onPress = {()=>{
              let Id = 0
              if(item.ParentCategoryId == 0)
              {
                  Id = item.Id
              }
              else{
                  Id = item.ParentCategoryId
              }
              this.props.navigation.navigate('Categories',{parentId:Id,isCategoris:true})
          }}
           style={[styles.categoriesBox]}>
                <View style={styles.topInnerBox2}>
                  <ImageLoad
                    placeholderStyle={{ height: 25, width: 25 }}
                    style ={{ height: 25, width: 25 }}
                    loadingStyle={{ size: 'large', color: Constant.appColorAlpha() }}
                    source={{ uri:item.ImageUrl}}
                    placeholderSource={images.placeholder}
                />
                </View>
                <Text numberOfLines={2} style={[BaseStyle.regularFont, styles.topInnerText]}>{item.Name}</Text>
              </TouchableOpacity>
              </View>
      )

  }
}



const styles = StyleSheet.create({
mainText : {marginTop:10,width:'100%',textAlign :'center',fontSize:16},
categoriesBox:{
    width:(width/4)-16,
    padding:10,
    marginRight:10,
    
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    elevation: 5,

  },
  topInnerBox2: { width: 40, height: 40, borderRadius: 6, elevation: 4, backgroundColor: Constant.appLightColor(), alignItems: 'center', justifyContent: 'center' },
  topInnerText: { color: Constant.textColor(), paddingLeft: 5, paddingRight: 5, marginTop: 10, fontSize: 11, textAlign: 'center', lineHeight: 13 ,height:26},
topBox: {
    height: 90,
    paddingRight:5,paddingLeft:5,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
})

export default CategoriesList


              