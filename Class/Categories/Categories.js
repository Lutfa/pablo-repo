import React, { Component } from "react"
import {
  View,
  FlatList,
  ImageBackground,
  AppState,
  ActivityIndicator,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import SwipeListView from "./../ExtraClass/SwipeList/SwipeListView"
import SplashScreen from "react-native-splash-screen"
import Db from "./../DB/Realm"
import Urls from "./../BaseClass/ServiceUrls"
import CartDb from "./../DB/CartDb"
const { width, height } = Dimensions.get("window")
import Toast from "react-native-simple-toast"
import ImageLoad from "./../BaseClass/ImageLoader"
import NoData from "../BaseClass/NoData"

class Categories extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      footerLoading: false,
      parentId:
        props.route.params != null
          ? props.route.params.parentId
          : 0,
      productArray: [],
      categoryId: 0,
      categoriesArray: [],
      categoryIndex: 0,
      pageNumber: 1,
      updateUi: true,
      isReachLast: false,
      isCategoris:
        props.route.params != null
          ? props.route.params.isCategoris
          : true,
    }
  }

  componentDidMount() {
    const { parentId, isCategoris } = this.state
    if (isCategoris) {
      let categoriesArray = Db.objects(
        "Categories"
      ).filtered(
        "ParentCategoryId = $0",
        this.state.parentId
      )
      this.setState({ categoriesArray })

      if (categoriesArray.length > 0) {
        this.setState(
          { categoryId: categoriesArray[0].Id },
          () => {
            this.getProducts(categoriesArray[0].Id)
          }
        )
      }
    } else {
      this.setState(
        { categoryId: parentId },
        () => {
          this.getHealthIssuesProducts(parentId)
        }
      )
      // this.getHealthIssuesProducts()
    }
    // SplashScreen.hide();
  }

  render() {
    const {
      article,
      footerLoading,
      categoryIndex,
      categoriesArray,
      isLoading,
      productArray,
    } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {isLoading ? Constant.showLoader() : null}

        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: 4,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor: Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              PRODUCTS
            </Text>

            <View
              style={{
                flexDirection: "row",
                marginRight: 15,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate(
                    "Cart"
                  )
                }}
                style={{
                  marginRight: 15,
                  height: 35,
                  borderRadius: 15,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Image
                  resizeMode={"center"}
                  style={{ width: 20, height: 20 }}
                  source={images.cart}
                />
                {CartDb.Count() > 0 ? (
                  <View
                    style={{
                      position: "absolute",
                      left: 12,
                      top: -6,
                      width: 20,
                      justifyContent: "center",
                      alignItems: "center",
                      height: 20,
                      borderRadius: 10,
                      backgroundColor: Constant.appColorAlpha(),
                    }}>
                    <Text
                      style={[
                        {
                          color: "#fff",
                          fontSize: 12,
                        },
                        BaseStyle.boldFont,
                      ]}>
                      {CartDb.Count()}
                    </Text>
                  </View>
                ) : null}
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate(
                    "Wishlist"
                  )
                }}
                style={{
                  marginLeft: 5,
                  marginRight: 10,
                  height: 35,
                  borderRadius: 15,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Image
                  resizeMode={"center"}
                  style={{ width: 20, height: 20 }}
                  source={images.pwishlist}
                />
                {CartDb.WishListCount() > 0 ? (
                  <View
                    style={{
                      position: "absolute",
                      left: 12,
                      top: -6,
                      width: 20,
                      justifyContent: "center",
                      alignItems: "center",
                      height: 20,
                      borderRadius: 10,
                      backgroundColor: Constant.appColorAlpha(),
                    }}>
                    <Text
                      style={[
                        {
                          color: "#fff",
                          fontSize: 12,
                        },
                        BaseStyle.boldFont,
                      ]}>
                      {CartDb.WishListCount()}
                    </Text>
                  </View>
                ) : null}
              </TouchableOpacity>
            </View>
          </View>
        </View>

        {this.state.isCategoris ? (
          <View
            style={{
              width: "100%",
              backgroundColor: "#fff",
              padding: 8,
            }}>
            <FlatList
              data={categoriesArray}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              renderItem={({ item, index }) => (
                <TouchableOpacity
                  onPress={() => {
                    // this.setState({categoryIndex:index})
                    this.setState(
                      {
                        categoryId: item.Id,
                        pageNumber: 1,
                        productArray: [],
                        categoryIndex: index,
                      },
                      () => {
                        this.getProducts(item.Id)
                      }
                    )
                  }}
                  style={{
                    height: 40,
                    padding: 5,
                    paddingRight: 8,
                    paddingLeft: 8,
                    marginRight: 6,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor:
                      categoryIndex == index
                        ? Constant.appColorAlpha()
                        : "#fff",
                    borderWidth:
                      categoryIndex != index
                        ? 1
                        : 0,
                    borderColor: Constant.headerColor(),
                    borderRadius: 18,
                  }}>
                  <Text
                    numberOfLines={1}
                    style={[
                      {
                        fontSize: 14,
                        color:
                          categoryIndex == index
                            ? "#fff"
                            : Constant.headerColor(),
                        textAlign: "center",
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {item.Name}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
        ) : null}

        <View
          style={{ flex: 1, height: height - 100 }}>
          <FlatList
            data={productArray}
            numColumns={2}
            contentContainerStyle={{ flex: 1 }}
            extraData={this.state}
            onEndReachedThreshold={0.1}
            onEndReached={this.loadData}
            ListEmptyComponent={() => {
              return this.state.isLoading ? null : (
                <NoData />
              )
            }}
            ListFooterComponent={() => {
              if (!footerLoading) {
                return null
              }
              return (
                <View
                  style={{
                    height: 40,
                    width: "100%",
                    alignItems: "center",
                    justifyContent: "center",
                  }}>
                  <ActivityIndicator
                    size="large"
                    color={Constant.appColorAlpha()}
                  />
                </View>
              )
            }}
            renderItem={this.renderItem}
          />
        </View>
      </SafeAreaView>
    )
  }

  renderItem = ({ item, index }) => {
    let Isexit = CartDb.productExist(item.Id)

    return (
      <View style={styles.bestSellingsMain}>
        <TouchableOpacity
          activeOpacity={2}
          onPress={() => {
            this.props.navigation.push(
              "ProductDetails",
              { product: item }
            )
          }}
          style={{
            flex: 1,
            width: "100%",
            borderRadius: 6,
            backgroundColor: "#fff",
            elevation: 3,
            marginRight: 2,
            paddingTop: 6,
            justifyContent: "center",
            alignItems: "center",
          }}>
          <ImageLoad
            placeholderStyle={{
              width: "100%",
              height: 90,
            }}
            style={{ width: "100%", height: 90 }}
            loadingStyle={{
              size: "large",
              color: Constant.appColorAlpha(),
            }}
            source={{
              uri:
                item.Images.length > 0
                  ? item.Images[0].Src
                  : "null",
            }}
            placeholderSource={images.placeholder}
          />
          {item.ColorCode ==
          Constant.prescriptionColorCode() ? (
            <View
              style={{
                position: "absolute",
                alignItems: "center",
                justifyContent: "center",
                top: 10,
                right: 10,
                height: 26,
                width: 32,
                borderBottomLeftRadius: 17.5,
                borderTopLeftRadius: 17.5,
              }}>
              <Image
                style={{ height: 15, width: 15 }}
                source={images.prescription}
              />
            </View>
          ) : null}
          <View style={{ padding: 5 }}>
            <Text
              numberOfLines={1}
              style={[
                { fontSize: 14 },
                BaseStyle.boldFont,
              ]}>
              {item.Name}
            </Text>
            <Text
              numberOfLines={1}
              style={[
                {
                  fontSize: 12,
                  color: Constant.textAlpha(),
                  lineHeight: 15,
                },
                BaseStyle.regularFont,
              ]}>
              {item.ShortDescription}
            </Text>

            <View
              style={{
                height: 40,
                marginBottom: 0,
                width: width / 2 - 40,
                justifyContent: "space-between",
                alignItems: "center",
                flexDirection: "row",
              }}>
              <Text
                style={[
                  {
                    fontSize: 16,
                    color: Constant.redColor(),
                  },
                  BaseStyle.boldFont,
                ]}>
                {Constant.price()}
                {parseFloat(item.Price).toFixed(2)}
              </Text>
              {Isexit ? (
                this.showCountView(item.Id)
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    let isAdded = CartDb.addData(
                      item
                    )
                    if (isAdded) {
                      Toast.show(
                        "Product added to cart"
                      )
                    }
                    this.setState({
                      updateUi: false,
                    })
                  }}
                  style={{
                    height: 35,
                    width: 35,
                    backgroundColor: Constant.appColorAlpha(),
                    borderRadius: 20,
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <Image
                    resizeMode={"center"}
                    style={{
                      width: 22,
                      height: 22,
                    }}
                    source={images.cart}
                  />
                </TouchableOpacity>
              )}
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  showCountView(Id) {
    return (
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          width: 65,
          marginRight: 10,
        }}>
        <TouchableOpacity
          onPress={() => {
            let cartItem = Db.objectForPrimaryKey(
              "CartDB",
              Id
            )
            if (cartItem.count - 1 > 0) {
              try {
                Db.write(() => {
                  cartItem.count =
                    cartItem.count - 1
                })
              } catch (e) {
                console.log("Error on creation")
              }

              this.setState({ updateUi: false })
            } else {
              CartDb.delete(Id)
              this.setState({ updateUi: false })
            }
          }}>
          <Image
            resizeMode={"contain"}
            style={{ height: 23, width: 23 }}
            source={images.pMinus}
          />
        </TouchableOpacity>
        <Text
          style={[
            {
              fontSize: 14,
              padding: 8,
              color: "#000",
            },
            BaseStyle.boldFont,
          ]}>
          {CartDb.CartDataCount(Id)}
        </Text>

        <TouchableOpacity
          onPress={() => {
            try {
              Db.write(() => {
                let cartItem = Db.objectForPrimaryKey(
                  "CartDB",
                  Id
                )
                cartItem.count = cartItem.count + 1
              })
            } catch (e) {
              console.log("Error on creation")
              console.log(e)
            }
            this.setState({ updateUi: false })
          }}>
          <Image
            resizeMode={"contain"}
            style={{ height: 23, width: 23 }}
            source={images.pPlus}
          />
        </TouchableOpacity>
      </View>
    )
  }

  loadData = () => {
    const {
      productArray,
      isCategoris,
      parentId,
      isReachLast,
    } = this.state

    if (productArray.length > 0 && !isReachLast) {
      if (isCategoris) {
        this.getProducts(this.state.categoryId)
      } else {
        this.getHealthIssuesProducts(
          this.state.categoryId
        )
      }
      //alert('end')
    }
  }

  getProducts(id) {
    const { pageNumber, productArray } = this.state
    this.setState({
      isLoading: pageNumber == 1 ? true : false,
      footerLoading: pageNumber != 1 ? true : false,
    })
    let params = {
      CategoryId: id,
      PageNo: pageNumber,
      PageSize: 20,
    }

    console.log("category request: " + params)
    Constant.postMethod(
      Urls.getCategoryProducts,
      params,
      (result) => {
        console.log(JSON.stringify(result))
        this.setState({
          isLoading: false,
          pageNumber: pageNumber + 1,
          footerLoading: false,
        })
        if (result.success) {
          if (result.result.Response != null) {
            this.setState({
              productArray: [
                ...productArray,
                ...result.result.Response.Data,
              ],
              isReachLast:
                result.result.Response.Data
                  .length <= 0,
            })
          }
        }
      }
    )
  }

  getHealthIssuesProducts(id) {
    const { pageNumber, productArray } = this.state
    this.setState({
      isLoading: pageNumber == 1 ? true : false,
    })
    let params = {
      CategoryId: id,
      PageNumber: pageNumber,
      PageSize: 8,
    }

    console.log("health issues req:" + params)
    Constant.postMethod(
      Urls.getHealthIssueProducts,
      params,
      (result) => {
        this.setState({
          isLoading: false,
          pageNumber: pageNumber + 1,
        })
        //  alert(JSON.stringify(result))
        if (result.success) {
          if (result.result.Response != null) {
            this.setState({
              productArray: [
                ...productArray,
                ...result.result.Response.Data,
              ],
            })
          }
        }
      }
    )
  }
}

const styles = StyleSheet.create({
  bestSellingsMain: {
    height: 200,
    width: "50%",
    borderRadius: 6,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
  },
})
export default Categories
