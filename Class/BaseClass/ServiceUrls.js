var baseUrl ="http://farmaci3.w102.wh-2.com/api/"
//"https://firstmeddev.azurewebsites.net/api/"
  
const urls = {
  
  login: baseUrl + "login",
  validateEmail: baseUrl + "ValidateEmail",
  validatePhone: baseUrl + "ValidatePhone",
  registerCustomer: baseUrl + "RegisterCustomer",
  getCustomerAddress:
    baseUrl + "GetCustomerAddresses",
  addCustomerAddress:
    baseUrl + "AddCustomerAddress",
  deleteCustomerAddress:
    baseUrl + "DeleteCustomerAddress",
  getAllHealthRecords:
    baseUrl + "GetAllHealthRecords",
  mediaUpload: baseUrl + "UploadImage",
  addHealthRecord: baseUrl + "AddHealthRecord",
  deleteHealthRecord:
    baseUrl + "DeleteHealthRecord/",
  getArticles: baseUrl + "GetArticles",
  updateHealthRecord:
    baseUrl + "UpdateHealthRecord",
  profileUpdate: baseUrl + "ProfileUpdate",
  getDiscounts: baseUrl + "GetDiscounts",
  getTopSellerProducts:
    baseUrl + "GetTopSellerProducts",
  getNewlyAddedProducts:
    baseUrl + "GetNewlyAddedProducts",
  getBanners: baseUrl + "GetBanners",
  getCategories: baseUrl + "GetCategories",
  getHealthIssues: baseUrl + "GetHealthIssues",
  getArticles: baseUrl + "GetArticles",
  editCustomerAddress:
    baseUrl + "EditCustomerAddress",
  addToCart: baseUrl + "AddToCart",
  createOrder: baseUrl + "CreateOrder",
  getCartItems: baseUrl + "GetCartItems",
  applyDiscountCoupon:
    baseUrl + "ApplyDiscountCoupon",
  bookmarkArticle: baseUrl + "BookmarkArticle",
  serachMedicine: baseUrl + "searchmedicines",
  createPillReminder:
    baseUrl + "CreatePillReminder",
  getPillReminder: baseUrl + "GetPillReminder",
  updatePillStatus: baseUrl + "UpdatePillStatus",
  getCategoryProducts:
    baseUrl + "GetCategoryProducts",
  getHealthIssueProducts:
    baseUrl + "GetHealthIssueProducts",
  getProductsWithAppliedDiscount:
    baseUrl + "GetProductsWithAppliedDiscount",
  getProductDetails: baseUrl + "GetProductDetails",
  deletePillReminder:
    baseUrl + "DeletePillReminder",
  getAllOrder: baseUrl + "GetCustomerOrders",

  updatePillReminder:
    baseUrl + "UpdatePillReminder",
  baseUrl: baseUrl,
}
export default urls
