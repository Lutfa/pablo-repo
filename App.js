/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react"
import { NavigationContainer } from "@react-navigation/native"
import {
  TransitionSpecs,
  HeaderStyleInterpolators,
  createStackNavigator,
} from "@react-navigation/stack"

import Intro from "./Class/Intro/Intro"
import Login from "./Class/Login/Login"
import Register from "./Class/Register/Register"
import Otp from "./Class/OTP/NewOtp"
import ArticleList from "./Class/HealthArticals/ArticleList"
import ArticleDetail from "./Class/HealthArticals/ArticleDetail"
import ArticleTabs from "./Class/HealthArticals/ArticleTabs"
import Home from "./Class/Home/Home"
import CreateAddress from "./Class/Address/CreateAddress"
import Address from "./Class/Address/Address"
import AddressSearch from "./Class/Address/AddressSearch"
import AddMedicine from "./Class/AddMedicine/AddMedicine"
import AllMedicine from "./Class/AddMedicine/AllMedicine"
import Chat from "./Class/Chat/Chat"
import HealthRecords from "./Class/HealthRecords/HealthRecords"
import AddHealthRecord from "./Class/HealthRecords/AddHealthRecord"
import HealthRecordDetails from "./Class/HealthRecords/HealthRecordDetails"
import ProductDetails from "./Class/Product/ProductDetails"
import Search from "./Class/Search/Search"
import UploadPrescription from "./Class/UploadPrescription/UploadPrescription"
import Wishlist from "./Class/Wishlist/Wishlist"
import Cart from "./Class/Cart/Cart"
import Categories from "./Class/Categories/Categories"
import Orders from "./Class/Orders/Orders"
import CreateOrder from "./Class/Orders/CreateOrder"
import Profile from "./Class/Profile/Profile"
import CreatePin from "./Class/Pin/Pin"
import ForgotPassword from "./Class/ForgotPassword/ForgotPassword"
import SuccessScreen from "./Class/Orders/SuccessScreen"
import EditProfile from "./Class/Profile/EditProfile"
import AddDiscount from "./Class/Discount/AddDiscount"
import CategoriesList from "./Class/Categories/CategoriesList"
import HealthIssues from "./Class/Categories/HealthIssuesList"
import AllProducts from "./Class/Product/AllProducts"
import OrderDetails from "./Class/Orders/OrderDetails"
import {
  DefaultTheme,
  Provider as PaperProvider,
} from "react-native-paper"
import { navigationRef } from "./RootNavigation"
import PillAlert from "./Class/PillAlert"

console.disableYellowBox = true

const Stack = createStackNavigator()

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#f36633",
    primaryAlpha: "#f9b299",
    accent: "yellow",
  },
}

function App() {
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator
          initialRouteName="Intro"
          screenOptions={{
            headerShown: true,
            cardOverlayEnabled: true,
            gestureEnabled: false,
            cardStyle: {
              backgroundColor: "transparent",
            },
          }}>
          <Stack.Screen
            name="Intro"
            component={Intro}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Register"
            component={Register}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Otp"
            component={Otp}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="ArticleList"
            component={ArticleList}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="ArticleDetail"
            component={ArticleDetail}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="ArticleTabs"
            component={ArticleTabs}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Home"
            component={Home}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Address"
            component={Address}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="AddressSearch"
            component={AddressSearch}
            options={{ headerShown: true }}
          />

          <Stack.Screen
            name="AddAddress"
            component={CreateAddress}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="AddMedicine"
            component={AddMedicine}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="AllMedicine"
            component={AllMedicine}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Chat"
            component={Chat}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="HealthRecords"
            component={HealthRecords}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="AddHealthRecord"
            component={AddHealthRecord}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="HealthRecordDetails"
            component={HealthRecordDetails}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="ProductDetails"
            component={ProductDetails}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Search"
            component={Search}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="UploadPrescription"
            component={UploadPrescription}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Wishlist"
            component={Wishlist}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Cart"
            component={Cart}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Categories"
            component={Categories}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="Orders"
            component={Orders}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="CreateOrder"
            component={CreateOrder}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="Profile"
            component={Profile}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="ForgotPassword"
            component={ForgotPassword}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="CreatePin"
            component={CreatePin}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="SuccessScreen"
            component={SuccessScreen}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="EditProfile"
            component={EditProfile}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="AddDiscount"
            component={AddDiscount}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="CategoriesList"
            component={CategoriesList}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="HealthIssues"
            component={HealthIssues}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="AllProducts"
            component={AllProducts}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="OrderDetails"
            component={OrderDetails}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="PillAlert"
            component={PillAlert}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  )
}

export default App
